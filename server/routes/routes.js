const express = require("express");
// const { verify } = require("crypto");
const Func = require("../functions/functions");
const sequelize = require("../../config/db");
const router = express.Router();
const jwt = require("jsonwebtoken");
const cache = require('../../config/node-cache');
const path = require("path");


const multer = require("multer");
// const storage = multer.diskStorage({
//   destination: function(req, file, cb) {
//     cb(null, path.join(global.rootPath,'uploads'));
//     console.log(file)
//   },
//   filename: function(req, file, cb) {
//     console.log(file,"fil in multer")
//     cb(null, new Date().toISOString().replace(/:/g, '-') + file.originalname);
//   }
// });

// const fileFilter = (req, file, cb) => {
//   // reject a file
//   if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png' || true) {
//     cb(null, true);
//   } else {
//     cb(null, false);
//   }
// };

// const upload = multer({
//   storage: storage,
//   // limits: {
//   //   fileSize: 1024 * 1024 * 15
//   // },
//   fileFilter: fileFilter
// });

// Multer Properties
const multerStorage = multer.memoryStorage();

const multerFilter = (req, file, cb) => {
  if (file.mimetype.startsWith('image')) {
    cb(null, true);
  } else {
    cb(new AppError('Not an image! Please upload only images.', 400), false);
  }
};

const upload = multer({ storage: multerStorage, fileFilter: multerFilter });


// Controllers
const AddressControllers = require("../controller/addresController");
const ConfigController = require("../controller/configController");
const CurrencyController = require("../controller/currencyController");
const FavouritesController = require("../controller/favouritesController");
const MarketsControllers = require("../controller/marketController");
const MarketKategoriyaController = require("../controller/marketKategoriyaController");
const MarketSubKategoriyaController = require("../controller/marketSubKategoriyaController");
const MarketAddressControllers = require("../controller/marketAddrressController");
const OrderedProductController = require("../controller/orderedProductController");
const OrdersController = require("../controller/ordersController");
const PhoneNumbersControllers = require("../controller/phoneNumberController");
const PostsController = require("../controller/postsController");
const ProductsController = require("../controller/productsController");
const SliderController = require("../controller/sliderController");
const StatusController = require("../controller/statusController");
const UnitsController = require("../controller/unitsController");
const UserControllers = require("../controller/userController");
const UserTypeControllers = require("../controller/userTypeController");
const SebetController = require("../controller/sebetController");
const NotificationController = require("../controller/notificationController");
const WelayatlarController = require("../controller/welayatlarController");
const KategoryOfMarketsController = require("../controller/kategoriyaOfMarketsController");
const RazmerlerController = require("../controller/razmerlerController");
const RenklerController = require("../controller/renklerController");
const BrandsKategoryController = require("../controller/brandsKategoryController");
const BrandsController = require("../controller/brandsController");
const UserMarketsController = require("../controller/userMarketsController");
const ReceptController = require("../controller/receptController");
const ReceptKategoryController = require("../controller/receptKategoryController");
const ReceptSubKategoryController = require("../controller/receptSubKategoryController");
const ReceptProductsController = require("../controller/receptProductsController");
const ReceptIngredientController = require("../controller/receptIngredientController");
const CashBackController = require("../controller/cashBackController");
const BrandsSubKategoryController = require("../controller/brandSubKategoryController");
const SowgatController = require("../controller/sowgatController");
const ReklamaController = require("../controller/reklamaController");
const FcmTokensController = require("../controller/fcmTokenController");
const FcmTokensOrdersController = require("../controller/fcmTokensOrdersController");
const FcmNotificationsController = require("../controller/fcmNotificationController");

// // Routes

router.get("/address_tb",AddressControllers.address_tb);
router.get("/config_tb",ConfigController.config_tb);
router.get("/currency_tb",CurrencyController.currency_tb);
router.get("/favourites_tb",FavouritesController.favourites_tb);
router.get("/markets_tb",MarketsControllers.markets_tb);
router.get("/marketKategoriya_tb",MarketKategoriyaController.marketKategoriya_tb);
router.get("/marketSubKategoriya_tb",MarketSubKategoriyaController.marketSubKategoriya_tb);
router.get("/marketAddress_tb",MarketKategoriyaController.marketKategoriya_tb);
router.get("/orderedProduct_tb",OrderedProductController.orderedProduct_tb);
router.get("/orders_tb",OrdersController.orders_tb);
router.get("/phoneNumber_tb",PhoneNumbersControllers.phoneNumber_tb);
router.get("/posts_tb",PostsController.post_tb);
router.get("/products_tb",ProductsController.products_tb);
router.get("/slider_tb",SliderController.sliders_tb);
router.get("/status_tb",StatusController.status_tb);
router.get("/units_tb",UnitsController.units_tb);
router.get("/users_tb", UserControllers.users_tb);
router.get("/userType_tb",UserTypeControllers.userType_tb);
router.get("/userPermission_tb",UserTypeControllers.userPermissions_tb);
router.get("/sebet_tb",SebetController.sebet_tb);
router.get("/notification_tb",NotificationController.notification_tb);
router.get("/welayatlar_tb",WelayatlarController.welayatlar_tb);
router.get("/kategoryOfMarkets_tb",KategoryOfMarketsController.kategoryyaOfMarkets_tb);
router.get("/razmerler_tb",RazmerlerController.razmerler_tb);
router.get("/renkler_tb",RenklerController.renkler_tb);
router.get("/bannerKategory_tb",BrandsKategoryController.brandsKategory_tb);
router.get("/brands_tb",BrandsController.brands_tb);
router.get("/userMarkets_tb",UserMarketsController.userMarkets_tb);
router.get("/recepts_tb",ReceptController.recepts_tb);
router.get("/receptKategory_tb",ReceptKategoryController.receptKategory_tb);
router.get("/receptSubKategory_tb",ReceptSubKategoryController.receptSubKategory_tb);
router.get("/receptProducts_tb",ReceptProductsController.receptProducts_tb);
router.get("/receptIngredient_tb",ReceptIngredientController.receptIngridient_tb);
router.get("/cashBack_tb",CashBackController.cashBack_tb);
router.get("/brandsSubKategory_tb",BrandsSubKategoryController.brandsSubKategory_tb);
router.get("/sowgat_tb",SowgatController.sowgat_tb);
router.get("/reklama_tb",ReklamaController.reklema_tb);
router.get("/fcmTokens_tb",FcmTokensController.fcmTokens_tb);
router.get("/fcmTokensOrders_tb",FcmTokensOrdersController.fcmTokensOrders_tb);
router.get("/fcmNotifications_tb",FcmNotificationsController.notification_tb);



// UserType Routes

router.get("/user/type",cache.get,UserTypeControllers.getUserType,cache.set);
router.post("/user/type/create",UserTypeControllers.createType);

router.post("/user/type/create/subMarket",UserTypeControllers.createTypeForMarket);

router.patch("/user/type/update/:type_id",UserTypeControllers.updateUserType);
router.delete("/user/type/delete/:type_id",UserTypeControllers.deleteUserType);
router.patch("/user/type/permission/update",UserTypeControllers.updateUserTypePermissions);

//User Routes
router.get("/users",cache.get,UserControllers.list,cache.set);
router.post("/user/create", UserControllers.create);
router.post("/user/login",UserControllers.login);
router.post("/user/update/:id",UserControllers.update_user);
router.post("/user/forget",UserControllers.update_user_onUser);
router.post("/user/update/address/:user_id",UserControllers.updatePrimaryAddress);
router.delete("/user/delete/:id",UserControllers.delete_user);
router.delete("/user/destroy/:id",UserControllers.destroy_user);

// Socket
router.post("/send", UserControllers.SendToSocket);


// Address Routes
router.get("/user/address",cache.get,AddressControllers.get_user_address,cache.set);
router.get("/user/address/:user_id",cache.get,AddressControllers.getOneUserAddresses,cache.set);
router.get("/user/address_one/:address_id",cache.get,AddressControllers.getUserOneAddress,cache.set);
router.post("/user/address/create/:user_id",AddressControllers.create_user_addres);
router.post("/user/address/rec_create",AddressControllers.create_user_rec_addres);
router.post("/user/address/update/:address_id",AddressControllers.update_address);
router.patch("/user/address/update/:address_id",AddressControllers.update_address);
router.delete("/user/address/delete/:address_id",AddressControllers.delete_address);
router.post("/user/address/delete/:address_id",AddressControllers.delete_address);
router.post("/user/address/destroy/:address_id",AddressControllers.destroy_address);

router.get("/market/address",cache.get,MarketAddressControllers.get_market_address,cache.set,);
router.get("/market/address/:market_address_id",cache.get,MarketAddressControllers.get_market_one_address,cache.set);
router.post("/market/address/create/:market_id",MarketAddressControllers.create_market_address);
router.patch("/market/address/update/:address_id",MarketAddressControllers.update_address);
router.delete("/market/address/delete/:address_id",MarketAddressControllers.delete_address);



// Phone Numbers of Markets
router.post("/market/phone/create/:market_id",PhoneNumbersControllers.create_market_phone);
router.delete("/market/phone/delete/:phone_id",PhoneNumbersControllers.delete_phone);

//Markets Routes
router.get("/market/description/",cache.get,MarketsControllers.getOneMarketDes,cache.set);

router.get("/markets",cache.get,MarketsControllers.get_all_markets,cache.set);
router.get("/market/:market_id",cache.get,MarketsControllers.get_one_markets,cache.set);
router.post("/market/create",MarketsControllers.create_market);
router.post("/market/surat/:market_id",MarketsControllers.uploadSurat);
router.patch("/markets/update/:market_id",MarketsControllers.update_markets);
router.patch("/market/update/:market_id",MarketsControllers.updateMarketWithoutSurat);
router.delete("/market/delete/:market_id",MarketsControllers.delete_market);
router.delete("/market/destroy/:market_id",MarketsControllers.destroy_market);

// Market Kategoriya
router.get("/market/kategoriya/:market_id",cache.get,MarketKategoriyaController.getAllKategoriya,cache.set);
router.get("/market/kategoriya_one/:kategoriya_id",cache.get,MarketKategoriyaController.getOneKategoriya,cache.set)
router.post("/market/kategoriya/create/:market_id",MarketKategoriyaController.createKategoriya);
router.patch("/market/kategoriya/update/:kategoriya_id",MarketKategoriyaController.updateKategoriya);
router.delete("/market/kategoriya/delete/:kategoriya_id",MarketKategoriyaController.deleteKategoriya);
router.delete("/market/kategoriya/destroy/:kategoriya_id",MarketKategoriyaController.destroyKategoriya);

// Market SubKategoriya
router.get("/market/subKategoriya/:kategory_id",cache.get,MarketSubKategoriyaController.getAllKategoriya,cache.set);
router.get("/market/subKategoriya_one/:subKategoriya_id",cache.get,MarketSubKategoriyaController.getOneKategoriya,cache.set)
router.post("/market/subKategoriya/create/:kategory_id",MarketSubKategoriyaController.createKategoriya);
router.patch("/market/subKategoriya/update/:subKategoriya_id",MarketSubKategoriyaController.updateKategoriya);
router.delete("/market/subKategoriya/delete/:subKategoriya_id",MarketSubKategoriyaController.deleteKategoriya);
router.delete("/market/subKategoriya/destroy/:subKategoriya_id",MarketSubKategoriyaController.destroyKategoriya);

//Products Routes
router.get("/product/fromExel",cache.get,ProductsController.getDataFromExel,cache.set);
router.get("/products",cache.get,ProductsController.all_harytlar,cache.set);
router.get("/products/search",cache.get,ProductsController.SearchHarytlar,cache.set);
router.get("/products/disActive",cache.get,ProductsController.allDisActive_harytlar,cache.set);
router.get("/product/market/:market_id",cache.get,ProductsController.getOneMarketPro,cache.set);
router.get("/product/market/product/:marketKategoriya_id",cache.get,ProductsController.getOneMarketKategoriyaPro,cache.set);
router.get("/product/market/subKategory/:marketSubKategoriya_id",cache.get,ProductsController.getOneMarketSubKategoriyaPro,cache.set)
router.get("/product/:product_id",cache.get,ProductsController.one_haryt,cache.set);
router.post("/product/create/:market_id",ProductsController.create_haryt);
router.patch("/product/update/:product_id",ProductsController.update_haryt);
router.patch("/product/skidka/:product_id",ProductsController.Skidka);
router.patch("/product/isActive/:product_id",ProductsController.IsActive);
router.patch("/products/update/new",ProductsController.UpdateNewAndSale)
router.patch("/products/update/saleUntil",ProductsController.UpdateSaleUntil);
router.patch("/product/viev/:id",ProductsController.Watched);
router.delete("/product/delete/:product_id",ProductsController.delete_haryt);
router.delete("/product/destroy/:product_id",ProductsController.destroy_haryt);
router.get("/product/same/:product_id",cache.get,ProductsController.sameProducts,cache.set);

//Units of Products
router.get("/units",cache.get,UnitsController.getAllUnits,cache.set);
router.get("/unit/:unit_id",cache.get,UnitsController.getOneUnits,cache.set)
router.post("/unit/create",UnitsController.create);
router.patch("/unit/update/:unit_id",UnitsController.update);
router.delete("/unit/delete/:unit_id",UnitsController.Delete);
router.delete("/unit/destroy/:unit_id",UnitsController.Destroy);



// Sliders Route
router.get("/sliders",cache.get,SliderController.getAllSliders,cache.set);
router.post("/slider/create",upload.single('productImage'),SliderController.create);
router.patch("/slider/update/:slider_id",upload.single('productImage'),SliderController.update);
router.delete("/slider/delete/:slider_id",SliderController.Delete);

//config 
router.get("/configs",cache.get,ConfigController.getAllConfig,cache.set);
router.post("/config/create",ConfigController.create);
router.patch("/config/update/:config_id",ConfigController.update);

//Posts 
router.get("/posts",cache.get,PostsController.getAllPosts,cache.set);
router.get("/posts/user/:UserId",cache.get,PostsController.getUserPosts,cache.set);
router.post("/post/create/:UserId",PostsController.create);
router.patch("/post/update/:PostId",PostsController.update);
router.delete("/post/delete/:PostId",PostsController.Delete);

// Currencies
router.get("/currencies",cache.get,CurrencyController.getAllCurrency,cache.set);
router.post("/currency/create/:ConfigId",CurrencyController.Create);
router.patch("/currency/update/:CurrencyId",CurrencyController.update);
router.delete("/currency/delete/:CurrencyId",CurrencyController.Delete);

// Status of Order;
router.get("/statuses",cache.get,StatusController.getAllStatus,cache.set);
router.get("/status/:status_id",cache.get,StatusController.getOneStatus,cache.set);
router.post("/status/create",StatusController.create);
router.patch("/status/update/:status_id",StatusController.update);
router.delete("/status/delete/:status_id",StatusController.Delete);
router.delete("/status/destroy/:status_id",StatusController.Destroy);

//Orders
router.get("/orders",cache.get,OrdersController.getAllOrders,cache.set);
router.get("/orders/new",cache.get,OrdersController.getAllNewOrders,cache.set);
router.get("/orders/delivered",cache.get,OrdersController.getAllDeliveredOrders,cache.set);
router.get("/orders/:user_id",cache.get,OrdersController.getOneUserOrders,cache.set);
router.get("/order/:order_id",cache.get,OrdersController.getOneOrder,cache.set);
router.post("/order/create",OrdersController.create);
router.patch("/order/update/:order_id",OrdersController.update);
router.patch("/order/status/:order_id",OrdersController.updateStatus);
router.patch("/order/deliveri/:order_id",OrdersController.updateDelivery);
router.patch("/order/canceled/:order_id",OrdersController.updateCanceled);
router.delete("/order/delete/:order_id",OrdersController.Delete);
router.delete("/order/destroy/:order_id",OrdersController.Destroy);

//Favourites
router.get("/favourites",cache.get,FavouritesController.getAllFavorites,cache.set);
router.get("/favourites/product/:product_id",cache.get,FavouritesController.getProductFavor,cache.set);
router.get("/favourites/user/:user_id",cache.get,FavouritesController.getUserFavor,cache.set);
router.post("/favourites/create",FavouritesController.create);
router.post("/favourites/delete",FavouritesController.Delete);
router.delete("/favourites/delete",FavouritesController.Delete);

// Sebet 
router.get("/user/sebet/:user_id",cache.get,SebetController.getAll,cache.set);
router.post("/user/sebet/create/:user_id",SebetController.create);
router.post("/user/sebet/inc/:sebet_id",SebetController.inc);
router.post("/user/sebet/dec/:sebet_id",SebetController.dec);
router.post("/user/sebet/update/:sebet_id",SebetController.update);
router.post("/user/sebet/delete/:sebet_id",SebetController.Delete);

//Notifications 
router.get("/notifications",cache.get,NotificationController.getAll,cache.set);
router.get("/notification/:id",cache.get,NotificationController.getOne,cache.set);
router.post("/notification/create",NotificationController.Create);
router.patch("/notification/update/:id",NotificationController.Update);
router.delete("/notification/delete/:id",NotificationController.Delete);


// Welayatlar 
router.get("/welayatlar",cache.get,WelayatlarController.getAll, cache.set);
router.post("/welayat/create",WelayatlarController.create);
router.patch("/welayat/update/:id",WelayatlarController.Update);
router.delete("/welayat/delete/:id",WelayatlarController.Delete);

//Kategoty of Markets
router.get("/kategoryOfMarkets/:WelayatlarId",cache.get,KategoryOfMarketsController.getAll,cache.set);
router.get("/kategoryOfMarkets/one/:id",cache.get,KategoryOfMarketsController.getOne,cache.set);
router.post("/kategoryOfMarkets/create/:WelayatlarId",KategoryOfMarketsController.create);
router.patch("/kategoryOfMarkets/update/:id",KategoryOfMarketsController.Update);
router.delete("/kategoryOfMarkets/delete/:id",KategoryOfMarketsController.Delete);
router.delete("/kategoryOfMarkets/destroy/:id",KategoryOfMarketsController.Destroy);


// Razmerler
router.get("/razmerler/:ProductId",cache.get,RazmerlerController.getAll,cache.set);
router.post("/razmerler/create/:ProductId",RazmerlerController.create);
router.patch("/razmerler/update/:id",RazmerlerController.Update);
router.delete("/razmerler/delete/:id",RazmerlerController.Delete);
router.delete("/razmerler/destroy/:id",RazmerlerController.Destroy);

// Renkler
router.get("/renkler/:ProductId",cache.get,RenklerController.getAll,cache.set);
router.post("/renkler/create/:ProductId",RenklerController.create);
router.patch("/renkler/update/:id",RenklerController.Update);
router.delete("/renkler/delete/:id",RenklerController.Delete);
router.delete("/renkler/destroy/:id",RenklerController.Destroy);


//Brand Kategory
router.get("/brand/kategory/:welayatId",cache.get,BrandsKategoryController.getAll,cache.set);
router.post("/brand/kategory/create",BrandsKategoryController.create);
router.patch("/brand/kategory/update/:id",BrandsKategoryController.update);
router.delete("/brand/kategory/delete/:id",BrandsKategoryController.Delete);
router.delete("/brand/kategory/destroy/:id",BrandsKategoryController.Destroy);

//Brands
router.get("/brands",cache.get,BrandsController.getAll,cache.set);
router.post("/brand/create",BrandsController.create);
router.patch("/brand/update/:id",BrandsController.update);
router.delete("/brand/delete/:id",BrandsController.Delete);
router.delete("/brand/destroy/:id",BrandsController.Destroy);

//CashBack 
router.get("/cashback/user",cache.get,CashBackController.getOneUserCashBacks,cache.set);
router.post("/cashback/create",CashBackController.create);


//fcmTokens
router.post("/fcmNotification/topic",FcmNotificationsController.fcmSendWithTopic);
router.post("/fcmNotification/allUser",FcmNotificationsController.senAllUser);
router.post("/fcmNotification/oneUser",FcmNotificationsController.sendOneUser);



// For Token

function verifyToken(req, res, next) {
    const bearerHeader =
      req.headers["authorization"] || req.headers["Authorization"];
    if (typeof bearerHeader !== "undefined") {
      const bearer = bearerHeader.split(" ");
      const bearerToken = bearer[1];
  
      jwt.verify(bearerToken, Func.Secret(), (err, authData) => {
        if (err) {
          res.json("err");
          console.log(err);
          
        } else {
          req.id = authData.id;
        }
      });
      next();
    } else {
      res.send("<center><h2>This link was not found! :(</h2></center>");
    }
  }
  
  module.exports = router;