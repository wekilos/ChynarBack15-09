// import sequelize 
const { CHAR } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Brands = require("./brand");
const BrandsKategory = require("./brandKategory");
const MarketKategoriya = require("./marketKategoriya");
const Markets = require("./markets");
const MarketSubKategoriya = require("./marketSubKategoriya");
const Products = require("./products");
const ReseptKategoriya = require("./reseptKategory");
const ReseptProduct = require("./reseptProducts");
const Resepts = require("./resepts");
const ReseptSubKategoriya = require("./reseptSubKategory");

var Sowgat = sequelize.define(
    "Sowgat",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        description_tm:Sequelize.STRING,
        description_ru:Sequelize.STRING,
        description_en:Sequelize.STRING,
        name_tm:Sequelize.STRING,
        name_ru:Sequelize.STRING,
        name_en:Sequelize.STRING,
        startBaha:Sequelize.DOUBLE,
        endBaha:Sequelize.DOUBLE,
        sany:Sequelize.DOUBLE,
        productSany:Sequelize.DOUBLE,
        sowgat_until:Sequelize.DATE,
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        surat3:Sequelize.STRING,
        active:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

    Sowgat.belongsTo(Markets);
    Markets.hasMany(Sowgat);

    Sowgat.belongsTo(MarketKategoriya);
    MarketKategoriya.hasMany(Sowgat);

    Sowgat.belongsTo(MarketSubKategoriya);
    MarketSubKategoriya.hasMany(Sowgat);

    Sowgat.belongsTo(Products);
    Products.hasMany(Sowgat);

    Sowgat.belongsTo(Resepts);
    Resepts.hasMany(Sowgat);

    Sowgat.belongsTo(ReseptKategoriya);
    ReseptKategoriya.hasMany(Sowgat);

    Sowgat.belongsTo(ReseptSubKategoriya);
    ReseptSubKategoriya.hasMany(Sowgat);

    Sowgat.belongsTo(ReseptProduct);
    ReseptProduct.hasMany(Sowgat);

    Sowgat.belongsTo(BrandsKategory);
    BrandsKategory.hasMany(Sowgat);

    Sowgat.belongsTo(Brands);
    Brands.hasMany(Sowgat);







module.exports = Sowgat;