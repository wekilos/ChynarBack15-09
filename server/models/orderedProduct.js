// import sequelize 
const { CHAR } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Products = require("./products");
const Orders = require("./orders");
const ReseptProduct = require("./reseptProducts");

var OrderedProduct = sequelize.define(
    "OrderedProduct",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        amount:Sequelize.INTEGER,
        baha:Sequelize.DOUBLE,
        razmer:{
            type:Sequelize.STRING,
            default:""},
        renk:{
            type:Sequelize.STRING,
            default:""},
        delivered:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


OrderedProduct.belongsTo(Products);
Products.hasMany(OrderedProduct);

OrderedProduct.belongsTo(ReseptProduct);
ReseptProduct.hasMany(OrderedProduct);

OrderedProduct.belongsTo(Orders);
Orders.hasMany(OrderedProduct);

module.exports = OrderedProduct;