// import sequelize 
const { CHAR } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Brands = require("./brand");
const BrandsKategory = require("./brandKategory");
const MarketKategoriya = require("./marketKategoriya");
const Markets = require("./markets");
const MarketSubKategoriya = require("./marketSubKategoriya");
const Products = require("./products");
const ReseptKategoriya = require("./reseptKategory");
const ReseptProduct = require("./reseptProducts");
const Resepts = require("./resepts");
const ReseptSubKategoriya = require("./reseptSubKategory");

var Reklama = sequelize.define(
    "Reklama",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        description_tm:Sequelize.STRING,
        description_ru:Sequelize.STRING,
        description_en:Sequelize.STRING,
        name_tm:Sequelize.STRING,
        name_ru:Sequelize.STRING,
        name_en:Sequelize.STRING,
        startBaha:Sequelize.DOUBLE,
        endBaha:Sequelize.DOUBLE,
        add_until:Sequelize.DATE,
        add_where:Sequelize.STRING,
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        surat3:Sequelize.STRING,
        active:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

    Reklama.belongsTo(Markets);
    Markets.hasMany(Reklama);

    Reklama.belongsTo(MarketKategoriya);
    MarketKategoriya.hasMany(Reklama);

    Reklama.belongsTo(MarketSubKategoriya);
    MarketSubKategoriya.hasMany(Reklama);

    Reklama.belongsTo(Products);
    Products.hasMany(Reklama);


    Reklama.belongsTo(Resepts);
    Resepts.hasMany(Reklama);

    Reklama.belongsTo(ReseptKategoriya);
    ReseptKategoriya.hasMany(Reklama);

    Reklama.belongsTo(ReseptSubKategoriya);
    ReseptSubKategoriya.hasMany(Reklama);

    Reklama.belongsTo(ReseptProduct);
    ReseptProduct.hasMany(Reklama);

    Reklama.belongsTo(BrandsKategory);
    BrandsKategory.hasMany(Reklama);

    Reklama.belongsTo(Brands);
    Brands.hasMany(Reklama);


module.exports = Reklama;