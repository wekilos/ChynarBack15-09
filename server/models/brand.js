// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const BrandsKategory = require("./brandKategory");
const BrandsSubKategory = require("./brandSubKategory");
const Welayatlar = require("./welayatlar");

var Brands = sequelize.define(
    "Brand",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        surat:Sequelize.STRING,
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

Brands.belongsTo(BrandsKategory);
BrandsKategory.hasMany(Brands);

Brands.belongsTo(BrandsSubKategory);
BrandsSubKategory.hasMany(Brands);

Brands.belongsTo(Welayatlar);
Welayatlar.hasMany(Brands);

module.exports = Brands;