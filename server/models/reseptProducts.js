// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const ReseptKategoriya = require("./reseptKategory");
const Resepts = require("./resepts");
const ReseptSubKategoriya = require("./reseptSubKategory");
const Welayatlar = require("./welayatlar");

var ReseptProduct = sequelize.define(
    "ReseptProduct",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        description_tm:Sequelize.TEXT,
        description_ru:Sequelize.TEXT,
        description_en:Sequelize.TEXT,
        price_usd:Sequelize.DOUBLE,
        price:Sequelize.DOUBLE,
        is_sale:{type:Sequelize.BOOLEAN, default:false},
        sale_until:Sequelize.DATE,
        product_code:Sequelize.BIGINT,
        bahaPrasent:Sequelize.INTEGER,
        is_active:{type:Sequelize.BOOLEAN,default:true},
        total_amount:Sequelize.INTEGER,
        sale_price:Sequelize.DOUBLE,
        view_count:Sequelize.INTEGER,
        is_valyuta_price:{type:Sequelize.BOOLEAN,default:false},
        search:Sequelize.STRING,
        sany:Sequelize.INTEGER,
        minSany:Sequelize.INTEGER,
        nacheAdamlyk:Sequelize.BIGINT,
        is_new:{type:Sequelize.BOOLEAN,default:false},
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        surat3:Sequelize.STRING,
        active:{
            type:Sequelize.BOOLEAN,
            default:true,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

ReseptProduct.belongsTo(Welayatlar);
Welayatlar.hasMany(ReseptProduct);

ReseptProduct.belongsTo(Resepts);
Resepts.hasMany(ReseptProduct);

ReseptProduct.belongsTo(ReseptKategoriya);
ReseptKategoriya.hasMany(ReseptProduct);

ReseptProduct.belongsTo(ReseptSubKategoriya);
ReseptSubKategoriya.hasMany(ReseptProduct);

module.exports = ReseptProduct;