// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const BrandsKategory = require("./brandKategory");

var BrandsSubKategory = sequelize.define(
    "BrandsSubKategory",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

BrandsSubKategory.belongsTo(BrandsKategory);
BrandsKategory.hasMany(BrandsSubKategory);


module.exports = BrandsSubKategory;