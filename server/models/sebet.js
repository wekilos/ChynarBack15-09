// import sequelize 
const { CHAR } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

var Sebet = sequelize.define(
    "Sebet",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        user_id:Sequelize.INTEGER,
        product_id:Sequelize.INTEGER,
        sany:Sequelize.INTEGER,
        renk:Sequelize.STRING,
        razmer:Sequelize.STRING,
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        gelenBaha:Sequelize.DOUBLE,
        price_usd:Sequelize.DOUBLE,
        price:Sequelize.DOUBLE,
        sale_price:Sequelize.DOUBLE,
        step:Sequelize.CHAR,
        product_code:Sequelize.BIGINT,
        bahaPrasent:Sequelize.INTEGER,
        article_tm:Sequelize.STRING(250),
        article_ru:Sequelize.STRING(250),
        article_en:Sequelize.STRING(250),
        description_tm:Sequelize.TEXT,
        description_ru:Sequelize.TEXT,
        description_en:Sequelize.TEXT,
        is_sale:{type:Sequelize.BOOLEAN, default:false},
        sale_until:Sequelize.DATE,
        is_active:{type:Sequelize.BOOLEAN,default:true},
        total_amount:Sequelize.INTEGER,
        view_count:Sequelize.INTEGER,
        is_valyuta_price:{type:Sequelize.BOOLEAN,default:false},
        search:Sequelize.STRING,
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        surat3:Sequelize.STRING,
        is_new:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);





module.exports = Sebet;