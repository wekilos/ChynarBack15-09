const Sequelize = require("sequelize");
const sequelize = require("../../config/db");

var Configs = sequelize.define(
    "Config",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        delivery_price:Sequelize.INTEGER,
        onlinePayment_active:Sequelize.BOOLEAN, 
        footer_phone_number:Sequelize.CHAR,
        currency_exchange:Sequelize.STRING,
        work_start_time:Sequelize.DATE,
        work_end_time:Sequelize.DATE,
        deleted:{type:Sequelize.BOOLEAN,default:false},
        
    },
    {
        timestamps: true,
    }
);



module.exports = Configs;