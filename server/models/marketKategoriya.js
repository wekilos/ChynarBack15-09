const Sequelize = require("sequelize");
const sequelize = require("../../config/db");
const Markets = require("./markets");

var MarketKategoriya = sequelize.define(
    "MarketKategoriya",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        active:{
            type:Sequelize.BOOLEAN,
            default:true,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


MarketKategoriya.belongsTo(Markets);
Markets.hasMany(MarketKategoriya);

module.exports = MarketKategoriya;