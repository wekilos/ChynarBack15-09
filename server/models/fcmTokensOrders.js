// import sequelize 
const { CHAR, INTEGER } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
var Orders = require("./orders");

var fcmTokenOrder = sequelize.define(
    "FcmTokenOrder",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        type:Sequelize.STRING,
        fcmToken:Sequelize.TEXT,
        
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


fcmTokenOrder.belongsTo(Orders);
Orders.hasMany(fcmTokenOrder);



module.exports = fcmTokenOrder;