// import sequelize 
const { DATE } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const MarketKategoriya = require("./marketKategoriya");

const Markets = require("./markets");
const Units = require("../models/units");
const Configs = require("./config");
const Welayatlar = require("./welayatlar");
const Brands = require("./brand");
const MarketSubKategoriya = require("./marketSubKategoriya");
var Products = sequelize.define(
    "Product",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        gelenBaha:Sequelize.DOUBLE,
        price_usd:Sequelize.DOUBLE,
        price:Sequelize.DOUBLE,
        sale_price:Sequelize.DOUBLE,
        step:Sequelize.CHAR,
        product_code:Sequelize.BIGINT,
        bahaPrasent:Sequelize.INTEGER,
        article_tm:Sequelize.STRING(250),
        article_ru:Sequelize.STRING(250),
        article_en:Sequelize.STRING(250),
        description_tm:Sequelize.TEXT,
        description_ru:Sequelize.TEXT,
        description_en:Sequelize.TEXT,
        is_sale:{type:Sequelize.BOOLEAN, default:false},
        sale_until:Sequelize.DATE,
        is_active:{type:Sequelize.BOOLEAN,default:true},
        total_amount:Sequelize.INTEGER,
        view_count:Sequelize.INTEGER,
        is_valyuta_price:{type:Sequelize.BOOLEAN,default:false},
        search:Sequelize.STRING,
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        surat3:Sequelize.STRING,
        is_new:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


Products.belongsTo(Markets);
Markets.hasMany(Products);

Products.belongsTo(Welayatlar);
Welayatlar.hasMany(Products);

Products.belongsTo(MarketKategoriya);
MarketKategoriya.hasMany(Products);

Products.belongsTo(MarketSubKategoriya);
MarketSubKategoriya.hasMany(Products);

Products.belongsTo(Units);
Units.hasMany(Products);

Products.belongsTo(Configs);

Products.belongsTo(Brands);
Brands.hasMany(Products);

Products.belongsTo(Brands);
Brands.hasMany(Products);

module.exports = Products;