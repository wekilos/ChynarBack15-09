// import sequelize 
const { CHAR } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Users = require("./users");

var Posts = sequelize.define(
    "Post",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        description_tm:Sequelize.STRING,
        description_ru:Sequelize.STRING,
        description_en:Sequelize.STRING,
        slug_tm:Sequelize.STRING,
        slug_ru:Sequelize.STRING,
        slug_en:Sequelize.STRING,
        active:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

Posts.belongsTo(Users);
Users.hasMany(Posts);



module.exports = Posts;