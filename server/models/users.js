// import sequelize 
const { CHAR, INTEGER } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

 const UserType = require("./userType");
// const Orders = require("./orders");
// const Address = require("./address");
// const Favourites = require("./favourites");

var Users = sequelize.define(
    "Users",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        fname:Sequelize.STRING(100),
        lastname:Sequelize.STRING(100),
        phoneNumber:Sequelize.BIGINT,
        password:Sequelize.STRING,
        primary_addres_id:INTEGER,
        cashBacks:Sequelize.DOUBLE,
        marketIds:Sequelize.STRING,
        dastawshik:{type:Sequelize.BOOLEAN,default:false,},
        admin:{type:Sequelize.BOOLEAN,default:false},
        subAdmin:{type:Sequelize.BOOLEAN,default:false},
        user:{type:Sequelize.BOOLEAN,default:true},
        operator:{type:Sequelize.BOOLEAN,default:false,},
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


Users.belongsTo(UserType);
UserType.hasMany(Users);



module.exports = Users;