// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Markets = require("./markets");
var UserType = sequelize.define(
    "UserType",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        type_tm:Sequelize.STRING,
        type_ru:Sequelize.STRING,
        type_en:Sequelize.STRING,
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


UserType.belongsTo(Markets);
Markets.hasMany(UserType);

module.exports = UserType;