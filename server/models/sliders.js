// import sequelize 
const { CHAR } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

var Sliders = sequelize.define(
    "Slider",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        title_tm:Sequelize.STRING,
        title_ru:Sequelize.STRING,
        title_en:Sequelize.STRING,
        link:Sequelize.STRING,
        photo_url:Sequelize.STRING,
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);




module.exports = Sliders;