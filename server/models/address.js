// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

const Markets = require("./markets");
const Users = require("./users");


var Address = sequelize.define(
    "Address",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        rec_name:Sequelize.STRING,
        rec_address:Sequelize.STRING,
        rec_number:Sequelize.BIGINT,
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    
        
    {
        timestamps: true,
    }
    );





Address.belongsTo(Users);
Users.hasMany(Address);



module.exports = Address;