// import sequelize 
const { CHAR } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Address = require("./address");
const Users = require("./users");
const Status = require("./status");

const Markets = require("./markets");
const Resepts = require("./resepts");
const Sowgat = require("./sowgat");

var Orders = sequelize.define(
    "Order",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        sum:Sequelize.DOUBLE,
        sany:Sequelize.INTEGER,
        is_cash:{type:Sequelize.BOOLEAN, default:true,},
        is_payed:{type:Sequelize.BOOLEAN, default:false,},
        is_sowgat:{type:Sequelize.BOOLEAN, default:false,},
        cashBack:{type:Sequelize.BOOLEAN, default:false,},
        cashBackPrice:Sequelize.DOUBLE,
        cashBackMoney:Sequelize.DOUBLE,
        order_date_time:Sequelize.DATE,
        delivery_date_time:Sequelize.DATE,
        delivered:{type:Sequelize.BOOLEAN, default:false,},
        canceled:{type:Sequelize.BOOLEAN, default:false,},
        delivery_time_status:Sequelize.INTEGER,
        active:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
        user_deleted:{type:Sequelize.BOOLEAN,default:false},
        dastawshikId:Sequelize.STRING,
        operatorId:Sequelize.STRING,
        adminId:Sequelize.STRING,
    },
    {
        timestamps: true,
    }
);


Orders.belongsTo(Users);
Users.hasMany(Orders);

Orders.belongsTo(Address);
Address.hasMany(Orders);

Orders.belongsTo(Status);
Status.hasMany(Orders);

Orders.belongsTo(Markets);
Markets.hasMany(Orders);

Orders.belongsTo(Resepts);
Resepts.hasMany(Orders);

Orders.belongsTo(Sowgat);
Sowgat.hasMany(Orders);

module.exports = Orders;