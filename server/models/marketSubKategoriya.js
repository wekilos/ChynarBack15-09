const Sequelize = require("sequelize");
const sequelize = require("../../config/db");
const MarketKategoriya = require("./marketKategoriya");

var MarketSubKategoriya = sequelize.define(
    "MarketSubKategoriya",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        active:{
            type:Sequelize.BOOLEAN,
            default:false,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


MarketSubKategoriya.belongsTo(MarketKategoriya);
MarketKategoriya.hasMany(MarketSubKategoriya);

module.exports = MarketSubKategoriya;