// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

const Markets = require("./markets");

var MarketAddress = sequelize.define(
    "MarketAddress",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING,
        name_ru:Sequelize.STRING,
        name_en:Sequelize.STRING,
        description_tm:Sequelize.TEXT,
        description_ru:Sequelize.TEXT,
        description_en:Sequelize.TEXT,
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    
        
    {
        timestamps: true,
    }
    );





MarketAddress.belongsTo(Markets);
Markets.hasMany(MarketAddress);



module.exports = MarketAddress;