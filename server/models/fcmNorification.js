// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");


var fcmNotification = sequelize.define(
    "FcmNotification",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING,
        name_ru:Sequelize.STRING,
        name_en:Sequelize.STRING,
        text_tm:Sequelize.TEXT,
        text_ru:Sequelize.TEXT,
        text_en:Sequelize.TEXT,
        image:Sequelize.TEXT,
        link:Sequelize.TEXT,
        device:Sequelize.STRING,
        active:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    
        
    {
        timestamps: true,
    }
    );




module.exports = fcmNotification;