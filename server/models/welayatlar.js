// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");

var Welayatlar = sequelize.define(
    "Welayatlar",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        short_name_tm:Sequelize.STRING(100),
        short_name_ru:Sequelize.STRING(100),
        short_name_en:Sequelize.STRING(100),
        active:{type:Sequelize.BOOLEAN,default:false},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);



module.exports = Welayatlar;