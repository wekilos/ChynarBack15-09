// import sequelize 
const { CHAR, INTEGER } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
var Users = require("./users");

var fcmToken = sequelize.define(
    "FcmToken",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        type:Sequelize.STRING,
        fcmToken:Sequelize.TEXT,
        
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


fcmToken.belongsTo(Users);
Users.hasMany(fcmToken);



module.exports = fcmToken;