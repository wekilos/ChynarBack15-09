// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Products = require("./products");
const Users = require("./users");

var Favourites = sequelize.define(
    "Favourite",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        sany:Sequelize.INTEGER,
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


Favourites.belongsTo(Products);
Products.hasMany(Favourites);
 
Favourites.belongsTo(Users);
Users.hasMany(Favourites);

module.exports = Favourites;