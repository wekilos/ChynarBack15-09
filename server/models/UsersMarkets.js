// import sequelize 
const { CHAR, INTEGER } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Markets = require("./markets");
const Users = require("./users");

var UserMarkets = sequelize.define(
    "UserMarket",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        active:{type:Sequelize.BOOLEAN,default:false},
        marketId:Sequelize.INTEGER,
        permissions:Sequelize.STRING,
        marketIds:Sequelize.STRING,
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


UserMarkets.belongsTo(Users);
Users.hasMany(UserMarkets);

UserMarkets.belongsTo(Markets);
Markets.hasMany(UserMarkets);



module.exports = UserMarkets;