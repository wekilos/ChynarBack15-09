const Sequelize = require("sequelize");
const sequelize = require("../../config/db");
const Config = require("./config");

var Currency = sequelize.define(
    "Currency",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING,
        name_ru:Sequelize.STRING,
        name_en:Sequelize.STRING,
        deleted:{type:Sequelize.BOOLEAN,default:false},
        
    },
    {
        timestamps: true,
    }
);


Currency.belongsTo(Config);
Config.hasMany(Currency);

module.exports = Currency;