var Sequelize = require("sequelize");
var sequelize = require("../../config/db");

var Markets = require("../models/markets");
const Users = require("./users");

const PhoneNumbers = sequelize.define(
    "PhoneNumber",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        phoneNumber:Sequelize.BIGINT,
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

PhoneNumbers.belongsTo(Markets);
Markets.hasMany(PhoneNumbers);

PhoneNumbers.belongsTo(Users);
Users.hasMany(PhoneNumbers);


module.exports = PhoneNumbers;