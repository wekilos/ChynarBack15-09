const Sequelize = require("sequelize");
const sequelize = require("../../config/db"); 
const ResepKategoriya = require("./reseptKategory");
const Resepts = require("./resepts");

var ReseptSubKategoriya = sequelize.define(
    "ReseptSubKategoriya",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        active:{
            type:Sequelize.BOOLEAN,
            default:false,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


ReseptSubKategoriya.belongsTo(ResepKategoriya);
ResepKategoriya.hasMany(ReseptSubKategoriya);

ReseptSubKategoriya.belongsTo(Resepts);
Resepts.hasMany(ReseptSubKategoriya);

module.exports = ReseptSubKategoriya;