const Sequelize = require("sequelize");
const sequelize = require("../../config/db"); 
const Resepts = require("./resepts");

var ReseptKategoriya = sequelize.define(
    "ReseptKategoriya",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        active:{
            type:Sequelize.BOOLEAN,
            default:false,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


ReseptKategoriya.belongsTo(Resepts);
Resepts.hasMany(ReseptKategoriya);

module.exports = ReseptKategoriya;