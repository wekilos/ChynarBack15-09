// import sequelize 
const { CHAR, INTEGER } = require("sequelize");
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Markets = require("./markets");
const Resepts = require("./resepts");
const Users = require("./users");

var CashBack = sequelize.define(
    "CashBack",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        cashBacks:Sequelize.DOUBLE,
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


CashBack.belongsTo(Users);
Users.hasMany(CashBack);

CashBack.belongsTo(Markets);
Markets.hasMany(CashBack);

CashBack.belongsTo(Resepts);
Resepts.hasMany(CashBack);


module.exports = CashBack;