const Sequelize = require("sequelize");
const sequelize = require("../../config/db"); 
const ReseptProduct = require("./reseptProducts");

var ReseptIngridient = sequelize.define(
    "ReseptIngridient",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        description_tm:Sequelize.TEXT,
        description_ru:Sequelize.TEXT,
        description_en:Sequelize.TEXT,
        price:Sequelize.DOUBLE,
        total_amount:Sequelize.INTEGER,
        sale_price:Sequelize.DOUBLE,
        view_count:Sequelize.INTEGER,
        search:Sequelize.STRING,
        sany:Sequelize.INTEGER,
        minSany:Sequelize.INTEGER,
        nacheAdamlyk:Sequelize.BIGINT,
        is_new:{type:Sequelize.BOOLEAN,default:false},
        is_sale:{type:Sequelize.BOOLEAN,default:false},
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        surat3:Sequelize.STRING,
        active:{
            type:Sequelize.BOOLEAN,
            default:true,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);


ReseptIngridient.belongsTo(ReseptProduct);
ReseptProduct.hasMany(ReseptIngridient);

module.exports = ReseptIngridient;