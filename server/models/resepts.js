// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Welayatlar = require("./welayatlar");

var Resepts = sequelize.define(
    "Resept",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        description_tm:Sequelize.TEXT,
        description_ru:Sequelize.TEXT,
        description_en:Sequelize.TEXT,
        addres_tm:Sequelize.TEXT,
        addres_ru:Sequelize.TEXT,
        addres_en:Sequelize.TEXT,
        tel:Sequelize.BIGINT,
        sany:Sequelize.INTEGER,
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        dastawkaStartI:Sequelize.TIME,
        dastawkaEndI:Sequelize.TIME,
        dastawkaStartII:Sequelize.TIME,
        dastawkaEndII:Sequelize.TIME,
        dastawkaPrice:Sequelize.DOUBLE,
        currency_exchange:Sequelize.DOUBLE,
        cashBackPrice:Sequelize.DOUBLE,
        cashBackPrasent:Sequelize.DOUBLE,
        cashBack:{type:Sequelize.BOOLEAN,default:false},
        is_cart:{
            type:Sequelize.BOOLEAN,
            default:false
        },
        is_online:{
            type:Sequelize.BOOLEAN,
            default:false
        },
        is_aksiya:{
            type:Sequelize.BOOLEAN,
            default:false
        },
        aksiyaLimit:Sequelize.INTEGER,
        active:{
            type:Sequelize.BOOLEAN,
            default:true,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

Resepts.belongsTo(Welayatlar);
Welayatlar.hasMany(Resepts);

module.exports = Resepts;