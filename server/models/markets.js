// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const KategoriyaOfMarkets = require("./KategoriyaOfMarkets");
const Welayatlar = require("./welayatlar");

var Markets = sequelize.define(
    "Market",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        addres_tm:Sequelize.TEXT,
        addres_ru:Sequelize.TEXT,
        addres_en:Sequelize.TEXT,
        tel:Sequelize.BIGINT,
        description_tm:Sequelize.TEXT,
        description_ru:Sequelize.TEXT,
        description_en:Sequelize.TEXT,
        surat:Sequelize.STRING,
        dastawkaStartI:Sequelize.TIME,
        dastawkaEndI:Sequelize.TIME,
        dastawkaStartII:Sequelize.TIME,
        dastawkaEndII:Sequelize.TIME,
        dastawkaPrice:Sequelize.DOUBLE,
        currency_exchange:Sequelize.DOUBLE,
        is_cart:{
            type:Sequelize.BOOLEAN,
            default:false
        },
        is_online:{
            type:Sequelize.BOOLEAN,
            default:false
        },
        is_aksiya:{
            type:Sequelize.BOOLEAN,
            default:false
        },
        aksiyaLimit:Sequelize.INTEGER,
        active:{type:Sequelize.BOOLEAN,default:true},
        deleted:{type:Sequelize.BOOLEAN,default:false},
        cashBackPrice:Sequelize.DOUBLE,
        cashBackPrasent:Sequelize.DOUBLE,
        cashBack:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

Markets.belongsTo(Welayatlar);
Welayatlar.hasMany(Markets);

Markets.belongsTo(KategoriyaOfMarkets);
KategoriyaOfMarkets.hasMany(Markets);


module.exports = Markets;