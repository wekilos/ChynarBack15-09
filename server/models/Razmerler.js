// import sequelize 
var Sequelize = require("sequelize");

//importing connection database
var sequelize = require("../../config/db");
const Products = require("./products");

var Razmerler = sequelize.define(
    "Razmerler",
    {
        id:{
            type:Sequelize.INTEGER,
            primaryKey:true,
            autoIncrement:true,
        },
        name_tm:Sequelize.STRING(100),
        name_ru:Sequelize.STRING(100),
        name_en:Sequelize.STRING(100),
        sany:Sequelize.INTEGER,
        surat:Sequelize.STRING,
        surat1:Sequelize.STRING,
        surat2:Sequelize.STRING,
        active:{
            type:Sequelize.BOOLEAN,
            default:true,
        },
        deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
        timestamps: true,
    }
);

Razmerler.belongsTo(Products);
Products.hasMany(Razmerler);

module.exports = Razmerler;