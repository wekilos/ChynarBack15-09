var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
var UserType = require("./userType");

var Permission = sequelize.define(
    "Permission",
    {
      id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
  
      number: Sequelize.INTEGER,
      active:{type:Sequelize.BOOLEAN,default:true},
      deleted:{type:Sequelize.BOOLEAN,default:false},
    },
    {
      timestamps: true,
    }
  );

  Permission.belongsTo(UserType);
  UserType.hasMany(Permission);

  module.exports=Permission;