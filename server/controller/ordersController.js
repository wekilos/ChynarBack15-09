var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Orders = require("../models/orders");
const OrderedProduct = require("../models/orderedProduct");
const Users = require("../models/users");
const Address = require("../models/address");
const Status = require("../models/status");
const Sebet = require("../models/sebet");
const Products = require("../models/products");
const Markets = require("../models/markets")
const Units = require("../models/units");
const Razmerler = require("../models/Razmerler");
const Renkler = require("../models/Renkler");
const Configs = require("../models/config");
const fcmTokenOrder = require("../models/fcmTokensOrders");
const { admin } = require("../functions/fcm");
const Op = Sequelize.Op;

const orders_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Orders.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}




const getAllOrders = async(req,res)=>{
    const { all, statusId, MarketId } = req.query;
    console.log("all,statusesId",all,statusId);
    let all1 = parseInt(all);
  var All = all1
    ? {
        [Op.or]: [
          { id: { [Op.eq]: `${all}` } },
          { sum: { [Op.eq]: `${all}` } },
        ],
      }
    : null;

  var StatusId = statusId
    ? {
        StatusId: { [Op.eq]: `${statusId}` },
      }
    : null;
    var MarketID = MarketId
    ? {
        MarketId: { [Op.eq]: `${MarketId}` },
        }
    : null;
    Orders.findAll({
        include:[
            { 
                model:OrderedProduct,
                attributes:["id","amount","razmer","renk","delivered","ProductId","createdAt","updatedAt"],
                include:[{
                    model:Products,
                    attributes:["id","name_tm","surat","surat1","surat2","surat3","sale_price","price","is_sale","is_valyuta_price","article_tm","description_tm"],
                    include:[{
                        model:Units,
                        attributes:["id","name_tm","name_ru","name_en"]},
                        {
                        model:Markets,
                        attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange","dastawkaPrice"],
                        },
                        {
                        model:Razmerler,
                        attributes:["id","name_tm","name_ru","name_en","sany","surat","surat1","surat2","active"],
                        // where:{deleted:false,active:true}
                        },
                        {
                        model:Renkler,
                        attributes:["id","name_tm","name_ru","name_en","sany","surat","surat1","surat2","active"],
                        // where:{deleted:false,active:true}
                        },
                        
                    ]
                }]
                
            },
            {
                model:Status,
                attributes:["id","name_tm","name_ru","name_en","createdAt","updatedAt"]
            },
            {
                model:Users,
                attributes:["id","fname","lastname","phoneNumber","primary_addres_id","UserTypeId","createdAt","updatedAt"],
                include:[{
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                    
                }]
            },
            {
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                   
                
            },
            {
                model:fcmTokenOrder,
                attributes:["id","fcmToken","type"]
                }
        ],
        where: {
            [Op.and]: [All, StatusId,MarketID,{delivered:false},{active:true},{deleted:false}],
          },
        order: [
            ['id', 'DESC'],
        ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error!");
    })
}



const getAllNewOrders = async(req,res)=>{
    const { all,MarketId } = req.query;
    console.log("all,statusesId",all);
    let all1 = parseInt(all);
  var All = all1
    ? {
        [Op.or]: [
          { id: { [Op.eq]: `${all}` } },
          { sum: { [Op.eq]: `${all}` } },
        ],
      }
    : null;
    var MarketID = MarketId
    ?{
        MarketId : {[Op.eq]:`${MarketId}`}
    }
    : null;

    Orders.findAll({
        include:[
            {
                model:OrderedProduct,
                attributes:["id","amount","razmer","renk","delivered","ProductId","createdAt","updatedAt"],
                include:[{
                    model:Products,
                    attributes:["id","name_tm","surat","surat1","surat2","surat3","sale_price","price","is_sale","is_valyuta_price","article_tm","description_tm"],
                    include:[{
                        model:Units,
                        attributes:["id","name_tm","name_ru","name_en"]},
                        {
                        model:Markets,
                        attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange","dastawkaPrice"]
                        },
                        {
                        model:Razmerler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        {
                        model:Renkler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        // {
                        // model:Configs,
                        // attributes:["id","currency_exchange"]
                        // }
                    ]
                }]
                
            },
            {
                model:Status,
                attributes:["id","name_tm","name_ru","name_en","createdAt","updatedAt"]
            },
            {
                model:Users,
                attributes:["id","fname","lastname","phoneNumber","primary_addres_id","UserTypeId","createdAt","updatedAt"],
                include:[{
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                    
                }]
            },
            {
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                   
                
            }
        ],
        where: {
            [Op.and]: [All,MarketID, {active:false},{delivered:false},{deleted:false}],
          },
        order: [
            ['id', 'DESC'],
        ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error!");
    })
}

const getAllDeliveredOrders = async(req,res)=>{
    const { all, statusId, MarketId } = req.query;
    console.log("all,statusesId",all,statusId);
    let all1 = parseInt(all);
  var All = all1
    ? {
        [Op.or]: [
          { id: { [Op.eq]: `${all}` } },
          { sum: { [Op.eq]: `${all}` } },
        ],
      }
    : null;

  var StatusId = statusId
    ? {
        StatusId: { [Op.eq]: `${statusId}` },
      }
    : null;
    var MarketID = MarketId
    ?{
        MarketId : {[Op.eq]:`${MarketId}`}
    }
    : null;
    Orders.findAll({
        include:[
            {
                model:OrderedProduct,
                attributes:["id","amount","razmer","renk","delivered","ProductId","createdAt","updatedAt"],
                include:[{
                    model:Products,
                    attributes:["id","name_tm","surat","surat1","surat2","surat3","sale_price","price","is_sale","is_valyuta_price","article_tm","description_tm"],
                    include:[{
                        model:Units,
                        attributes:["id","name_tm","name_ru","name_en"]},
                        {
                        model:Markets,
                        attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange","dastawkaPrice"]
                        },
                        {
                        model:Razmerler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        {
                        model:Renkler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        // {
                        // model:Configs,
                        // attributes:["id","currency_exchange"]
                        // }
                    ]
                }]
                
            },
            {
                model:Status,
                attributes:["id","name_tm","name_ru","name_en","createdAt","updatedAt"]
            },
            {
                model:Users,
                attributes:["id","fname","lastname","phoneNumber","primary_addres_id","UserTypeId","createdAt","updatedAt"],
                include:[{
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                    
                }]
            },
            {
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                   
                
            }
        ],
        where: {
            [Op.and]: [All, StatusId,MarketID,{delivered:true},{active:false},{deleted:false}],
          },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error!");
    })
}

const getOneUserOrders = async(req,res)=>{
    const { user_id } = req.params;
    Orders.findAll({
        include:[
            {
                model:OrderedProduct,
                attributes:["id","amount","razmer","renk","delivered","ProductId","createdAt","updatedAt"],
                include:[{
                    model:Products,
                    attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","surat3","sale_price","price","is_sale","is_valyuta_price","article_tm","description_tm","description_ru","description_en"],
                    include:[{
                        model:Units,
                        attributes:["id","name_tm","name_ru","name_en"]},
                        {
                        model:Markets,
                        attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange","dastawkaPrice"]
                        },
                        {
                        model:Razmerler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        {
                        model:Renkler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        // {
                        // model:Configs,
                        // attributes:["id","currency_exchange"]
                        // }
                    ]
                }]
            },
            {
                model:Status,
                attributes:["id","name_tm","name_ru","name_en","createdAt","updatedAt"]
            },
            {
                model:Users,
                attributes:["id","fname","lastname","phoneNumber","primary_addres_id","UserTypeId","createdAt","updatedAt"],
                include:[{
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                    
                }]
            }
        ],
        where:{
            UserId:user_id
        },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const getOneOrder = async(req,res)=>{
    const { order_id } = req.params;
    const orders = await Orders.findOne({
        include:[
            {
                model:OrderedProduct,
                attributes:["id","amount","razmer","renk","delivered","ProductId","createdAt","updatedAt"],
                include:[{
                    model:Products,
                    attributes:["id","name_tm","name_en","name_ru","surat","surat1","surat2","surat3","sale_price","price","is_sale","article_tm","description_tm","description_ru","description_en"],
                    include:[{
                        model:Units,
                        attributes:["id","name_tm","name_ru","name_en"]},
                        {
                        model:Markets,
                        attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange","dastawkaPrice"]
                        },
                        {
                        model:Razmerler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        {
                        model:Renkler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        // {
                        // model:Configs,
                        // attributes:["id","currency_exchange"]
                        // }
                    ]
                }]
            },
            {
                model:Status,
                attributes:["id","name_tm","name_ru","name_en","createdAt","updatedAt"]
            },
            {
                model:Users,
                attributes:["id","fname","lastname","phoneNumber","primary_addres_id","UserTypeId","createdAt","updatedAt"],
                include:[{
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                    
                }]
            }
        ],
    },{where:{id:order_id}});
    console.log("orders:::::::::::::::::::::::",orders)
if(orders){
    Orders.findAll({
        include:[
            {
                model:OrderedProduct,
                attributes:["id","amount","razmer","renk","delivered","ProductId","createdAt","updatedAt"],
                include:[{
                    model:Products,
                    attributes:["id","name_tm","name_ru","name_en","sale_price","price","is_sale","is_valyuta_price","description_tm","description_ru","description_en","surat","surat1","surat2","surat3"],
                    include:[{
                        model:Units,
                        attributes:["id","name_tm","name_ru","name_en"]},
                        {
                        model:Markets,
                        attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange","dastawkaPrice"]
                        },
                        {
                        model:Razmerler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        {
                        model:Renkler,
                        attributes:["id","name_tm","name_ru","name_en","surat","surat1","surat2","active"]
                        },
                        // {
                        // model:Configs,
                        // attributes:["id","currency_exchange"]
                        // }
                    ]
                }]
            },
            {
                model:Status,
                attributes:["id","name_tm","name_ru","name_en","createdAt","updatedAt"]
            },
            {
                model:Users,
                attributes:["id","fname","lastname","phoneNumber","primary_addres_id","UserTypeId","createdAt","updatedAt"],
                include:[{
                    model:Address,
                    attributes:["id","rec_name","rec_address","rec_number","createdAt","updatedAt","UserId"],
                    
                }]
            }
        ],
        where:{
            id:order_id
        },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error!");
    });
}else{
    res.json({
        msg:"Bu Id boyuncha Order tapylmady!"
    })
}
}

const create = async(req,res)=>{
    const { sum, is_cash,SowgatId,ReseptId, is_payed,is_sowgat,order_date_time,delivery_time_status,MarketId, delivery_date_time,orderedProducts,delivered, UserId, StatusId,AddressId ,cashBack,cashBackMoney,fcmToken,type } = req.body;
    let today = new Date();
    today.setHours(today.getHours()+5);
    Orders.create({
        sum, 
        is_cash, 
        is_payed,
        is_sowgat,
        order_date_time:today, 
        delivery_date_time,
        delivered:false,
        canceled:false,
        delivery_time_status,
        cashBack,
        cashBackMoney,
        active:false,
        deleted:false,
        MarketId,
        ReseptId,
        UserId, 
        AddressId,
        SowgatId,
    }).then(async(data)=>{

         fcmTokenOrder.create({
            fcmToken:fcmToken,
            type:type,
            OrderId:data.id,
        });
        let sany=0;
        orderedProducts.forEach((orderedProduct)=>{
            orderedProduct.OrderId = data.id;
            sany = sany+orderedProduct.amount;
        });
        await Orders.update({
            sany:sany
        },{where:{
            id:data.id
        }})

        orderedProducts.forEach(async(orderedPro)=>{
            let Pro = await Products.findOne({where:{id:orderedPro.ProductId}}) 
            if(Pro.total_amount==orderedPro.amount){
                await Products.update({
                    total_amount:Pro.total_amount-orderedPro.amount,
                    is_active:false,
                },
                {where:{
                    id:orderedPro.ProductId
                }});
            }else{
                await Products.update({
                    total_amount:Pro.total_amount-orderedPro.amount
                },
                {where:{
                    id:orderedPro.ProductId
                }});
            }
        })

        OrderedProduct.bulkCreate(orderedProducts).then((data)=>{
            res.json({
                data:data,
                msg:"Successfully!"
            });
           
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }).catch((err)=>{
        console.log(err);
            res.json({
                msg:"error"
            })
    })
   
}



const updateDelivery = async(req,res)=>{
    const { order_id } = req.params;
    const foundOrder = await Orders.findOne({where:{id:order_id}});
    let d = new Date();
    let time = d.getFullYear()+"-"+d.getMonth()+1 + "-"+ d.getDate();
    console.log("date:",d,"datetime:",time)
    if(foundOrder){
        Orders.update({
            delivered:true,
            delivery_date_time:d,
            active:false
        },{
            where:{
                id:order_id
            }
        }).then(async(data)=>{
            var ordeToken = await fcmTokenOrder.findOne({
                where:{
                    OrderId:order_id
                }
            });

            const message = {
                notification: {
                    title: "Sargyt",
                    body: "Siziň sargydyňyz gowşuryldy!"
                },
                token: ordeToken?.fcmToken
            };
        
            ordeToken?.fcmToken && await  admin.messaging().send(message)

            res.json({
                msg:"successfully",
                data:data,
            })
           
        }).catch((err)=>{
            console.log(err);
                res.json({
                    msg:"error"
                })
        })
}else{
    res.json("Bu Id boyuncha Order Tapylmady!")
}
}

const updateCanceled = async(req,res)=>{
    const { order_id } = req.params;
    const foundOrder = await Orders.findOne({where:{id:order_id}});
    let d = new Date();
    let time = d.getFullYear()+"-"+d.getMonth()+1 + "-"+ d.getDate();
    console.log("date:",d,"datetime:",time)
    if(foundOrder){

        let orderedProducts = await OrderedProduct.findAll({where:{OrderId:order_id}})
        orderedProducts.forEach(async(orderedPro)=>{
            let Pro = await Products.findOne({where:{id:orderedPro.ProductId}}) 
            
            await Products.update({
                total_amount:Pro.total_amount+orderedPro.amount,
                is_active:true,
            },
            {where:{
                id:orderedPro.ProductId
            }});
            
        });

        Orders.update({
            delivered:false,
            delivery_date_time:d,
            canceled:true,
            active:false,
        },{
            where:{
                id:order_id
            }
        }).then(async(data)=>{
            var ordeToken = await fcmTokenOrder.findOne({
                where:{
                    OrderId:order_id
                }
            });

            const message = {
                notification: {
                    title: "Sargyt",
                    body: "Siziň sargydyňyz ýatyryldy!"
                },
                token: ordeToken?.fcmToken
            };
        
            ordeToken?.fcmToken &&   await  admin.messaging().send(message)
            res.json({
                msg:"successfully",
                data:data,
            })
           
        }).catch((err)=>{
            console.log(err);
                res.json({
                    msg:"error"
                })
        })
}else{
    res.json("Bu Id boyuncha Order Tapylmady!")
}
}

const update = async(req,res)=>{
    const { sum, is_cash, order_date_time,delivered, delivery_date_time,orderedProducts, StatusId, dastawshikId, cashBack , cashBackMoney } = req.body;
    const { order_id } = req.params;
    const foundOrder = await Orders.findOne({where:{id:order_id}});
    if(foundOrder){
        Orders.update({
            sum, 
            is_cash, 
            order_date_time, 
            delivery_date_time,
            delivered,
            StatusId,
            active:true,
            dastawshikId,
            cashBack,
            cashBackMoney
        },{
            where:{
                id:order_id
            }
        }).then(async(data)=>{
            orderedProducts.forEach((orderedProduct)=>{
                orderedProduct.OrderId = order_id;
            });
            await OrderedProduct.destroy({where:{OrderId:order_id}})
            OrderedProduct.bulkCreate(orderedProducts).then(()=>{
                res.json({
                    msg:"Successfully!"
                })
            }).catch((err)=>{
                console.log(err);
                res.json({
                    msg:"error"
                })
            })
        }).catch((err)=>{
            console.log(err);
                res.json({
                    msg:"error"
                })
        })
}else{
    res.json("Bu Id boyuncha Order Tapylmady!")
}
}

const updateStatus = async(req,res)=>{
    const { order_id } = req.params;
    const { StatusId } = req.body;
    
    Orders.update({
        StatusId,
        active:true,
    },{where:{id:order_id}}).then(async(data)=>{

        var ordeToken = await fcmTokenOrder.findOne({
            where:{
                OrderId:order_id
            }
        });
        
        var status = await Status.findOne({
            where:{
                id:StatusId
            }
        });

        const message = {
            notification: {
                title: "Sargyt",
                body: `Siziň sargydyňyz ${status.name_tm}!`
            },
            token: ordeToken?.fcmToken
        };
    
        ordeToken?.fcmToken &&  await  admin.messaging().send(message)

        res.json({
            msg:"successfully",
        })
    }).catch((err)=>{
        console.log(err);
    })
}

const Delete = async(req,res)=>{
    const { order_id } = req.params;
    const foundOrder = await Orders.findOne({where:{id:order_id}});

    if(foundOrder){
        let orderedProducts = await OrderedProduct.findAll({where:{OrderId:order_id}})
        orderedProducts.forEach(async(orderedPro)=>{
            let Pro = await Products.findOne({where:{id:orderedPro.ProductId}}) 
            
            await Products.update({
                total_amount:Pro.total_amount+orderedPro.amount,
                is_active:true,
            },
            {where:{
                id:orderedPro.ProductId
            }});
            
        });

        OrderedProduct.update({
            deleted:true
            },
            {
            where:{
                OrderId:order_id
            }
        }).then(()=>{
            Orders.update({
                deleted:true,
            },{
                where:{
                    id:order_id
                }
            }).then(()=>{
                res.json({
                    msg:"Successfully!"
                })
            }).catch((err)=>{
                console.log(err);
                res.json("error")
            })
        }).catch((err)=>{
            console.log(err);
            res.json("error")
        })
    }else{
        res.json({
            msg:"Bu Id boyuncha Order Tapylmady!"
        })
    }
}

const Destroy = async(req,res)=>{
    const { order_id } = req.params;
    const foundOrder = await Orders.findOne({where:{id:order_id}});

    if(foundOrder){

        let orderedProducts = await OrderedProduct.findAll({where:{OrderId:order_id}})
        orderedProducts.forEach(async(orderedPro)=>{
            let Pro = await Products.findOne({where:{id:orderedPro.ProductId}}) 
            
            await Products.update({
                total_amount:Pro.total_amount+orderedPro.amount,
                is_active:true,
            },
            {where:{
                id:orderedPro.ProductId
            }});
            
        });

        OrderedProduct.destroy({
            where:{
                OrderId:order_id
            }
        }).then(()=>{
            Orders.destroy({
                where:{
                    id:order_id
                }
            }).then(()=>{
                res.json({
                    msg:"Successfully!"
                })
            }).catch((err)=>{
                console.log(err);
                res.json("error")
            })
        }).catch((err)=>{
            console.log(err);
            res.json("error")
        })
    }else{
        res.json({
            msg:"Bu Id boyuncha Order Tapylmady!"
        })
    }
}
exports.orders_tb = orders_tb;

exports.getAllOrders = getAllOrders;
exports.getAllNewOrders = getAllNewOrders;
exports.getAllDeliveredOrders = getAllDeliveredOrders;
exports.getOneOrder = getOneOrder;
exports.getOneUserOrders = getOneUserOrders;
exports.create = create;
exports.update = update;
exports.updateStatus = updateStatus;
exports.updateDelivery = updateDelivery;
exports.updateCanceled = updateCanceled;
exports.Delete = Delete;
exports.Destroy = Destroy;