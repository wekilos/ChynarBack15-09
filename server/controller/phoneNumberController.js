var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const PhoneNumbers = require("../models/phoneNumber");
const Markets = require("../models/markets");
const Op = Sequelize.Op;

const phoneNumber_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = PhoneNumbers.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const create_market_phone = async(req,res)=>{
    const { market_id } = req.params;
    const { phoneNumber } = req.body;
    const phoneNumbers = parseInt(phoneNumber);

    const foundMarket = await Markets.findOne({
        where:{
            id:market_id
        }
    });
    if(foundMarket){    
        // for(let i=0; i<phoneNumbers.length; i++){
          PhoneNumbers.create({
                phoneNumber:phoneNumbers,
                MarketId:market_id,
            }).then(data=>{
                res.json({
                    msg:"successfully",
                    data:data,
                });
            }).catch((err)=>{
                console.log(err);
            })
        // }
        
    }else{
        res.json({
            msg:"BU ID boyuncha market tapylmady"
        });
        }
}

const delete_phone = async(req,res)=>{
    const { phone_id } = req.params;
    const foundNumber = await PhoneNumbers.findOne({
        where:{
            id:phone_id
        }
    });
    if(foundNumber){ 
        PhoneNumbers.destroy({
            where:{
                id:phone_id,
            }
        }).then(()=>{
            res.status(200).json({
                msg:"Suссessfully",
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"BU ID boyuncha telefon Nomur tapylmady!"
        });
    }
}



exports.phoneNumber_tb = phoneNumber_tb;

exports.create_market_phone = create_market_phone;
exports.delete_phone = delete_phone;