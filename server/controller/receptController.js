var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const Resepts = require("../models/resepts");

const recepts_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Resepts.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

exports.recepts_tb = recepts_tb;