var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
var Address = require("../models/address");
var Users = require("../models/users");
var Markets = require("../models/markets");
const { json } = require("sequelize");
const Op = Sequelize.Op;

const address_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Address.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const get_user_address = async(req,res)=>{

    const {deleted } = req.query;

    var Deleted = deleted ?
     { deleted :{[Op.eq]:deleted}
    }
    : { deleted :{[Op.not]:true}
    };

    

    Address.findAll({
       include:[{
           model:Users,
           attributes: ["id", "fname","lastname","phoneNumber","primary_addres_id","UserTypeId"],
       }
       ],
       where:{
           [Op.and]:[{UserId:{[Op.not]:null}},Deleted]
       }
   }
   ).then((data)=>{
       res.status(200).json(data);
   }).catch((err)=>{
       console.log(err);
   });
  
}

const getOneUserAddresses = async(req,res)=>{
    const { user_id } = req.params;
    const {deleted, active } = req.query;

    var Deleted = deleted 
    ? { deleted :{[Op.eq]:deleted}
    }
    : { deleted :{[Op.not]:true}
    };


    Address.findAll({
        include:[{
            model:Users,
            attributes: ["id", "fname","lastname","phoneNumber","primary_addres_id","UserTypeId"],
        }
        ],
        where:{
            [Op.and]:[{UserId:user_id},Deleted]
        }
    }).then((data)=>{
        res.status(200).json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const getUserOneAddress = async(req,res)=>{
    const { address_id } = req.params;
    const {deleted } = req.query;

    var Deleted = deleted 
    ? { deleted :{[Op.eq]:deleted}
    }
    : { deleted :{[Op.not]:true}
    };
    const foundAddress = await Address.findOne({where:{id:address_id}});
    if(foundAddress){
    Address.findOne({
        include:[{
            model:Users,
            attributes: ["id", "fname","lastname","phoneNumber","primary_addres_id","UserTypeId"],
        }
        ],
        where:{
            [Op.and]:[{id:address_id},Deleted]
        }
    }
    ).then((data)=>{
        res.status(200).json(data);
    }).catch((err)=>{
        console.log(err);
    })
}else{
    res.json("Bu Id boyuncha Address tapylmady!")
}
}

const create_user_addres = async(req,res)=>{
   const { user_id } = req.params;
   const {  rec_name, rec_address, rec_number } = req.body;
   Address.create({
       rec_name,
       rec_address,
       rec_number,
       deleted:false,
       UserId:user_id,
   }).then((data)=>{
           res.status(200).json({
               msg:"Suссessfully",
               data:data
           });
       }).catch((err)=>{
       console.log(err);
   });
}
const create_user_rec_addres = async(req,res)=>{
    // const { user_id } = req.params;
    const {  rec_name, rec_address, rec_number } = req.body;
    Address.create({
        rec_name,
        rec_address,
        rec_number,
        deleted:false,
        UserId:null,
    }).then((data)=>{
            res.status(200).json({
                msg:"Suссessfully",
                data:data
            });
        }).catch((err)=>{
        console.log(err);
        res.json(err)
    });
 }

const update_address = async(req,res)=>{
   const { address_id } = req.params;
   const {  rec_name, rec_address, rec_number} = req.body;
   
   const address = await Address.findOne({
       where:{
           id:address_id,
       }
   });

   if(address){
   Address.update({
    rec_name,
    rec_address,
    rec_number,
   },
   {
       where:{
           id:address_id
       }
   }).then(()=>{
           res.status(200).json({
               msg:"Suссessfully"
           });
       }).catch((err)=>{
       console.log(err);
   });
}else{
   res.json({
       msg:"Bu ID boyuncha Address tapylmady!"
   })
}
}

const delete_address = async(req,res)=>{
   const { address_id } = req.params;
   const address = await Address.findOne({
       where:{
           id:address_id,
       }
   });
   if(address){
       Address.update({
           deleted:true,
       },{
           where:{
               id:address_id,
           }
       }).then(()=>{
           res.status(200).json({
               msg:"Suссessfully",
           })
       }).catch((err)=>{
           console.log(err);
       });
   }else{
       res.json({
           msg:"Bu ID boyuncha address tapylmady!"
       })
   }
  
}

const destroy_address = async(req,res)=>{
    const { address_id } = req.params;
    const address = await Address.findOne({
        where:{
            id:address_id,
        }
    });
    if(address){
        Address.destroy({
            where:{
                id:address_id,
            }
        }).then(()=>{
            res.status(200).json({
                msg:"Suссessfully",
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"Bu ID boyuncha address tapylmady!"
        })
    }
   
 }
 

const get_market_address = async(req,res)=>{
   Address.findAll({
       include:[{
           model:Markets,
           attributes:["id","name_tm","name_ru","name_en"],
       }],
       where:{
           MarketId:{
               [Op.not]:null
           }
       }
   }).then((data)=>{
       res.json(data);
   })
}

const create_market_address = async(req,res)=>{
   const { market_id } = req.params;
   const {  name_tm,name_ru,name_en, description_tm,description_ru,description_en, lat, lan } = req.body;
   const foundMarket = await Markets.findOne({
       where:{
           id:market_id,
       }
   });

if(foundMarket){
   Address.create({
       name_tm,
       name_ru,
       name_en,
       description_tm,
       description_ru,
       description_en,
       lat:lat,
       lan:lan,
       MarketId:market_id,
   }).then((data)=>{
       res.json({
           msg:"successfully",
           data:data,
       })
   }).catch((err)=>{
       console.log(err);
   });
}else{
   res.json({
       msg:"bU ID boyuncha Market tapylmady!",
   })
}
}


const get_resturan_address = async(req,res)=>{
   Address.findAll({
       include:[{
           model:Resturans,
           attributes:["id","name_tm","name_ru","name_en"]
       }],
       where:{
           ResturanId:{
               [Op.not]:null
           }
       }
   }).then((data)=>{
       res.json(data);
   })
}



const create_resturan_address = async(req,res)=>{
   const { resturan_id } = req.params;
   const {  name_tm,name_ru,name_en, description_tm,description_ru,description_en, lat, lan } = req.body;
   const foundResturan = await Resturans.findOne({
       where:{
           id:resturan_id,
       }
   });

if(foundResturan){
   Address.create({
       name_tm,
       name_ru,
       name_en,
       description_tm,
       description_ru,
       description_en,
       lat:lat,
       lan:lan,
       ResturanId:resturan_id,
   }).then(()=>{
       res.json({
           msg:"successfully",
       })
   }).catch((err)=>{
       console.log(err);
   });
}else{
   res.json({
       msg:"BU ID boyuncha Resturan tapylmady!"
   })
}
}

exports.address_tb=address_tb;
exports.get_user_address=get_user_address;
exports.getOneUserAddresses = getOneUserAddresses;
exports.getUserOneAddress = getUserOneAddress;
exports.create_user_addres=create_user_addres;
exports.create_user_rec_addres = create_user_rec_addres;
exports.update_address = update_address;
exports.delete_address = delete_address;
exports.destroy_address = destroy_address;

exports.create_market_address = create_market_address;
exports.get_market_address = get_market_address;

exports.create_resturan_address = create_resturan_address;
exports.get_resturan_address = get_resturan_address;

