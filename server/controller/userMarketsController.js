var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const UserMarkets = require("../models/UsersMarkets");
const Op = Sequelize.Op;

const userMarkets_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = UserMarkets.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}



exports.userMarkets_tb = userMarkets_tb;