var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs"); 
const BrandsSubKategory = require("../models/brandSubKategory");

const brandsSubKategory_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = BrandsSubKategory.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

exports.brandsSubKategory_tb = brandsSubKategory_tb;