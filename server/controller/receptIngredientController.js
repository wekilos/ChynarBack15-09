var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const ReseptIngridient = require("../models/reseptIngridient");

const receptIngridient_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = ReseptIngridient.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

exports.receptIngridient_tb = receptIngridient_tb;