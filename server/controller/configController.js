var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Configs = require("../models/config");
const Op = Sequelize.Op;

const config_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Configs.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const getAllConfig = async(req,res)=>{
    Configs.findAll().then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"Error"
        })
    })
}

const create = async(req,res)=>{
    const { delivery_price, onlinePayment_active, footer_phone_number, currency_exchange, work_start_time, work_end_time } = req.body;
    Configs.create({
        delivery_price, 
        onlinePayment_active, 
        footer_phone_number, 
        currency_exchange, 
        work_start_time, 
        work_end_time
    }).then(()=>{
        res.json({
            msg:"successfully"
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"Error"
        })
    })
}
const update = async(req,res)=>{
    const { config_id } = req.params;
    const { delivery_price, onlinePayment_active, footer_phone_number, currency_exchange, work_start_time, work_end_time } = req.body;
    Configs.update({
        delivery_price, 
        onlinePayment_active, 
        footer_phone_number, 
        currency_exchange, 
        work_start_time, 
        work_end_time
    },{
        where:{
            id:config_id
        }
    }).then(()=>{
        res.json({
            msg:"successfully"
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"Error"
        })
    })
}


exports.config_tb = config_tb;

exports.create = create;
exports.update=update;
exports.getAllConfig = getAllConfig;