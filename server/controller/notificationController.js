var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
var Notification = require("../models/notification");
const { json } = require("sequelize");
const fcmNotification = require("../models/fcmNorification");
const Op = Sequelize.Op;

const notification_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Notification.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAll = async(req,res)=>{
    const {device} = req.query;
    var Device = device? device : null;
    fcmNotification.findAll(
    //     {
    //     where:{
    //         [Op.or]:[{device:Device},{device:"all"}]
    //     }
    // }
    ).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}
const getOne = async(req,res)=>{
    const {id} = req.params;
    fcmNotification.findOne({
        where:{
            id:id
        }
    }).then((data)=>{
        if(data){
        res.json(data);
        }else{
            res.json("Bu Id boyuncha notification yok")
        }
    }).catch((err)=>{
        console.log(err);
    })
}
const Create = async(req,res)=>{
    const {name_tm,name_ru,name_en,text_tm,text_ru,text_en,link,device} = req.body;
    let image;

    let imagePath = "";
    if(req.files && req.files.image){
        image = req.files.image;
        let randomNumber = Math.floor(Math.random() * 999999999999);
        imagePath = `./uploads/`+randomNumber+`${image.name}`;
        image.mv(imagePath, (err) => {
          console.log(err);
        })
      }

    await fcmNotification.create({
        name_tm,
        name_ru,
        name_en,
        text_en,
        text_ru,
        text_tm,
        device:device,
        image:imagePath,
        link,
        deleted:false,
        active:true
    }).then((data)=>{
        res.json("successfully!");
    }).catch((err)=>{
        console.log(err);
    })
}
const Update = async(req,res)=>{
    const {id} = req.params;
    const {name_tm,name_ru,name_en,text_tm,text_ru,text_en,link,device} = req.body;
    let image ;

    let fountNoti;
    let imagePath = "";
    if(req.files){
        image = req.files.image;
        let randomNumber = Math.floor(Math.random() * 999999999999);
        imagePath = `./uploads/`+randomNumber+`${image.name}`;
        image.mv(imagePath, (err) => {
          console.log(err);
        });

        await fcmNotification.update({
            name_tm,
            name_ru,
            name_en,
            text_en,
            text_ru,
            text_tm,
            device:device,
            image:imagePath,
            link
        },
        {
            where:{
                id:id
            }
        }).then((data)=>{
            res.json("successfully!");
        }).catch((err)=>{
            console.log(err);
        })
      }else{
        await fcmNotification.update({
            name_tm,
            name_ru,
            name_en,
            text_en,
            text_ru,
            text_tm,
            device:device,
            link
        },
        {
            where:{
                id:id
            }
        }).then((data)=>{
            res.json("successfully!");
        }).catch((err)=>{
            console.log(err);
        })
      }


    
}

const Delete = async(req,res)=>{
    const {id} = req.params;
    fcmNotification.destroy({
        where:{
            id:id
        }
    }).then((data)=>{
        res.json("successfully")
    }).catch((err)=>{
        console.log(err);
        res.json(err)
    })
}

exports.notification_tb = notification_tb;
exports.Create = Create;
exports.getAll = getAll;
exports.getOne = getOne;
exports.Update = Update;
exports.Delete = Delete;
