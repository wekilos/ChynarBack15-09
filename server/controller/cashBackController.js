var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const CashBack = require("../models/cashBack");

const cashBack_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = CashBack.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
};

const getOneUserCashBacks = (req,res)=>{
    const {UserId,MarketId,ReseptId} = req.query;

    const UserID = UserId?
    {
        UserId:UserId
    }
    :null;
    const MarketID = MarketId?
    {
        MarketId:MarketId
    }
    :null;
    const ReseptID = ReseptId?
    {
        ReseptId:ReseptId
    }
    :null

    CashBack.findAll({
        where:{
            [Op.and]:[UserID,MarketID,ReseptID]
        }
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })

}
const create = async(req,res)=>{
    const { cashBacks,UserId, MarketId,ReseptId } = req.body;

    CashBack.create({
        cashBacks,
        active:true,
        deleted:false,
        UserId,
        MarketId,
        ReseptId
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

exports.cashBack_tb = cashBack_tb;
exports.create = create;
exports.getOneUserCashBacks = getOneUserCashBacks;