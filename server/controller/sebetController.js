var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const Sebet = require("../models/sebet");

const sebet_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Sebet.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const getAll = async(req,res)=>{
    const { user_id } = req.params;
    Sebet.findAll({
        where:{
            user_id:user_id,
        }
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const create = async(req,res)=>{
    const {user_id}=req.params;
   const {product_id,sany,name_tm,name_ru,name_en,price,sale_price,step,view_count,is_valyuta_price,surat, unit } = req.body;
   let product; 
   await Sebet.findOne({
        where:{
            product_id:product_id,
        }
    }).then((data)=>{
        product = data; 
    }).catch((err)=>{
        console.log(err);
    });
    if(product){
        Sebet.update({
            sany:product.sany+1,
        },{
            where:{
                product_id:product_id
            }
        }).then((data)=>{

        }).catch((err)=>{
            console.log(err);
        })
    }else{
   Sebet.create({
        user_id,
        product_id,
        sany,
        name_tm,
        name_ru,
        name_en,
        price,
        sale_price,
        step,
        view_count,
        is_valyuta_price,
        surat,
        unit
    }).then((data)=>{
        console.log(data);
        res.json("successfully!");
    }).catch((err)=>{
        console.log(err);
    });
}
}

const dec = async(req,res)=>{
    const { sebet_id } = req.params;
    let sebet ;
     await Sebet.findOne({where:{id:sebet_id}}).then((data)=>{
       sebet =data;
     }).catch((err)=>{
         console.log(err);
     })
    Sebet.update({
        sany:sebet.sany-1
        },{
        where:{
            id:sebet_id,
        }
    }).then((data)=>{
        res.json("successfully!")
    }).catch((err)=>{
        console.log(err);
    })
}

const inc = async(req,res)=>{
    const { sebet_id } = req.params;
    let sebet ;
     await Sebet.findOne({where:{id:sebet_id}}).then((data)=>{
       sebet =data;
     }).catch((err)=>{
         console.log(err);
     });
     let sany = sebet.sany+1;
    Sebet.update({
        sany:sany,
        },{
        where:{
            id:sebet_id,
        }
    }).then((data)=>{
        res.json("successfully!");
    }).catch((err)=>{
        console.log(err);
    })
}

const update = (req,res)=>{
    const { sebet_id } = req.params;
    const {user_id,product_id,sany,name_tm,name_ru,name_en,price,sale_price,step,view_count,is_valyuta_price,surat } = req.body;
    Sebet.update({
        user_id,
        product_id,
        sany,
        name_tm,
        name_ru,
        name_en,
        price,
        sale_price,
        step,
        view_count,
        is_valyuta_price,
        surat,
        },
        {
            id:sebet_id
        
    }).then((data)=>{
        console.log(data);
        res.json("successfully!");
    }).catch((err)=>{
        console.log(err);
    })
}

const Delete = async(req,res)=>{
    const { sebet_id } = req.params;
    Sebet.destroy({
        where:{
            id:sebet_id
        }
    }).then((data)=>{
        console.log(data);
        res.json("successfully!");
    }).catch((err)=>{
        console.log(err);
    })
}
exports.sebet_tb = sebet_tb;
exports.getAll = getAll;
exports.dec = dec;
exports.inc = inc;
exports.create = create;
exports.update = update;
exports.Delete = Delete;