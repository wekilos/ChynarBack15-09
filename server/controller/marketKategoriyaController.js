var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const MarketKategoriya = require("../models/marketKategoriya");
const Markets = require("../models/markets");
const MarketSubKategoriya = require("../models/marketSubKategoriya");
const Op = Sequelize.Op;

const marketKategoriya_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = MarketKategoriya.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}



const getAllKategoriya = async(req,res)=>{
    const { market_id } = req.params;
    const {active,deleted} = req.query;
    const foundMarket = await Markets.findOne({
        where:{
            id:market_id,
        }
    });
    var Active = active ?
    {
      active:{[Op.eq]:active},
    }
    :{
      active:{[Op.eq]:true},
    };

  var Deleted = deleted 
  ? {
    deleted :{ [Op.eq]:deleted}
  }
  : {
    deleted :{ [Op.eq]:false}
  };
    if(foundMarket){
        MarketKategoriya.findAll(
            {
                include:[{
                    model:Markets,
                    attributes:["id","name_tm","name_ru","name_en","surat"],
                    // where:{deleted:false,active:true}
                },
                {
                    model:MarketSubKategoriya,
                    attributes:["id","name_tm","name_ru","name_en","active"],
                    // where:{[Op.and]:[{deleted:false},{active:true}]}

                }
            ],
            where:{
                [Op.and]:[{MarketId:market_id},Active,Deleted]
            }
        }).then((data)=>{
            res.json(data)
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        });
    }else{
        res.json({
            msg:"BU ID boyuncha Market tapylmady"
        })
    }
}

const getOneKategoriya = async(req,res)=>{
    const { kategoriya_id } = req.params;
   
        MarketKategoriya.findOne({
                include:[{
                    model:Markets,
                    attributes:["id","name_tm","name_ru","name_en","surat"],
                    // where:{deleted:false,active:true}
                },
                {
                    model:MarketSubKategoriya,
                    attributes:["id","name_tm","name_ru","name_en","active"],
                    // where:{deleted:false,active:true}
                }],
            where:{
                [Op.and]:[{id:kategoriya_id}]
            }
        }).then((data)=>{
            res.json(data)
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        });
    
}

const createKategoriya = async(req,res)=>{
    const { market_id } = req.params;
    const { name_tm, name_ru, name_en, active } = req.body;
    
    const foundMarket = await Markets.findOne({
        where:{
            id:market_id
        }
    });

    if(foundMarket){
        MarketKategoriya.create({
            name_tm,
            name_ru,
            name_en,
            active:true,
            deleted:false,
            MarketId:market_id,
        }).then(()=>{
            res.json({
                msg:"successfully!",
            })
        }).catch(err=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"BU ID boyuncha Market tapylmady"
        })
    }
}

const updateKategoriya = async(req,res)=>{
    const { kategoriya_id } = req.params;
    const { market_id, name_tm, name_ru, name_en, active, deleted } = req.body;

    const foundKategoriya = await MarketKategoriya.findOne({
        where:{
            id:kategoriya_id,
        }
    });
    // const foundMarket = await Markets.findOne({
    //     where:{
    //         id:market_id,
    //     }
    // });
    // if(foundMarket){
    if(foundKategoriya){
        MarketKategoriya.update({
            name_en,
            name_ru,
            name_tm,
            active,
            deleted,
            MarketId:market_id,
        },
        {
        where:{
            [Op.and]: [
                { id:kategoriya_id},
                // { MarketId:market_id }
              ]
        }
    }).then(()=>{
        res.json({
            msg:"successfully!",
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    })
    }else{
        res.json({
            msg:"BU ID boyuncha Kategoriya tapylmady!"
        })
    }
// }else{
//     res.json({
//         msg:"BU ID boyuncha Market tapylmady!"
//     })
// }
}

const deleteKategoriya = async(req,res)=>{
    const { kategoriya_id } = req.params;
    const foundKategoriya = await MarketKategoriya.findOne({
        where:{
            id:kategoriya_id,
        }
    });
    if(foundKategoriya){
        MarketKategoriya.update({
            deleted:true,
            active:false
        },{
            where:{
                id:kategoriya_id,
            }
        }).then(()=>{
                res.json({
                    msg:"successfully!"
                })
            }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error!"
            })
        })
    }else{
        res.json({
            msg:"BU ID boyuncha Market Kategoriya tapylmady"
        })
    }
}

const destroyKategoriya = async(req,res)=>{
    const { kategoriya_id } = req.params;
    const foundKategoriya = await MarketKategoriya.findOne({
        where:{
            id:kategoriya_id,
        }
    });
    if(foundKategoriya){
        MarketKategoriya.destroy({
            where:{
                id:kategoriya_id,
            }
        }).then(()=>{
                res.json({
                    msg:"successfully!"
                })
            }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error!"
            })
        })
    }else{
        res.json({
            msg:"BU ID boyuncha Market Kategoriya tapylmady"
        })
    }
}









exports.marketKategoriya_tb=marketKategoriya_tb;

exports.getAllKategoriya = getAllKategoriya;
exports.getOneKategoriya = getOneKategoriya;
exports.createKategoriya = createKategoriya;
exports.updateKategoriya = updateKategoriya;
exports.deleteKategoriya = deleteKategoriya;
exports.destroyKategoriya = destroyKategoriya;

