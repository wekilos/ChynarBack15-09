var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
var UserType = require("../models/userType");
var Permission = require("../models/userTypePermissions");
const { json } = require("sequelize");
const Op = Sequelize.Op;

const userType_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = UserType.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const userPermissions_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Permission.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getUserType = async(req,res)=>{
  const {active, deleted } = req.query;

  var Active = active
    ? {
        active: { [Op.eq]: active },
    }
    : {
        active: { [Op.eq]: true },
    };

    var Deleted = deleted
    ? {
        deleted: { [Op.eq]: deleted },
    }
    : {
        deleted: { [Op.eq]: false },
    };

    try{
      data = await UserType.findAll({
        include: [
          {
            model: Permission,
            attributes: ["id", "number"],
          },
        ],
    
        attributes: ["id", "type_tm", "type_ru", "type_en","MarketId"],
        where:{
          [Op.and]:[Active,Deleted]
        }
      });
      res.json(data);
    }catch(err){
        console.log(err);
    }
}

const createType = async(req,res)=>{
    const {type_tm, type_ru, type_en, numbers,MarketId}=req.body;
    console.log("Body="+ JSON.stringify(req.body) + "\nNumbers=" + typeof(numbers));
    const numberss = JSON.parse(numbers);
    console.log(typeof(numberss));
    try{
        UserType.create({
          type_tm:type_tm,
          type_ru:type_ru,
          type_en:type_en,
          active:true,
          deleted:false,
          MarketId:MarketId
        }).then((data)=>{
            for (let i = 0; i < numberss.length; i++) {
                Permission.create({
                  number: numberss[i],
                  UserTypeId: data.id,
                });
              }

            res.status(200).json({
                msg:"Suссessfully",
            }).catch(err=>{
              console.log(err);
                res.status(500).json({
                    msg:"error",
                    err:err,
                });
            })
        })
    }catch(err){
        console.log(err);
        res.json({
            msg : "Error",
            err:err,
        })
    }
}

const createTypeForMarket = async(req,res)=>{
  const {type_tm, type_ru, type_en, numbers,MarketId}=req.body;
  console.log("Body="+ JSON.stringify(req.body) + "\nNumbers=" + typeof(numbers));
  const numberss = JSON.parse(numbers);
  console.log(typeof(numberss));
  try{
      UserType.create({
        type_tm:type_tm,
        type_ru:type_ru,
        type_en:type_en,
        active:true,
        deleted:false,
        MarketId
      }).then((data)=>{
          for (let i = 0; i < numberss.length; i++) {
              Permission.create({
                number: numberss[i],
                UserTypeId: data.id,
              });
            }

          res.status(200).json({
              msg:"Suссessfully",
          }).catch(err=>{
            console.log(err);
              res.status(500).json({
                  msg:"error",
                  err:err,
              });
          })
      })
  }catch(err){
      console.log(err);
      res.json({
          msg : "Error",
          err:err,
      })
  }
}

const updateUserType = async(req,res)=>{
  const { type_id } = req.params;
  const {type_tm, type_ru, type_en, numbers, MarketId, active,deleted }=req.body;
  
  console.log("Body="+ JSON.stringify(req.body) + "\nNumbers=" + typeof(numbers));
  const numberss = JSON.parse(numbers);
  console.log(typeof(numberss));

  const foundType = await UserType.findOne({
    where:{
      id:type_id,
    }
  });
  

   if(foundType){
      UserType.update({
        type_tm:type_tm,
        type_ru:type_ru,
        type_en:type_en,
        active,deleted,
        MarketId
      },
      {
        where:{
          id:type_id,
        }
      }).then(async()=>{
        await  Permission.destroy({
          where: { 
                UserTypeId: type_id,
          },
        });
        for (let i = 0; i < numberss.length; i++) {
            await Permission.create({
              number: numberss[i],
              UserTypeId: type_id,
            });
          }

          res.status(200).json({
              msg:"Suссessfully",
          }).catch(err=>{
              console.log(err);
              res.status(500).json({
                  msg:"error",
                  err:err,
              });
          })
      })
  }else{
      res.json({
          msg : "Bu ID boyuncha UserType tapylmady!"
      })
  }

}

const deleteUserType = async(req,res)=>{
  const { type_id } = req.params;

  const foundType = await UserType.findOne({
    where:{
      id:type_id
    }
  });
  if(foundType){
    UserType.update({
      deleted:true,
      active:false,
    },{
      where:{
        id:type_id
      }
    }).then(()=>{
      res.json({
        msg:"successfully!"
      }).catch((err)=>{
          console.log(err);
      })
    })
  }else{
    res.json({
      msg:"Bu ID boyuncha User Type tapylmady!"
    });
  }
}

const destroyUserType = async(req,res)=>{
  const { type_id } = req.params;

  const foundType = await UserType.findOne({
    where:{
      id:type_id
    }
  });
  if(foundType){
    UserType.destroy({
      where:{
        id:type_id
      }
    }).then(()=>{
      res.json({
        msg:"successfully!"
      }).catch((err)=>{
          console.log(err);
      })
    })
  }else{
    res.json({
      msg:"Bu ID boyuncha User Type tapylmady!"
    });
  }
}

const updateUserTypePermissions = async(req,res)=>{
    // const { type_id } = req.params;
    const { typeId,numbers } = req.body;
     
    console.log(req.body.data);
    const numberss = JSON.parse(numbers);
    await  Permission.destroy({
      where: { 
            UserTypeId: typeId,
      },
    });

    for (let i = 0; i < numberss.length; i++) {
      Permission.create({
        number: numberss[i],
        UserTypeId: typeId,
      });
    }

    res.status(200).json({
      msg: "Suссessfully",
    });
  }

 

exports.userType_tb = userType_tb;
exports.createType=createType;
exports.getUserType=getUserType;
exports.deleteUserType = deleteUserType;
exports.destroyUserType = destroyUserType;
exports.updateUserType = updateUserType;

exports.createTypeForMarket = createTypeForMarket;

exports.userPermissions_tb=userPermissions_tb;
exports.updateUserTypePermissions = updateUserTypePermissions;