var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Sowgat = require("../models/sowgat");
const Op = Sequelize.Op;

const sowgat_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Sowgat.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}



exports.sowgat_tb = sowgat_tb;