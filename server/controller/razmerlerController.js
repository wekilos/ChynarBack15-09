var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Razmerler = require("../models/Razmerler");
const Op = Sequelize.Op;

const razmerler_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Razmerler.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const getAll = async(req,res)=>{
    const {active,deleted} = req.query;
    var Active = active
    ? {
        active: { [Op.eq]: active },
    }
    : {
        active: { [Op.eq]: true },
    };

    var Deleted = deleted
    ? {
        deleted: { [Op.eq]: deleted },
    }
    : {
        deleted: { [Op.eq]: false },
    };

    const {ProductId} = req.params;
    Razmerler.findAll({
        where: {
            [Op.and]: [Active,Deleted,{ProductId:ProductId}],
          },
        // where:{
        //     ProductId:ProductId
        // },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.status(200).json(data);
    }).catch((err)=>{
        console.log(err);
    })
}


const create = async(req,res)=>{
    const {ProductId} = req.params;
    const {name_tm,name_ru,name_en,sany,surat,surat1,surat2,active} = req.body;

    Razmerler.create({
        name_tm,
        name_ru,
        name_en,
        sany,
        active:true,
        deleted:false,
        ProductId
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const Update = async(req,res)=>{
    const {id} = req.params;
    const {name_tm,name_ru,name_en,sany,surat,surat1,surat2,active,deleted, ProductId } = req.body;

    Razmerler.update({
        name_tm,
        name_ru,
        name_en,
        sany,
        active,
        deleted,
        ProductId
    },{
        where:{id:id}
    }).then(()=>{
        res.json("updated");
    }).catch((err)=>{
        console.log(err);
    })
}

const Delete = async(req,res)=>{
    const {id} = req.params;

    Razmerler.update({
        active:false,
        deleted:true,
    },
    {
        where:{id:id}
    }).then(()=>{
        res.json("deleted");
    }).catch((err)=>{
        console.log(err);
    })
}

const Destroy = async(req,res)=>{
    const {id} = req.params;

    Razmerler.destroy({
        where:{id:id}
    }).then(()=>{
        res.json("deleted");
    }).catch((err)=>{
        console.log(err);
    })
}

exports.razmerler_tb = razmerler_tb;

exports.create = create;
exports.getAll = getAll;
exports.Update = Update;
exports.Delete = Delete;
exports.Destroy = Destroy;