var Sequelize = require("sequelize");
var fetch = require('node-fetch');
var Users = require("../models/users");
var Address = require("../models/address");
var sequelize = require("../../config/db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Func = require("../functions/functions");
const Op = Sequelize.Op;
const UserType = require("../models/userType");
const Permission = require("../models/userTypePermissions");
const Numbers = require("../models/numbers");
const FcmToken = require("../models/fcmTokens");

 const users_tb = async (req, res) => {
    const response = await sequelize
      .sync()
      .then(function () {
        const data = Users.findAll();
        console.log("connection connected");
        return data;
      })
      .catch((err) => {
        return err;
        console.log("connection connected");
      });
    res.json(response);
  };



  const SendToSocket = async(req,res)=>{
    const {name,lastname,address,password,number} = req.body;
    let data;
    let userData = {name,lastname,address,password,number}
    await fetch('http://95.85.122.39:1111/api/send', {
      method: 'POST',
      body: JSON.stringify(userData),
      headers: { 'Content-Type': 'application/json' }
    }).then(res => res.json())
      .then(json => {console.log(json); data = json});

      res.json(data);
    }


  const list = async (req, res) => {
    var { all, typeId, active, deleted } = req.query;
    var All = all
      ? {
          [Op.or]: [
            { fname: { [Op.iLike]: `%${all}%` } },
            // { phoneNumber: { [Op.iLike]: `%${all}%` } },
            { lastname: { [Op.iLike]: `%${all}%` } },
          ],
        }
      : null;
  
    var TypeId = typeId
      ? {
          UserTypeId: { [Op.eq]: `${typeId}` },
        }
      : null;

      var Active = active
    ? {
        active: { [Op.eq]: active },
    }
    : {
        active: { [Op.eq]: true },
    };

    var Deleted = deleted
    ? {
        deleted: { [Op.eq]: deleted },
    }
    : {
        deleted: { [Op.eq]: false },
    };

  
  
    Users.findAll({
      include: [
        {
          model: UserType,
          attributes: ["id", "type_tm","type_ru","type_en","active","deleted"], 
          // where: {
          //   [Op.and]: [Type],
          // },
        },
        {
          model:Address,
          attributes:["id","rec_name","rec_address","rec_number","UserId"],
        },
        {
          model:FcmToken,
          attributes:["id","fcmToken","type","active","deleted"],
        }
      ],
      // include:[
        
      // ],
  
      where: {
        [Op.and]: [All, TypeId,Active,Deleted],
      },
      order: [["id", "ASC"]],
    }).then((data)=>{
      res.json(data);
    }).catch((err)=>{
      console.log(err);
      res.json("error!")
    })
  };

  


    const create = async (req, res) => {
  
    const { marketIds,fname, lastname, phoneNumber, password, UserTypeId, rec_name, rec_address, rec_number,
 
      dastawshik,
      admin,
      subAdmin,
      operator,fcmToken,type} = req.body;
    console.log("data",req.body);
    const existUser = await Users.findOne({
      where:{
        phoneNumber:phoneNumber,
      }
    });

    const salt = bcrypt.genSaltSync();
    bcrypt.hash(password, salt, (err, hashpassword) => {
      if (err) {
        console.log(err);
        return res.status(500).json({ msg: "Error", err: err });
      } else {
        
    if(existUser){
      res.json({
        msg:"Bu nomurda ulanyjy onden bar."
      })
    }else{
      Users.create({
        fname,
        lastname,
        phoneNumber,
        password:hashpassword,
        cashBacks:0,
        marketIds,
        dastawshik,
        admin,
        subAdmin,
        user:true,
        operator,
        active:true,
        deleted:false,
        UserTypeId,
      }).then(async(data) => {
         let userId = data.id;
         let user_fname = data.fname;
         let userPhone = data.phoneNumber;
         let userTypeId = data.UserTypeId;
         let UserType=data.UserType;

        await FcmToken.create({
           fcmToken:fcmToken,
           type:type,
           UserId:userId,
         })
            Address.create({
                rec_name:user_fname,
                rec_address:rec_address,
                rec_number:userPhone,
                UserId:userId,
            }).then(async(addres)=>{
              console.log(addres)
              await Users.update({
                primary_addres_id:addres.id,
              },{
              where:{
                id:userId
              }
            });
              await Numbers.destroy({
                where:{
                  phoneNumber:phoneNumber
                }
              });
            jwt.sign(
              {
                id: data.id,
                role: userTypeId,
                name: user_fname,
                phoneNumber:userPhone,
                address:addres,
              },
              Func.Secret(),
              (err, token) => {
                res.status(200).json({
                  msg: "Suссessfully",
                  token: token,
                  id:userId,
                  UserTypeId:userTypeId,
                  UserType:UserType,
                  phoneNumber:userPhone,
                  address:addres,
                });
              }
            );
            }).catch((err)=>{
              console.log(err);
              console.log("error::::: ______________________________________________  Create Address");
              res.json("create address",err)
            })
        // res.json("succsess!")
      }).catch((err) => {
        console.log(err);
        console.log("error:::::________________________________________________________  Create User");
        res.json("create user",err)
      });
    }


      }
    });

  };
  
  // const create = async (req, res) => {
  //   const salt = bcrypt.genSaltSync();
  
  //   const { name, phoneNumber, password, typeID } = req.body.data;
  //   bcrypt.hash(password, salt, (err, hashpassword) => {
  //     if (err) {
  //       console.log(err);
  //       return res.status(500).json({ msg: "Error", err: err });
  //     } else {
  //       console.log(hashpassword);
  //       Users.create({
  //         name: name,
  //         phoneNumber: phoneNumber,
  //         password: hashpassword,
  //         userTypeId: typeID,
  //       })
  //         .then((data) => {
  //           jwt.sign(
  //             {
  //               id: data.id,
  //               role: typeID,
  //               name: name,
  //               phoneNumber:phoneNumber,
  //             },
  //             Func.Secret(),
  //             (err, token) => {
  //               res.status(200).json({
  //                 msg: "Suссessfully",
  //                 token: token,
  //               });
  //             }
  //           );
  //         })
  //         .catch((err) => {
  //           res.status(500).json({ msg: "Error", err: err });
  //         });
  //     }
  //   });
  // };

  const login = async(req,res)=>{
    console.log("Login data ="+ JSON.stringify(req.body));
  
  const { phoneNumber, password,type,fcmToken } = req.body

  
  
   await Users.findOne({
    include: [
      {
        model: UserType,
        attributes: ["type_tm","type_ru","type_en","MarketId"],
        include: [
          {
            model: Permission,
            attributes: ["id", "number"],
          },
        ],
      },
      
      {
        model: FcmToken,
        attributes:["id","fcmToken","type","active","deleted"],
      },
    ],
    where: { phoneNumber: phoneNumber },
  }).then(async(data)=>{


   if (await bcrypt.compare(password, data.password)) {
        const token = jwt.sign(
          { id: data.id, role:data.UserTypeId, name: data.fname, phoneNumber:data.phoneNumber },
          Func.Secret()
        );

        let bar = false;
        // return  res.json(data);
        data?.FcmTokens?.map((fcm)=>{
            if(fcm===fcmToken){
              bar=true;
            }
        });

        if(bar===false){
          await FcmToken.create({
            fcmToken:fcmToken,
            type:type,
            UserId:data.id,
          })
        }
        
        return res.json({
          id: data.id,
          name: data.fname,
          token: token,
          dastawshik:data.dastawshik,
          subAdmin:data.subAdmin,
          admin:data.admin,
          operator:data.operator,
          type:data.UserType,
          typeID: data.UserTypeId,
          permission: data.UserType.Permissions,
          login: true,
        });
      } else {
        res.send({
          msg: "Your username or password is invalid!",
          login: false,
        });
      }
    
  }).catch((err)=> {
   
    res.send({ login: false, msg: "Hasaba alynmadyk ulanyjy!",err:err });
  }) 

  }

// const update_user = async(req,res)=>{
//   const { id }=req.params;
//   const { fname, lastname, password, UserTypeId}=req.body;
//   const foundUser = await Users.findOne({
//     where:{
//       id:id,
//     }
//   });
//   if(foundUser){
//   Users.update({
//     fname,
//     lastname,
//     password:password,
//     UserTypeId,
//   },
//   {
//     where: {
//       id: id,
//     },
//   }).then((data)=>{
//     console.log("data:",data)
//     res.status(200).json({
//       msg: "Suссessfully",
//     });
//   }).catch((err)=>{
//       console.log(err);
//   });
// }else{
//   res.json({
//     msg:"BU ID boyuncha user tapylmady!"
//   })
// }
// }

const update_user = async(req,res)=>{
  const { id }=req.params;
  const { fname, lastname, password,phoneNumber, UserTypeId, cashBacks,
    marketIds,
    dastawshik,
    admin,
    subAdmin,
    operator,
    active,
    deleted,}=req.body;
  const foundUser = await Users.findOne({
    where:{
      id:id,
    }
  });
  if(foundUser){
    const salt = bcrypt.genSaltSync();
    bcrypt.hash(password, salt, (err, hashpassword) => {
      if (err) {
        console.log(err);
        return res.status(500).json({ msg: "Error", err: err });
      } else {
        
    if(false){
      res.json({
        msg:"Bu nomurda ulanyjy onden bar."
      })
    }else{
      Users.update({
        fname,
        lastname,
        password:hashpassword,
        cashBacks,
        marketIds,
        dastawshik,
        admin,
        subAdmin,
        user:true,
        operator,
        active,
        deleted,
        UserTypeId,
      },{
        where:{id:id}
      }).then(async(data) => {
        Users.findOne({where:{id:id}}).then((data)=>{
          let userId = data.id;
          let user_fname = data.fname;
          let userPhone = data.phoneNumber;
          let userTypeId = data.UserTypeId;
          let UserType=data.UserType;
               
          jwt.sign(
            {
              id: data.id,
              role: userTypeId,
              name: user_fname,
              phoneNumber:userPhone,
            },
            Func.Secret(),
            (err, token) => {
              res.status(200).json({
                msg: "Suссessfully",
                token: token,
                id:userId,
                UserTypeId:userTypeId,
                UserType:UserType,
                phoneNumber:userPhone,
              });
            }
          );
        }).catch((err)=>{
          console.log("errrr",err);
        })
         
         
      }).catch((err) => {
        console.log(err);
        console.log("error:::::________________________________________________________  update User");
      });
    }


      }
    });
  // Users.update({
  //   fname,
  //   lastname,
  //   password:password,
  //   UserTypeId,
  // },
  // {
  //   where: {
  //     id: id,
  //   },
  // }).then((data)=>{
  //   console.log("data:",data)
  //   res.status(200).json({
  //     msg: "Suссessfully",
  //   });
  // }).catch((err)=>{
  //     console.log(err);
  // });
}else{
  res.json({
    msg:"BU ID boyuncha user tapylmady!"
  })
}
}
const update_user_onUser = async(req,res)=>{
  const { fname, lastname, password,phoneNumber, UserTypeId, cashBacks,
    marketIds,
    dastawshik,
    admin,
    subAdmin,
    operator,
    active,
    deleted,}=req.body;
  const foundUser = await Users.findOne({
    where:{
      phoneNumber:phoneNumber,
    }
  });
  if(foundUser){
    const salt = bcrypt.genSaltSync();
    bcrypt.hash(password, salt, (err, hashpassword) => {
      if (err) {
        console.log(err);
        return res.status(500).json({ msg: "Error", err: err });
      } else {
        
    if(false){
      res.json({
        msg:"Bu nomurda ulanyjy onden bar."
      })
    }else{
      Users.update({
        fname,
        lastname,
        password:hashpassword,
        cashBacks,
        marketIds,
        dastawshik,
        admin,
        subAdmin,
        user:true,
        operator,
        active,
        deleted,
        UserTypeId,
      },{
        where:{phoneNumber:phoneNumber}
      }).then(async(data) => {
        await Numbers.destroy({where:{phoneNumber:phoneNumber}});
        Users.findOne({where:{phoneNumber:phoneNumber}}).then((data)=>{

          let userId = data.id;
          let user_fname = data.fname;
          let userPhone = data.phoneNumber;
          let userTypeId = data.UserTypeId;
          let UserType=data.UserType;
               
          jwt.sign(
            {
              id: data.id,
              role: userTypeId,
              name: user_fname,
              phoneNumber:userPhone,
            },
            Func.Secret(),
            (err, token) => {
              res.status(200).json({
                msg: "Suссessfully",
                token: token,
                id:userId,
                UserTypeId:userTypeId,
                UserType:UserType,
                phoneNumber:userPhone,
              });
            }
          );
        }).catch((err)=>{
          console.log("errrr",err);
        })
         
         
      }).catch((err) => {
        console.log(err);
        console.log("error:::::________________________________________________________  update User");
      });
    }


      }
    });
  // Users.update({
  //   fname,
  //   lastname,
  //   password:password,
  //   UserTypeId,
  // },
  // {
  //   where: {
  //     id: id,
  //   },
  // }).then((data)=>{
  //   console.log("data:",data)
  //   res.status(200).json({
  //     msg: "Suссessfully",
  //   });
  // }).catch((err)=>{
  //     console.log(err);
  // });
}else{
  res.json({
    msg:"BU ID boyuncha user tapylmady!"
  })
}
}

  
const updatePrimaryAddress = async(req,res)=>{
  const {user_id} = req.params;
  const { primary_addres_id } = req.body;
  Users.update({
    primary_addres_id
  },{
    where:{
      id:user_id
    }
  }).then(data=>{
    res.json("successfully!")
  }).catch((err)=>{
    console.log(err);
  })
}
const delete_user = async(req,res)=>{
  const { id } = req.params;
  const foundUser = await Users.findOne({
    where:{
      id:id
    }
  });
  if(foundUser){
    Users.update({
      deleted:true,
      active:false,
    },{
      where:{
        id:id
      }
    }).then(()=>{
        res.status(200).json({
          msg: "Suссessfully",
      });

    }).catch((err)=>{
      console.log(err);
    });
  }else{
    res.json({
      msg:"Bu ID boyuncha user tapylmady!"
    })
  }
}

const destroy_user = async(req,res)=>{
  const { id } = req.params;
  const foundUser = await Users.findOne({
    where:{
      id:id
    }
  });
  if(foundUser){
    Users.destroy({
      where:{
        id:id
      }
    }).then(()=>{
      Address.destroy({
        where:{
          UserId:id,
        }
      }).then(()=>{
        res.status(200).json({
          msg: "Suссessfully",
      })
      
      }).catch((err)=>{
        console.log(err);
      });
    }).catch((err)=>{
      console.log(err);
    });
  }else{
    res.json({
      msg:"Bu ID boyuncha user tapylmady!"
    })
  }
}
  exports.users_tb=users_tb;
  exports.list=list;
  exports.create=create;
  exports.login = login;
  exports.update_user=update_user;
  exports.update_user_onUser = update_user_onUser;
  exports.updatePrimaryAddress = updatePrimaryAddress;
  exports.delete_user=delete_user;
  exports.destroy_user = destroy_user;

  exports.SendToSocket = SendToSocket;