var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Markets = require("../models/markets");
const MarketAddress = require("../models/marketAddress");
const Products = require("../models/products");
const PhoneNumbers = require("../models/phoneNumber");
const Op = Sequelize.Op;
const fs = require("fs");
const MarketKategoriya = require("../models/marketKategoriya");
const MarketSubKategoriya = require("../models/marketSubKategoriya");

const markets_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Markets.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const getOneMarketDes = async(req,res)=>{
  const { welayatId, marketId,active,deleted} = req.query;
  
  let MarketId = marketId?
  marketId:3;

  let WelayatId = welayatId?
    { WelayatlarId:welayatId }
    :null;

  var Active = active ?
  {
    active:{[Op.eq]:active},
  }
  :{
    active:{[Op.eq]:true},
  };

var Deleted = deleted 
? {
  deleted :{ [Op.eq]:deleted}
}
: {
  deleted :{ [Op.eq]:false}
};

  Markets.findOne({
    where:{
      [Op.and]:[WelayatId,{id:MarketId},Deleted,Active]
    }
  }).then((data)=>{
    res.json({
      id:data.id,
      description_en:data.description_en,
      description_ru:data.description_ru,
      description_tm:data.description_tm
    })
  }).catch((err)=>{
    console.log(err)
    res.json(err);
  })
  
}


  const get_all_markets = async(req,res)=>{
    const {WelayatlarId,KategoriyaOfMarketId,active,deleted} = req.query;
    var Welayat = WelayatlarId
    ? {
        WelayatlarId: { [Op.eq]: `${WelayatlarId}` } 
      
      }
    : null;
    var Kategoryya = KategoriyaOfMarketId
    ? {
        KategoriyaOfMarketId: { [Op.eq]: `${KategoriyaOfMarketId}` },
      }
    : null;
    var Active = active ?
      {
        active:{[Op.eq]:active},
      }
      :{
        active:{[Op.eq]:true},
      };

    var Deleted = deleted 
    ? {
      deleted :{ [Op.eq]:deleted}
    }
    : {
      deleted :{ [Op.eq]:false}
    };
    Markets.findAll({
      include:[
        {
          model:PhoneNumbers,
          attributes:["id","phoneNumber"],
          // where:{deleted:false,active:true}
        },
        {
          model:MarketAddress,
          attributes:["id","name_tm","name_ru","name_en","description_tm","description_ru","description_en","createdAt","updatedAt","MarketId"],
          // where:{deleted:false,active:true}
        }        
              ],
      where:{
        [Op.and]:[Welayat,Kategoryya,Deleted,Active]
      },
      order: [
        ['id', 'ASC'],
      ]
    }).then((data)=>{
      res.status(200).json(data);
    }).catch((err)=>{
      console.log(err);
    });
  }


  const get_one_markets = async(req,res)=>{
    const { market_id } = req.params;
    Markets.findOne({
      include:[
        {
          model:PhoneNumbers,
          attributes:["id","phoneNumber"],
          // where:{deleted:false,active:true}
        },
        {
          model:MarketAddress,
          attributes:["id","name_tm","name_ru","name_en","description_tm","description_ru","description_en","createdAt","updatedAt","MarketId"],
          
          // where:{deleted:false,active:true}
        },
        {
          model:MarketKategoriya,
          attributes:["id","name_tm","name_ru","name_en","createdAt","updatedAt","MarketId",'active',"deleted"],
          // where:{deleted:false,active:true},
          include:[
           { model:MarketSubKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
          },
          {
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat"]
          }
          ]
        }        
              ],
        where:{
          id:market_id
        }

    }).then((data)=>{
      res.status(200).json(data);
    }).catch((err)=>{
      console.log(err);
    });
  }

  

const uploadSurat = async(req,res)=>{
  const { market_id } = req.params;
  let data = req.body.data;
  console.log("coming file",data);
  console.log("image name:",data.img_name);
  console.log("image:",data.img);

 // getting base64 image and converting to buffer
  function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
      response = {};
  
    if (matches.length !== 3) {
      return new Error('Invalid input string');
    }
  
    response.type = matches[1];
    response.data = new Buffer(matches[2], 'base64');
  
    return response;
  }
  var imageBuffer = decodeBase64Image(req.body.data.img);
  console.log(imageBuffer);
// converting buffer to original image to /upload folder
  fs.writeFile(`./uploads/${req.body.data.img_name}`, imageBuffer.data, function(err) { console.log(err) });
  
}
  const create_market = (req,res)=>{
    // const { name_tm,name_ru,name_en, phoneNumber, addressName_tm,addressName_ru,addressName_en, description_tm,description_ru,description_en,surat_name } = req.body;
    // const phoneNumbers = phoneNumber;

    let data = req.body.data;
    // console.log("coming file",data);
    // console.log("image name:",data.img_name);
    // console.log("image:",data.img);
  
   // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
      var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');
    
      return response;
    }
    var imageBuffer = decodeBase64Image(req.body.data.img);
  // converting buffer to original image to /upload folder
  let randomNumber = Math.floor(Math.random() * 999999999999);
  console.log("random Number:",randomNumber);
  let img_direction = `./uploads/`+randomNumber+`${req.body.data.img_name}`;
    fs.writeFile(img_direction, imageBuffer.data, function(err) { console.log(err) });
    ///////////////////////////////////////////////////////////////////////////////////////////
    // console.log("data::::::::",data);
    Markets.create({
      name_tm:data.name_tm,
      name_ru:data.name_ru,
      name_en:data.name_en,
      surat:img_direction,
      dastawkaStartI:data.dastawkaStartI,
      dastawkaEndI:data.dastawkaEndI,
      dastawkaStartII:data.dastawkaStartII,
      dastawkaEndII:data.dastawkaEndII,
      dastawkaPrice:data.dastawkaPrice,
      is_cart:data.is_cart,
      is_online:false,
      is_aksiya:false,
      active:true,
      cashBackPrice:data.cashBackPrice,
      cashBackPrasent:data.cashBackPrasent,
      cashBack:data.cashBack,
      currency_exchange:data.currency_exchange,
      addres_tm:data.address_tm,
      addres_ru:data.address_ru,
      addres_en:data.address_en,
      tel:data.phoneNumber,
      description_tm:data.description_tm,
      description_ru:data.description_ru,
      description_en:data.description_en,
      aksiyaLimit:null,
      deleted:false,
      WelayatlarId:data.WelayatlarId,
      KategoriyaOfMarketId:data.KategoriyaOfMarketId
    }).then(async(response)=>{
    
        await PhoneNumbers.create({
          MarketId:response.id,
          phoneNumber:data.phoneNumber,
        })
    
         await MarketAddress.create({
          name_tm:data.address_tm,
          name_ru:data.address_ru,
          name_en:data.address_en,
          description_tm:data.description_tm,
          description_ru:data.description_ru,
          description_en:data.description_en,
          MarketId:response.id,
      })
      res.json({
        msg:"successfully",
      })
    }).catch(err=>{
      console.log("errrrr",err);
    })
  }

  const update_markets = async(req,res)=>{

    let data = req.body.surat;
    // console.log("coming file",data);
    // console.log("image name:",data.img_name);
    // console.log("image:",data.img);
  
   // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
      var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');
    
      return response;
    }
    let img_direction = ""
    if(req.body.surat.img){
      var imageBuffer = decodeBase64Image(req.body.surat.img);
      // console.log(imageBuffer);
      // converting buffer to original image to /upload folder
      let randomNumber = Math.floor(Math.random() * 999999999999);
      console.log("random Number:",randomNumber);
      img_direction = `./uploads/`+randomNumber+`${req.body.surat.img_name}`;
      fs.writeFile(img_direction, imageBuffer.data, function(err) { console.log(err) });
    }else{
      img_direction=null;
    }


    const { active,
      cashBackPrice,
      cashBackPrasent,
      cashBack,addres_tm,
      addres_ru,
      addres_en,
      tel,currency_exchange,
      description_tm,
      description_ru,is_online,is_aksiya,aksiyaLimit,WelayatlarId,KategoriyaOfMarketId,
      description_en, name_tm,name_ru,name_en,is_cart,dastawkaPrice,dastawkaEndII,dastawkaStartI,dastawkaEndI,dastawkaStartII } = req.body;
    const { market_id } = req.params;
    const foundMarket = await Markets.findOne({
      where:{
        id:market_id
      }
    });
    if(foundMarket){
      if(req.body.surat){
       await fs.unlink(foundMarket.surat,(err)=>{console.log("updated")})
    }
      Markets.update({
        name_tm:name_tm,
        name_ru:name_ru,
        name_en:name_en,
        addres_tm,
        addres_ru,
        addres_en,
        tel,
        description_tm,
        description_ru,
        description_en,
        surat:img_direction,

        dastawkaStartI:dastawkaStartI,
        dastawkaEndI:dastawkaEndI,
        dastawkaStartII:dastawkaStartII,
        dastawkaEndII:dastawkaEndII,
        dastawkaPrice:dastawkaPrice,
        is_cart:is_cart,
        is_online,
        is_aksiya,
        aksiyaLimit,
        currency_exchange,
        active,
        cashBackPrice,
        cashBackPrasent,
        cashBack,
        
        WelayatlarId,
        KategoriyaOfMarketId,
      },
      {
        where:{
          id:market_id,
        }
      }).then((data)=>{
        res.status(200).json({
          msg:"successfully",
          name:img_direction
        })
      }).catch((err)=>{
        console.log(err);
      });
    }else{
      res.json({
        msg:"Bu ID boyuncha market tapylmady!"
      })
    }
  };

  const updateMarketWithoutSurat = async(req,res)=>{
    const { active,
      cashBackPrice,
      cashBackPrasent,
      cashBack,addres_tm,
      addres_ru,
      addres_en,
      tel,
      description_tm,
      description_ru,is_online,currency_exchange,
      is_aksiya,
      aksiyaLimit,WelayatlarId,
      KategoriyaOfMarketId,deleted,
      description_en,name_tm,name_ru,name_en,is_cart,dastawkaPrice,dastawkaEndII,dastawkaStartI,dastawkaEndI,dastawkaStartII } = req.body;
   
      const { market_id } = req.params;
    const foundMarket = await Markets.findOne({
      where:{
        id:market_id
      }
    });
    if(foundMarket){
      Markets.update({
        name_tm:name_tm,
        name_ru:name_ru,
        name_en:name_en,
        addres_tm,
        addres_ru,
        addres_en,
        tel,
        description_tm,
        description_ru,
        description_en,
        dastawkaStartI:dastawkaStartI,
        dastawkaEndI:dastawkaEndI,
        dastawkaStartII:dastawkaStartII,
        dastawkaEndII:dastawkaEndII,
        dastawkaPrice:dastawkaPrice,
        currency_exchange,
        is_cart:is_cart,
        is_online,
        is_aksiya,
        aksiyaLimit,
        active,
        deleted,
        cashBackPrice,
        cashBackPrasent,
        cashBack,
        WelayatlarId,
        KategoriyaOfMarketId,
      },
      {
        where:{
          id:market_id,
        }
      }).then((data)=>{
        res.status(200).json({
          msg:"successfully",
        })
      }).catch((err)=>{
        console.log(err);
      });
    }else{
      res.json({
        msg:"Bu ID boyuncha market tapylmady!"
      })
    }
  }


  const delete_market = async(req,res)=>{
    const { market_id } = req.params;
    const foundMarket = await Markets.findOne({
      where:{
        id:market_id
      }
    });
    if(foundMarket){
      await  MarketAddress.update({
        deleted:true,
      },{
        where:{
          MarketId:market_id
        }
      });
      await Products.update({
        deleted:true
      },{
            where:{
              MarketId:market_id,
            }
          });
      await PhoneNumbers.update({
        deleted:true,
      },{
                where:{
                  MarketId:market_id,
                }
              });
      await MarketKategoriya.update({
        deleted:true,
      },{
                where:{
                  MarketId:market_id,
                }
              });

      await Markets.update({
        active:false,
        deleted:true,
      },{
        where:{
          id:market_id,
        }
      }).then(()=>{
                    res.json({
                      msg:"successfully",
                    })
                  }).catch((err)=>{
                    console.log(err);
                  })
                }else{
                  res.json({
                    msg:"BU ID boyuncha market Tapylmady!"
                  })
                }          
  }

  const destroy_market = async(req,res)=>{
    const { market_id } = req.params;
    const foundMarket = await Markets.findOne({
      where:{
        id:market_id
      }
    });
    if(foundMarket){
      await  MarketAddress.destroy({
        where:{
          MarketId:market_id
        }
      });
      await Products.destroy({
            where:{
              MarketId:market_id,
            }
          });
      await PhoneNumbers.destroy({
                where:{
                  MarketId:market_id,
                }
              });
      await MarketKategoriya.destroy({
        where:{
          MarketId:market_id,
        }
      });
      await Markets.destroy({
        where:{
          id:market_id,
        }
      }).then(()=>{
                    res.json({
                      msg:"successfully",
                    })
                  }).catch((err)=>{
                    console.log(err);
                  })
                }else{
                  res.json({
                    msg:"BU ID boyuncha market Tapylmady!"
                  })
                }          
  }


  exports.markets_tb=markets_tb;

  exports.getOneMarketDes = getOneMarketDes;

  exports.get_all_markets = get_all_markets;
  exports.get_one_markets = get_one_markets;
  exports.create_market = create_market;
  exports.update_markets = update_markets;
  exports.updateMarketWithoutSurat = updateMarketWithoutSurat;
  exports.delete_market = delete_market;
  exports.destroy_market = destroy_market;

  exports.uploadSurat = uploadSurat;

exports.markets_tb = markets_tb;