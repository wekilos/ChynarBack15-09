var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const MarketKategoriya = require("../models/marketKategoriya");
const Markets = require("../models/markets");
const MarketSubKategoriya = require("../models/marketSubKategoriya");
const Op = Sequelize.Op;

const marketSubKategoriya_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = MarketSubKategoriya.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}



const getAllKategoriya = async(req,res)=>{
    const { kategory_id } = req.params;
    const {active,deleted} = req.query;
    var Active = active ?
      {
        active:{[Op.eq]:active},
      }
      :{
        active:{[Op.eq]:true},
      };

    var Deleted = deleted 
    ? {
      deleted :{ [Op.eq]:deleted}
    }
    : {
      deleted :{ [Op.eq]:false}
    };
    const foundMarket = await MarketKategoriya.findOne({
        where:{
            id:kategory_id,
        }
    });
    if(foundMarket){
        MarketSubKategoriya.findAll(
            {
                include:[{
                    model:MarketKategoriya,
                    attributes:["id","name_tm","name_ru","name_en","active"],
                    // where:{deleted:false,active:true}
                }],
            where:{
                [Op.and]: [Active,{MarketKategoriyaId:kategory_id},Deleted],
            }
        }).then((data)=>{
            res.json(data)
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        });
    }else{
        res.json({
            msg:"BU ID boyuncha MarketKategoryya tapylmady"
        })
    }
}

const getOneKategoriya = async(req,res)=>{
    const { subKategoriya_id } = req.params;
   
        MarketSubKategoriya.findOne({
                include:[{
                    model:MarketKategoriya,
                    attributes:["id","name_tm","name_ru","name_en","active"],
                    // where:{deleted:false,active:true}
                }],
            where:{
                id:subKategoriya_id,
            }
        }).then((data)=>{
            res.json(data)
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        });
    
}

const createKategoriya = async(req,res)=>{
    const { kategory_id } = req.params;
    const { name_tm, name_ru, name_en, active } = req.body;
    
    const foundMarket = await MarketKategoriya.findOne({
        where:{
            id:kategory_id
        }
    });

    if(foundMarket){
        MarketSubKategoriya.create({
            name_tm,
            name_ru,
            name_en,
            active:true,
            MarketKategoriyaId:kategory_id,
            deleted:false,
        }).then(()=>{
            res.json({
                msg:"successfully!",
            })
        }).catch(err=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"BU ID boyuncha Market tapylmady"
        })
    }
}

const updateKategoriya = async(req,res)=>{
    const { subKategoriya_id } = req.params;
    const { market_id, name_tm, name_ru, name_en, active,deleted,MarketKategoriyaId } = req.body;

    const foundKategoriya = await MarketSubKategoriya.findOne({
        where:{
            id:subKategoriya_id,
        }
    });
    // const foundMarket = await Markets.findOne({
    //     where:{
    //         id:market_id,
    //     }
    // });
    // if(foundMarket){
    if(foundKategoriya){
        MarketSubKategoriya.update({
            name_en,
            name_ru,
            name_tm,
            active,
            deleted,
            MarketKategoriyaId,
        },
        {
        where:{
            [Op.and]: [
                { id:subKategoriya_id},
                // { MarketId:market_id }
              ]
        }
    }).then(()=>{
        res.json({
            msg:"successfully!",
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error",
            err:err
        })
    })
    }else{
        res.json({
            msg:"BU ID boyuncha Kategoriya tapylmady!"
        })
    }
// }else{
//     res.json({
//         msg:"BU ID boyuncha Market tapylmady!"
//     })
// }
}

const deleteKategoriya = async(req,res)=>{
    const { subKategoriya_id } = req.params;
    const foundKategoriya = await MarketSubKategoriya.findOne({
        where:{
            id:subKategoriya_id,
        }
    });
    if(foundKategoriya){
        MarketSubKategoriya.update({
            deleted:true,
            active:false,
        },{
            where:{
                id:subKategoriya_id,
            }
        }).then(()=>{
                res.json({
                    msg:"successfully!"
                })
            }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error!"
            })
        })
    }else{
        res.json({
            msg:"BU ID boyuncha Market Kategoriya tapylmady"
        })
    }
}

const destroyKategoriya = async(req,res)=>{
    const { subKategoriya_id } = req.params;
    const foundKategoriya = await MarketSubKategoriya.findOne({
        where:{
            id:subKategoriya_id,
        }
    });
    if(foundKategoriya){
        MarketSubKategoriya.destroy({
            where:{
                id:subKategoriya_id,
            }
        }).then(()=>{
                res.json({
                    msg:"successfully!"
                })
            }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error!"
            })
        })
    }else{
        res.json({
            msg:"BU ID boyuncha Market Kategoriya tapylmady"
        })
    }
}








exports.marketSubKategoriya_tb=marketSubKategoriya_tb;

exports.getAllKategoriya = getAllKategoriya;
exports.getOneKategoriya = getOneKategoriya;
exports.createKategoriya = createKategoriya;
exports.updateKategoriya = updateKategoriya;
exports.deleteKategoriya = deleteKategoriya;
exports.destroyKategoriya = destroyKategoriya;

