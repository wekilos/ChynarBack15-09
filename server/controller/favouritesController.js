var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Favourites = require("../models/favourites");
const Users = require("../models/users");
const Products = require("../models/products");
const Op = Sequelize.Op;

const favourites_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Favourites.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAllFavorites = async(req,res)=>{
    Favourites.findAll().then((data)=>{
        res.json(data)
    }).catch((err)=>{
        console.log(err);
        res.json("error")
    })
}

const getProductFavor = async(req,res)=>{
    const { product_id } = req.params;
    Favourites.findAndCountAll({
        include:[
            {
                model:Users
            },
            {
                model:Products
            }
        ],
        where:{
            ProductId:product_id
        }
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error")
    })
}

const getUserFavor = async(req,res)=>{
    const { user_id } = req.params;
    Favourites.findAndCountAll({
        include:[
            {
                model:Users
            },
            {
                model:Products
            }
        ],
        where:{
            UserId:user_id
        }
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error")
    })
}

const create = async(req,res)=>{
    const { user_id, product_id } = req.body;
    const foundFav = await Favourites.findOne({where:{
        [Op.and]:[{ProductId:product_id},{UserId:user_id}]
    }});
    if(!foundFav){
        Favourites.create({
            ProductId:product_id,
            UserId:user_id,
        }).then(()=>{
            res.json("successfully!");
        }).catch(err=>{
            console.log(err);
            res.json("error!")
        })
    }else{
        res.json("Siz eyyam Like goydynyz")
    }
}

const Delete = async(req,res)=>{
    const { user_id, product_id } = req.body;
    const foundfov = await Favourites.findOne({where:{
        [Op.and]:[{UserId:user_id},{ProductId:product_id}]
    }});
    if(foundfov){
        Favourites.destroy({
            where:{
                [Op.and]:[{UserId:user_id},{ProductId:product_id}]
            }
        }).then(()=>{
            res.json({
                msg:"successfully!"
            })
        }).catch((err)=>{
            console.log(err);
            res.json("error");
        })
    }else{
        res.json({
            msg:"Bu Id boyuncha Favourite tapylmady!"
        })
    }
}

exports.favourites_tb = favourites_tb;

exports.getAllFavorites = getAllFavorites;
exports.getProductFavor = getProductFavor;
exports.getUserFavor = getUserFavor;
exports.create = create;
exports.Delete = Delete;