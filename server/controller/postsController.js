var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Posts = require("../models/posts");
const Users = require("../models/users");
const UserType = require("../models/userType");
const Op = Sequelize.Op;

const post_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Posts.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const getAllPosts = async(req,res)=>{
    Posts.findAll({
        include:[{
            model:Users,
            attributes:["id","fname","lastname","phoneNumber"],
            include:[{
                model:UserType,
                attributes:["id","type_tm","type_ru","type_en","createdAt","updatedAt"]
            }]
        }]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    })
}

const getUserPosts = async(req,res)=>{
    const { UserId } = req.params;
    Posts.findAll({
        include:[{
            model:Users,
            attributes:["id","fname","lastname","phoneNumber"]
        }]
    },{
        where:{
            id:UserId
        }
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    })
}


const create = async(req,res)=>{
    const { UserId } = req.params;
    const {description_tm, description_ru, description_en, slug_tm, slug_ru, slug_en,} = req.body;

    const user = await Users.findOne({
        where:{
            id:UserId
        }
    });

    if(user){
        Posts.create({
            description_tm, 
            description_ru, 
            description_en, 
            slug_tm, 
            slug_ru, 
            slug_en,
            UserId
        }).then(()=>{
            res.json({
                msg:"Successfully"
            })
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"Bu ID boyuncha User yok."
        })
    }

}

const update = async(req,res)=>{
    const { PostId } = req.params;
    const {description_tm, description_ru, description_en, slug_tm, slug_ru, slug_en,} = req.body;

    const post = await Posts.findOne({
        where:{
            id:PostId
        }
    });

    if(post){
        Posts.update({
            description_tm, 
            description_ru, 
            description_en, 
            slug_tm, 
            slug_ru, 
            slug_en
        },{
            where:{
                id:PostId
            }
        }).then(()=>{
            res.json({
                msg:"Successfully"
            })
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"Bu ID boyuncha post yok."
        })
    }

}

const Delete = async (req,res)=>{
    const { PostId } = req.params;

    const post = await Posts.findOne({
        where:{
            id:PostId
        }
    });
    if(post){
        Posts.destroy({
            where:{
                id:PostId
            }
        }).then(()=>{
            res.json({
                msg:"Successfully"
            })
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"Bu ID boyuncha User yok."
        })
    }
}

exports.post_tb = post_tb;

exports.getAllPosts = getAllPosts;
exports.getUserPosts = getUserPosts;
exports.create = create;
exports.update = update;
exports.Delete = Delete;