var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Welayatlar = require("../models/welayatlar");
const Markets = require("../models/markets");
const Op = Sequelize.Op;

const welayatlar_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Welayatlar.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAll = async(req,res)=>{
    const {active,deleted} = req.query;

    var Active = active
    ? {
        active: { [Op.eq]: active },
    }
    : {
        active: { [Op.eq]: true },
    };

    var Deleted = deleted
    ? {
        deleted: { [Op.eq]: deleted },
    }
    : {
        deleted: { [Op.eq]: false },
    };

    Welayatlar.findAll({
        where:{
            [Op.and]:[Deleted,Active]
        },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.status(200).json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const create = async(req,res)=>{
    const {name_tm,name_ru,name_en,
        short_name_tm,
        short_name_ru,
        short_name_en,} = req.body;

    Welayatlar.create({
        name_tm,
        name_ru,
        name_en,
        short_name_tm,
        short_name_ru,
        short_name_en,
        active:true,
        deleted:false,
    }).then((data)=>{
        res.status(200).json("created")
    }).catch((err)=>{
        console.log(err);
    })
}

const Update = async(req,res)=>{
    const {id} = req.params;
    const {name_tm,name_ru,name_en,
        short_name_tm,
        short_name_ru,
        short_name_en,active, deleted, } = req.body;

    Welayatlar.update({
        name_tm,
        name_ru,
        name_en,
        short_name_tm,
        short_name_ru,
        short_name_en,
        active,
        deleted,
    },
    {
        where:{id:id}
    }).then((data)=>{
        res.status(200).json(data)
    }).catch((err)=>{
        console.log(err);
    })
}
const Delete = async(req,res)=>{
    const {id} = req.params;

    Welayatlar.update({
        deleted:true,
        active:false,
    },
    {
        where:{id:id}
    }).then((data)=>{
        res.status(200).json("deleted")
    }).catch((err)=>{
        console.log(err);
    })
}

const Destroy = async(req,res)=>{
    const {id} = req.params;

    Welayatlar.destroy(
    {
        where:{id:id}
    }).then((data)=>{
        res.status(200).json("deleted")
    }).catch((err)=>{
        console.log(err);
    })
}

exports.welayatlar_tb = welayatlar_tb;
exports.getAll = getAll;
exports.create = create;
exports.Update = Update;
exports.Delete = Delete;
exports.Destroy = Destroy;