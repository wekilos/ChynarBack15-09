var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Units = require("../models/units");
const Op = Sequelize.Op;

const units_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Units.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAllUnits = async(req,res)=>{
    const {active,deleted} = req.query;
    var Active = active
    ? {
        active: { [Op.eq]: active },
    }
    : {
        active: { [Op.eq]: true },
    };

    var Deleted = deleted
    ? {
        deleted: { [Op.eq]: deleted },
    }
    : {
        deleted: { [Op.eq]: false },
    };

    Units.findAll({
        where: {
            [Op.and]: [Active,Deleted],
          },
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    })
}
const getOneUnits = async(req,res)=>{
    const { unit_id } = req.params;
    Units.findOne({where:{id:unit_id}}).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    })
}

const create = async(req,res)=>{
    const { name_tm,name_ru,name_en,active } = req.body;
    Units.create({
        name_tm,
        name_ru,
        name_en,
        active:true,
        deleted:false
    }).then(()=>{
        res.json({
            msg:"successfully"
        });
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    })
}

const update = async(req,res)=>{
    const { unit_id } = req.params;
    const { name_tm,name_ru,name_en,active,deleted } = req.body;
    const foundUnit = await Units.findOne({
        where:{
            id:unit_id
        }
    })
    if(foundUnit){
    Units.update({
        name_tm,
        name_ru,
        name_en,
        active,
        deleted
    },
    {
        where:{
            id:unit_id,
        }
    }).then(()=>{
        res.json({
            msg:"successfully"
        });
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    });
}else{
    res.json({
        msg:"BU ID boyuncha Unit tapylmady!"
    })
}
}

const Delete = async(req,res)=>{
    const { unit_id } = req.params;
    const foundUnit = await Units.findOne(
        {
        where:{
            id:unit_id
        }
    });
    if(foundUnit){
    Units.update(
        {
            deleted:true,
            active:false
      },
        {
        where:{
            id:unit_id
        }
    }).then(()=>{
        res.json({
            msg:"successfully"
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    });
}else{
    res.json({
        msg:"Bu ID boyuncha unit tapylmady"
    })
}
}


const Destroy = async(req,res)=>{
    const { unit_id } = req.params;
    const foundUnit = await Units.findOne(
        {
        where:{
            id:unit_id
        }
    });
    if(foundUnit){
    Units.destroy({
        where:{
            id:unit_id
        }
    }).then(()=>{
        res.json({
            msg:"successfully"
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    });
}else{
    res.json({
        msg:"Bu ID boyuncha unit tapylmady"
    })
}
}



exports.units_tb = units_tb;

exports.getAllUnits = getAllUnits;
exports.getOneUnits = getOneUnits;
exports.create = create;
exports.update = update;
exports.Delete = Delete;
exports.Destroy = Destroy;