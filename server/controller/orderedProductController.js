var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const OrderedProduct = require("../models/orderedProduct");
const Op = Sequelize.Op;

const orderedProduct_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = OrderedProduct.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}



exports.orderedProduct_tb = orderedProduct_tb;