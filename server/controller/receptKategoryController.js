var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const ReseptKategoriya = require("../models/reseptKategory");

const receptKategory_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = ReseptKategoriya.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

exports.receptKategory_tb = receptKategory_tb;