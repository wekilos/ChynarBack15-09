var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Status = require("../models/status");
const { getAllSliders } = require("./sliderController");
const Op = Sequelize.Op;

const status_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Status.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAllStatus = async(req,res)=>{
    const {active,deleted} = req.query;
    var Active = active
    ? {
        active: { [Op.eq]: active },
    }
    : {
        active: { [Op.eq]: true },
    };

    var Deleted = deleted
    ? {
        deleted: { [Op.eq]: deleted },
    }
    : {
        deleted: { [Op.eq]: false },
    };
    
    Status.findAll({
        where: {
            [Op.and]: [Active,Deleted],
          },
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error");
    })
}

const getOneStatus = async(req,res)=>{
    const { status_id } = req.params;

    Status.findOne({
        where:{
            id:status_id,
        }
    }).then((data)=>{
        if(data===null){
            res.json("Bu Id boyuncha Status yok!")
        }else{
        res.json(data);
        }
    }).catch((err)=>{
        console.log(err);
        res.json("error");
    });

}

const create = async(req,res)=>{
    const { name_tm, name_ru, name_en, active } = req.body;
    Status.create({
        name_tm, 
        name_ru, 
        name_en,
        active:true,
        deleted:false
    }).then(()=>{
        res.json({
            msg:"Successfully"
        })
    }).catch((err)=>{
    console.log(err);
    res.json({
        msg:"error"
    })
    })
}

const update = async(req,res)=>{
    const { status_id } = req.params;
    const { name_tm, name_ru, name_en, active, deleted } = req.body;
    const founndSlide = await Status.findOne({
        where:{
            id:status_id,
        }
    });
   
    if(founndSlide){
        
    Status.update({
        name_tm,
        name_ru, 
        name_en,
        active,deleted, 
    },
    {
        where:{
            id:status_id
        }
    }
    ).then(()=>{
        res.json({
            msg:"Successfully"
        });
    }).catch((err)=>{
    console.log(err);
    res.json({
        msg:"error"
    })
    })
}else{
    res.json({
        msg:"BU ID boyuncha Status tapylmady!"
    })
}
}

const Delete = async(req,res)=>{
    const { status_id } = req.params;
    const founndSlide = await Status.findOne({
        where:{
            id:status_id,
        }
    });
    if(founndSlide){
    Status.update(
        {
            deleted:true,
            active:false
      },
      {
        where:{
            id:status_id,
        }
    }).then(()=>{
        res.json({
            msg:"successfully"
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error",
        })
    });
}else{
    res.json({
        msg:"Bu ID boyuncha Status tapylmady"
    })
}
}

const Destroy = async(req,res)=>{
    const { status_id } = req.params;
    const founndSlide = await Status.findOne({
        where:{
            id:status_id,
        }
    });
    if(founndSlide){
    Status.destroy({
        where:{
            id:status_id,
        }
    }).then(()=>{
        res.json({
            msg:"successfully"
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error",
        })
    });
}else{
    res.json({
        msg:"Bu ID boyuncha Status tapylmady"
    })
}
}

exports.status_tb = status_tb;

exports.getAllStatus = getAllStatus;
exports.getOneStatus = getOneStatus;
exports.create = create;
exports.update = update;
exports.Delete = Delete;
exports.Destroy = Destroy;
