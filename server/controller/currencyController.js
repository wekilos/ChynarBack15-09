var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Currency = require("../models/currency");
const Configs = require("../models/config");
const Op = Sequelize.Op;

const currency_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Currency.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const getAllCurrency = async(req,res)=>{
    Currency.findAll().then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error"
        })
    })
}

const Create = async(req,res)=>{
    const { ConfigId } = req.params;
    const { name_tm, name_ru, name_en } = req.body;

    const config = Configs.findOne({
        where:{
            id:ConfigId
        }
    });
    if(config){
        Currency.create({
            name_tm, 
            name_ru, 
            name_en,
            ConfigId,
        }).then(()=>{
            res.json({
                msg:"successfully"
            })
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"Bu Id boyuncha Config tapylmady"
        })
    }
}

const update = async(req,res)=>{
    const { CurrencyId } = req.params;
    const { name_tm, name_ru, name_en } = req.body;

    const currency = await Currency.findOne({
        where:{
            id:CurrencyId
        }
    });
    if(currency){
        Currency.update({
            name_tm, 
            name_ru, 
            name_en
        },{
            where:{
                id:CurrencyId
            }
        }).then(()=>{
            res.json({
                msg:"successfully"
            })
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"Bu Id boyuncha Currency tapylmady"
        })
    }
}

const Delete = async(req,res)=>{
    const { CurrencyId } = req.params;
    const currency = Currency.findOne({
        where:{
            id:CurrencyId
        }
    });
    if(currency){
        Currency.destroy({
           where:{
               id:CurrencyId
           }
        }).then(()=>{
            res.json({
                msg:"successfully"
            })
        }).catch((err)=>{
            console.log(err);
            res.json({
                msg:"error"
            })
        })
    }else{
        res.json({
            msg:"Bu Id boyuncha Currency tapylmady"
        })
    }
}



exports.currency_tb = currency_tb;

exports.Create = Create;
exports.getAllCurrency = getAllCurrency;
exports.update = update;
exports.Delete = Delete;