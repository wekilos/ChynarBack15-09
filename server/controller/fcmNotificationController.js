var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
var Notification = require("../models/fcmNorification");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const { admin } = require("../functions/fcm");
const fcmToken = require("../models/fcmTokens");

const notification_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Notification.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
};


const fcmSendWithTopic = async(req,res)=>{
    const { title,text, topic, } = req.body;

    
        const message = {
            notification: {
                title: title,
                body: text
            },
            topic: topic
        };

        const createdNotification = await Notification.create({
            name_tm:title,
            text_tm:text,
            device:topic,
            active:true,
            deleted:false
        })

        if(createdNotification){
            admin.messaging().send(message)
            .then((resp) => {
                // Response is a message ID string.
                console.log('Successfully sent message:', resp);
                res.json(resp);
            })
            .catch((error) => {
                console.log('Error sending message:', error);
                res.json(error);
            });
        }else{
            res.json("tazeden synanyshyn");
        }


}


const sendOneUser = async(req,res)=>{
    const { title,text,userId } = req.body;

    userTokens = await fcmToken.findAll({
        where:{
            UserId:userId
        }
    });
    var tokens = [];
    userTokens?.map((fcm)=>{
        tokens.push(fcm.fcmToken)
    })
    // res.json({userTokens,tokens})
    const message = {
        notification: {
            title: title,
            body: text
        },
        token: tokens[0]
    };

        admin.messaging().send(message)
        .then((resp) => {
            // Response is a message ID string.
            console.log('Successfully sent message:', resp);
            res.json(resp);
        })
        .catch((error) => {
            console.log('Error sending message:', error);
            res.json(error);
        });


}

const senAllUser = async(req,res)=>{
    const { title, text, type } = req.body;

    var Type = type? {type:type}: null;

    let tokens = await fcmToken.findAll({
        where:{
            Type
        }
    });

    res.json(tokens);

    let registrationTokens = [];
    tokens?.map((fcm,i)=>{
        registrationTokens.push(fcm.fcmToken)
    })

    const  message = {
        notification:{
            title:title,
            body:text
        },
        tokens: registrationTokens,
    }

    const createdNotification = await Notification.create({
        name_tm:title,
        text_tm:text,
        device:topic,
        active:true,
        deleted:false
    })

    if(createdNotification){
    admin.messaging().send(message)
    .then((resp) => {
        // Response is a message ID string.
        console.log('Successfully sent message:', resp);
        res.json(resp);
    })
    .catch((error) => {
        console.log('Error sending message:', error);
        res.json(error);
    });
    }else{
        res.json("gaytadan barlan!")
    }
}









exports.notification_tb = notification_tb;

exports.fcmSendWithTopic = fcmSendWithTopic;
exports.sendOneUser = sendOneUser;
exports.senAllUser = senAllUser;