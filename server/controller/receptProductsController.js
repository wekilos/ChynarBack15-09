var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const ReseptProduct = require("../models/reseptProducts");

const receptProducts_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = ReseptProduct.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

exports.receptProducts_tb = receptProducts_tb;