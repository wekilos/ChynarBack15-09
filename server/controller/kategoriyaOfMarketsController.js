var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const KategoriyaOfMarkets = require("../models/KategoriyaOfMarkets");
const Markets = require("../models/markets");
const Op = Sequelize.Op;

const kategoryyaOfMarkets_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = KategoriyaOfMarkets.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAll = async(req,res)=>{
    const {WelayatlarId} = req.params;
    const {active,deleted } =req.query;

    var Active = active ?
    {
      active:{[Op.eq]:active},
    }
    :{
      active:{[Op.eq]:true},
    };

  var Deleted = deleted 
  ? {
    deleted :{ [Op.eq]:deleted}
  }
  : {
    deleted :{ [Op.eq]:false}
  };

    KategoriyaOfMarkets.findAll({
        include:[
            {
                model:Markets,
                attributes:["id","name_tm","name_ru","name_en","surat","dastawkaStartI","dastawkaEndI","dastawkaStartII","dastawkaEndII","dastawkaPrice","is_cart","active","cashBack","cashBackPrasent","currency_exchange","tel","description_tm","description_ru","description_en","dastawkaPrice"],
                where:{deleted:false,active:true}
            }
        ],
        where:{
            [Op.and]:[{WelayatlarId:WelayatlarId},Active,Deleted]
        },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const getOne = async(req,res)=>{
    const {id} = req.params;
    const {active,deleted } =req.query;

    

    KategoriyaOfMarkets.findAll({
        include:[
            {
                model:Markets,
                attributes:["id","name_tm","name_ru","name_en","surat","dastawkaStartI","dastawkaEndI","dastawkaStartII","dastawkaEndII","dastawkaPrice","is_cart","active","deleted","cashBack","cashBackPrasent","currency_exchange","tel","description_tm","description_ru","description_en","dastawkaPrice"],
                // where:{deleted:false,active:true}
            }
        ],
        where:{
            [Op.and]:[{id:id}]
        },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const create = async(req,res)=>{
    const {WelayatlarId} = req.params;
    const {name_tm,name_ru,name_en,active} = req.body;

    KategoriyaOfMarkets.create({
        name_tm, 
        name_ru,
        name_en,
        active:true,
        deleted:false,
        WelayatlarId,
    }).then((data)=>{
        res.status(200).json(data);
    }).catch((err)=>{
        console.log(err);
        res.json(err)
    })
}
const Update = async(req,res)=>{
    const {id} = req.params;
    const {name_tm,name_ru,name_en,active,deleted,WelayatlarId} = req.body;

    KategoriyaOfMarkets.update({
        name_tm, 
        name_ru,
        name_en,
        WelayatlarId,
        active,
        deleted,
    },{
        where:{id:id}
    }).then((data)=>{
        res.status(200).json("updated");
    }).catch((err)=>{
        console.log(err);
    })
}

const Delete = async(req,res)=>{
    const {id} = req.params;

    KategoriyaOfMarkets.update({
        deleted:true,
        active:false,
    },{
        where:{id:id}
    }).then((data)=>{
        res.status(200).json("deleted");
    }).catch((err)=>{
        console.log(err);
    })
}

const Destroy = async(req,res)=>{
    const {id} = req.params;

    KategoriyaOfMarkets.destroy({
        where:{id:id}
    }).then((data)=>{
        res.status(200).json("deleted");
    }).catch((err)=>{
        console.log(err);
    })
}

exports.kategoryyaOfMarkets_tb = kategoryyaOfMarkets_tb;
exports.getAll = getAll;
exports.getOne =getOne;
exports.create = create;
exports.Update = Update;
exports.Delete = Delete;
exports.Destroy = Destroy;