var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const fs = require("fs");
const Sliders = require("../models/sliders");
const Op = Sequelize.Op;


const sliders_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Sliders.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAllSliders = async(req,res)=>{
    Sliders.findAll().then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json("error");
    })
}


const create = async(req,res)=>{
    let data = req.body.data;
    // console.log("coming file",data);
    // console.log("image name:",data.img_name);
    // console.log("image:",data.img);
  
   // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
      var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');
    
      return response;
    }
    var imageBuffer_tm = decodeBase64Image(req.body.data.img_tm);
    // console.log(imageBuffer);
  // converting buffer to original image to /upload folder
  let randomNumber_tm = Math.floor(Math.random() * 999999999999);
  console.log("random Number:",randomNumber_tm);
  let img_direction_tm = `./uploads/`+randomNumber_tm+`${req.body.data.img_name_tm}`;
    fs.writeFile(img_direction_tm, imageBuffer_tm.data, function(err) { console.log(err) });
    ///////////////////////////////////////////////////////////////////////////////////////////
    var imageBuffer_ru = decodeBase64Image(req.body.data.img_ru);
    // console.log(imageBuffer);
  // converting buffer to original image to /upload folder
  let randomNumber_ru = Math.floor(Math.random() * 999999999999);
  console.log("random Number:",randomNumber_ru);
  let img_direction_ru = `./uploads/`+randomNumber_ru+`${req.body.data.img_name_ru}`;
    fs.writeFile(img_direction_ru, imageBuffer_ru.data, function(err) { console.log(err) });
    ///////////////////////////////////////////////////////////////////////////////////////////
    var imageBuffer_en = decodeBase64Image(req.body.data.img_en);
    // console.log(imageBuffer);
  // converting buffer to original image to /upload folder
  let randomNumber_en = Math.floor(Math.random() * 999999999999);
  console.log("random Number:",randomNumber_en);
  let img_direction_en = `./uploads/`+randomNumber_en+`${req.body.data.img_name_en}`;
    fs.writeFile(img_direction_en, imageBuffer_en.data, function(err) { console.log(err) });
    ///////////////////////////////////////////////////////////////////////////////////////////
   


    const { title_tm, title_ru, title_en, link } = req.body;
    console.log("file::::::",req.file);
    Sliders.create({
        title_tm:img_direction_tm, 
        title_ru:img_direction_ru, 
        title_en:img_direction_en,
        link:data.link,
        photo_url:null,
    }).then(()=>{
        res.json({
            msg:"Successfully"
        })
    }).catch((err)=>{
    console.log(err);
    res.json({
        msg:"error"
    })
    })
}


const update = async(req,res)=>{
    let data = req.body.data;
    const { slider_id } = req.params;
    const founndSlide = await Sliders.findOne({
        where:{
            id:slider_id,
        }
    });
    // console.log("coming file",data);
    // console.log("image name:",data.img_name);
    // console.log("image:",data.img);
  
   // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
      var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');
    
      return response;
    }

    if(req.body.data.img_tm || req.body.data.img_ru || req.body.data.img_en){
        let img_direction_tm = "";
        if(req.body.data.img_tm){
            var imageBuffer_tm = decodeBase64Image(req.body.data.img_tm);
                // console.log(imageBuffer);
            // converting buffer to original image to /upload folder
            let randomNumber_tm = Math.floor(Math.random() * 999999999999);
            console.log("random Number:",randomNumber_tm);
            img_direction_tm = `./uploads/`+randomNumber_tm+`${req.body.data.img_name_tm}`;
                fs.writeFile(img_direction_tm, imageBuffer_tm.data, function(err) { console.log(err) });
                ///////////////////////////////////////////////////////////////////////////////////////////
                fs.unlink(founndSlide.title_tm,(err)=>{console.log("updated")})
        }else{
             img_direction_tm = founndSlide.title_tm;
        }
        let img_direction_ru = ""
        if(req.body.data.img_ru){
                var imageBuffer_ru = decodeBase64Image(req.body.data.img_ru);
                    // console.log(imageBuffer);
                // converting buffer to original image to /upload folder
                let randomNumber_ru = Math.floor(Math.random() * 999999999999);
                console.log("random Number:",randomNumber_ru);
                img_direction_ru = `./uploads/`+randomNumber_ru+`${req.body.data.img_name_ru}`;
                    fs.writeFile(img_direction_ru, imageBuffer_ru.data, function(err) { console.log(err) });
                    ///////////////////////////////////////////////////////////////////////////////////////////
                    fs.unlink(founndSlide.title_ru,(err)=>{console.log("updated")})
        }else{
             img_direction_ru = founndSlide.title_ru
        }
        let img_direction_en = "";
        if(req.body.data.img_en){
            var imageBuffer_en = decodeBase64Image(req.body.data.img_en);
                // console.log(imageBuffer);
            // converting buffer to original image to /upload folder
            let randomNumber_en = Math.floor(Math.random() * 999999999999);
            console.log("random Number:",randomNumber_en);
             img_direction_en = `./uploads/`+randomNumber_en+`${req.body.data.img_name_en}`;
                fs.writeFile(img_direction_en, imageBuffer_en.data, function(err) { console.log(err) });
                ///////////////////////////////////////////////////////////////////////////////////////////
                fs.unlink(founndSlide.title_en,(err)=>{console.log("updated")})
        }else{
            img_direction_en = founndSlide.title_en
        }
    
   
    if(founndSlide){
            
       
    Sliders.update({
        title_tm:img_direction_tm, 
        title_ru:img_direction_ru, 
        title_en:img_direction_en,
        link:data.link,
        // photo_url:img_direction,
    },
    {
        where:{
            id:slider_id
        }
    }
    ).then(()=>{
        res.json({
            msg:"Successfully"
        });
    }).catch((err)=>{
    console.log(err);
    res.json({
        msg:"error"
    })
    })
}else{
    res.json({
        msg:"BU ID boyuncha Slider tapylmady!"
    })
}
    }else{
        const { slider_id } = req.params;
    const founndSlide = await Sliders.findOne({
        where:{
            id:slider_id,
        }
    });
   
    if(founndSlide){       
    Sliders.update({
        // title_tm:data.title_tm, 
        // title_ru:data.title_ru, 
        // title_en:data.title_en,
        link:data.link,
        // photo_url:img_direction,
    },
    {
        where:{
            id:slider_id
        }
    }
    ).then(()=>{
        res.json({
            msg:"Successfully"
        });
    }).catch((err)=>{
    console.log(err);
    res.json({
        msg:"error"
    })
    })
}else{
    res.json({
        msg:"BU ID boyuncha Slider tapylmady!"
    })
}
    }
}

const Delete = async(req,res)=>{
    const { slider_id } = req.params;
    const founndSlide = await Sliders.findOne({
        where:{
            id:slider_id,
        }
    });
    fs.unlink(founndSlide.photo_url,(err)=>{
        console.log("deleted");
    })
    if(founndSlide){
    Sliders.destroy({
        where:{
            id:slider_id,
        }
    }).then(()=>{
        res.json({
            msg:"successfully"
        })
    }).catch((err)=>{
        console.log(err);
        res.json({
            msg:"error",
        })
    });
}else{
    res.json({
        msg:"Bu ID boyuncha Slider tapylmady"
    })
}
}

exports.sliders_tb = sliders_tb;

exports.getAllSliders = getAllSliders;
exports.create = create;
exports.update = update;
exports.Delete = Delete;