var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
var MarketAddress = require("../models/marketAddress");
var Markets = require("../models/markets");
const { json } = require("sequelize");
const Op = Sequelize.Op;

const marketAddress_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = MarketAddress.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}



const update_address = async(req,res)=>{
    const { address_id } = req.params;
    const {  name_tm,name_ru,name_en, description_tm,description_ru,description_en} = req.body;
    
    const address = await MarketAddress.findOne({
        where:{
            id:address_id,
        }
    });
 
    if(address){
    MarketAddress.update({
     name_tm,
     name_ru,
     name_en,
     description_tm,
     description_ru,
     description_en,
    },
    {
        where:{
            id:address_id
        }
    }).then(()=>{
            res.status(200).json({
                msg:"Suссessfully"
            });
        }).catch((err)=>{
        console.log(err);
    });
 }else{
    res.json({
        msg:"Bu ID boyuncha Address tapylmady!"
    })
 }
 }
 
 const delete_address = async(req,res)=>{
    const { address_id } = req.params;
    const address = await MarketAddress.findOne({
        where:{
            id:address_id,
        }
    });
    if(address){
        MarketAddress.destroy({
            where:{
                id:address_id,
            }
        }).then(()=>{
            res.status(200).json({
                msg:"Suссessfully",
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"Bu ID boyuncha address tapylmady!"
        })
    }
   
 }
 
 
 
 const get_market_address = async(req,res)=>{
    MarketAddress.findAll({
        include:[{
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en"],
        }],
        where:{
            MarketId:{
                [Op.not]:null
            }
        }
    }).then((data)=>{
        res.json(data);
    })
 }

 const get_market_one_address = async(req,res)=>{
     const { market_address_id } = req.params;
    MarketAddress.findAll({
        include:[{
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en"],
        }],
        where:{
            id:market_address_id
        }
    }).then((data)=>{
        res.json(data);
    })
 }
 
 const create_market_address = async(req,res)=>{
    const { market_id } = req.params;
    const {  name_tm,name_ru,name_en, description_tm,description_ru,description_en } = req.body;
    const foundMarket = await Markets.findOne({
        where:{
            id:market_id,
        }
    });
 
 if(foundMarket){
    MarketAddress.create({
        name_tm,
        name_ru,
        name_en,
        description_tm,
        description_ru,
        description_en,
        MarketId:market_id,
    }).then((data)=>{
        res.json({
            msg:"successfully",
            data:data,
        })
    }).catch((err)=>{
        console.log(err);
    });
 }else{
    res.json({
        msg:"bU ID boyuncha Market tapylmady!",
    })
 }
 }
 


exports.marketAddress_tb = marketAddress_tb;

exports.create_market_address = create_market_address;
exports.get_market_address = get_market_address;
exports.get_market_one_address = get_market_one_address;
exports.update_address = update_address;
exports.delete_address = delete_address;