var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
// const base64 = require("base64");
const { json } = require("sequelize");
const Op = Sequelize.Op;
const fs = require("fs");
const ReseptSubKategoriya = require("../models/reseptSubKategory");

const receptSubKategory_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = ReseptSubKategoriya.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

exports.receptSubKategory_tb = receptSubKategory_tb;