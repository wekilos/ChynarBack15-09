var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Products = require("../models/products");
const Markets = require("../models/markets");
const MarketAddress = require("../models/marketAddress");
const Op = Sequelize.Op;
const fs = require("fs");
const Units = require("../models/units");
const MarketKategoriya = require("../models/marketKategoriya");
const MarketSubKategoriya = require("../models/marketSubKategoriya");
const Configs = require("../models/config");
const Welayatlar = require("../models/welayatlar");
const Brands = require("../models/brand");
const Razmerler = require("../models/Razmerler");
const Renkler = require("../models/Renkler");
const { GetData } = require("../functions/readFromExel");

const products_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Products.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const all_harytlar = async(req,res)=>{
    let { code,all, market_id, kategoriya_id,is_sale,welayatId,brandId,baha, startBaha,endBaha,is_new,page,limit } = req.query;
    let Page = page ?
    page : 0;
    let Limit = limit ? limit : 15;
    let Offset = Page * Limit;

    let all1 = parseInt(all);
    var codeInt = parseInt(code)
    var Code = code ? 
    {
        product_code: { [Op.eq]:code}
    }
    : null;
    var All = all
    ? {
        [Op.or]: [
        //   { id: { [Op.eq]: `${all}` } },
          { name_tm: { [Op.like]: `%${all}%` } },
          { name_ru: { [Op.like]: `%${all}%` } },
          { name_en: { [Op.like]: `%${all}%` } },
          { description_tm: { [Op.like]: `%${all}%` } },
          { description_ru: { [Op.like]: `%${all}%` } },
          { description_en: { [Op.like]: `%${all}%` } },
        ],
      }
    : null;

    var BrandID = brandId
    ? {
        BrandId :{[Op.eq]:brandId}
    }
    :null;

    var Baha = baha==2
    ? 
        ['price', 'DESC']
    : (baha==1 ?
        ['price', 'ASC']
    : (baha ==3?
        ['name_tm','ASC']
    : (baha == 4?
        ['name_tm','DESC']
    :
        ['id', 'DESC'])));

    var StartBaha = startBaha
    ?
        {
            price :{[Op.gte]:startBaha}
        }
    :
    null;

    var EndBaha = endBaha 
    ?
        {
            price : {[Op.lte]:endBaha}
        }
    :
    null;

    var WelayatlarId = welayatId
    ? {
        WelayatlarId: { [Op.eq]: `${welayatId}` } 
      }
    : null;

    var MarketId = market_id
    ? {
         MarketId: { [Op.eq]: `${market_id}` } 
      }
    : null;
    var KategoriyaId = kategoriya_id
    ? { 
        MarketKategoriyaId: { [Op.eq]: `${kategoriya_id}` } 
      }
    : null;

    var Is_sale = is_sale
    ? {
        is_sale:{[Op.eq]:is_sale}
    }
    : null;

    var Is_new = is_new
    ? {
        is_new:{[Op.eq]:is_new}
    }
    : null;

    Products.findAll({
        include:[
            {
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            include:[{
                model:MarketAddress,
                attributes:["id","name_tm","name_ru","name_en","description_tm","description_ru","description_en","MarketId"]
            }],
            },
            {
                model:MarketKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:MarketSubKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:Units,
                attributes:["id","name_tm","name_ru","name_en"]
            },
            {
                model:Configs,
                attributes:["id","currency_exchange"]
            },
            {
                model:Welayatlar,
                attributes:["id","name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
            },
            {
                model:Brands,
                attributes:["id","name_tm","name_ru","name_en","surat"]
            },
            {
                model:Razmerler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
            ,{
                model:Renkler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
        
    ],
    where:{
        [Op.and]:[All,MarketId,KategoriyaId,Is_sale,WelayatlarId,BrandID,EndBaha,StartBaha,Is_new,Code,
            {is_active:true},{deleted:false}
        ]
    },
        order: [
            Baha
        ],

        offset:Offset,
        limit : Limit,

    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const allDisActive_harytlar = async(req,res)=>{
    let { code,all, market_id, kategoriya_id,is_sale,welayatId,brandId,page,limit } = req.query;
    
    let Page = page ?
    page : 0;
    let Limit = limit ? limit : 15;
    let Offset = Page * Limit;

    let all1 = parseInt(all);
    var codeInt = parseInt(code)
    var Code = code ? 
    {
        product_code: { [Op.eq]:code}
    }
    : null;
    var All = all
    ? {
        [Op.or]: [
        //   { id: { [Op.eq]: `${all}` } },
          { name_tm: { [Op.like]: `%${all}%` } },
          { name_ru: { [Op.like]: `%${all}%` } },
          { name_en: { [Op.like]: `%${all}%` } },
          { description_tm: { [Op.like]: `%${all}%` } },
          { description_ru: { [Op.like]: `%${all}%` } },
          { description_en: { [Op.like]: `%${all}%` } },
        ],
      }
    : null;
    var BrandId = brandId
    ? {
        BrandId :{[Op.eq]:`${brandId}`}
    }
    :null;
    var WelayatlarId = welayatId
    ? {
        WelayatlarId: { [Op.eq]: `${welayatId}` } 
      }
    : null;
    var MarketId = market_id
    ? {
         MarketId: { [Op.eq]: `${market_id}` } 
      }
    : null;
    var KategoriyaId = kategoriya_id
    ? { 
        MarketKategoriyaId: { [Op.eq]: `${kategoriya_id}` } 
      }
    : null;
    var Is_sale = is_sale
    ? {
        is_sale:{[Op.eq]:is_sale}
    }
    : null;
    Products.findAll({
        include:[{
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            include:[{
                model:MarketAddress,
                attributes:["id","name_tm","name_ru","name_en","description_tm","description_ru","description_en","MarketId"]
            }],
        },
        {
            model:MarketKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"]
        },
        {
            model:MarketSubKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"]
        },
        {
            model:Units,
            attributes:["id","name_tm","name_ru","name_en"]
        },
        {
            model:Configs,
            attributes:["id","currency_exchange"]
        },
        {
            model:Welayatlar,
            attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
        },
        {
            model:Brands,
            attributes:["id","name_tm","name_ru","name_en","surat"]
        },
        {
            model:Razmerler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
        ,{
            model:Renkler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
    
    ],
    where:{
        [Op.and]:[All,MarketId,KategoriyaId,Is_sale,WelayatlarId,BrandId,Code,
            {is_active:false},{deleted:false}
        ]
    },
        order: [
            ['id', 'DESC'],
        ],
        offset:Offset,
        limit : Limit,
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const SearchHarytlar = async(req,res)=>{
    let { code,all, market_id, kategoriya_id,is_sale,welayatId,brandId,is_new,endBaha,startBaha,baha } = req.query;
    let all1 = parseInt(all);
    var Code = code ? 
    {
        product_code: { [Op.eq]:code}
    }
    : null;
    var All = all
    ? {
        [Op.or]: [
          { name_tm: { [Op.like]: `%${all}%` } },
          { name_ru: { [Op.like]: `%${all}%` } },
          { name_en: { [Op.like]: `%${all}%` } },
          { search: { [Op.like]: `%${all}%` } },
        ],
      }
    : null;
    var BrandId = brandId
    ? {
        BrandId :{[Op.eq]:`${brandId}`}
    }
    :null;
    var WelayatlarId = welayatId
    ? {
        WelayatlarId: { [Op.eq]: `${welayatId}` } 
      }
    : null;
    var MarketId = market_id
    ? {
         MarketId: { [Op.eq]: `${market_id}` } 
      }
    : null;
    var KategoriyaId = kategoriya_id
    ? { 
        MarketKategoriyaId: { [Op.eq]: `${kategoriya_id}` } 
      }
    : null;
    var Is_sale = is_sale
    ? {
        is_sale:{[Op.eq]:is_sale}
    }
    : null;

    var Baha = baha==2
    ? 
        ['price', 'DESC']
    : (baha==1 ?
        ['price', 'ASC']
    : (baha ==3?
        ['name_tm','ASC']
    : (baha == 4?
        ['name_tm','DESC']
    :
        ['id', 'DESC'])));

    var StartBaha = startBaha
    ?
        {
            price :{[Op.gte]:startBaha}
        }
    :
    null;

    var EndBaha = endBaha 
    ?
        {
            price : {[Op.lte]:endBaha}
        }
    :
    null;

    var Is_new = is_new
    ? {
        is_new:{[Op.eq]:is_new}
    }
    : null;

    Products.findAll({
        include:[{
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            
        },
        {
            model:MarketKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"]
        },
        {
            model:MarketSubKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"]
        },
        {
            model:Units,
            attributes:["id","name_tm","name_ru","name_en"]
        },
        {
            model:Configs,
            attributes:["id","currency_exchange"]
        },
        {
            model:Welayatlar,
            attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
        },
        {
            model:Brands,
            attributes:["id","name_tm","name_ru","name_en","surat"]
        },
        {
            model:Razmerler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
        ,{
            model:Renkler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
    
    ],
    where:{
        [Op.and]:[All,MarketId,KategoriyaId,Is_sale,WelayatlarId,BrandId,Code,Is_new,StartBaha,EndBaha,
            {is_active:true},{deleted:false}
        ]
    },
    order: [
        Baha
    ]
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
        res.json({"err":err})
    })
}
const getOneMarketPro = async(req,res)=>{
    const { market_id} = req.params;
    const {baha, startBaha,endBaha,is_new ,is_sale,page,limit} = req.query;
    
    let Page = page ?
    page : 0;
    let Limit = limit ? limit : 15;
    let Offset = Page * Limit;

    var Baha = baha==2
    ? 
        ['price', 'DESC']
    : (baha==1 ?
        ['price', 'ASC']
    : (baha ==3?
        ['name_tm','ASC']
    : (baha == 4?
        ['name_tm','DESC']
    :
        ['id', 'DESC'])));

    var StartBaha = startBaha
    ?
        {
            price :{[Op.gte]:startBaha}
        }
    :
    null;

    var EndBaha = endBaha 
    ?
        {
            price : {[Op.lte]:endBaha}
        }
    :
    null;

    var Is_sale = is_sale
    ? {
        is_sale:{[Op.eq]:is_sale}
    }
    : null;

    var Is_new = is_new
    ? {
        is_new:{[Op.eq]:is_new}
    }
    : null;

    Products.findAll({
        include:[
            {
                model:MarketKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:MarketSubKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:Markets,
                attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
                },
            {
                model:Units,
                attributes:["id","name_tm","name_ru","name_en"]
            },
            {
                model:Configs,
                attributes:["id","currency_exchange"]
            },
            {
                model:Welayatlar,
                attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
            },
            {
                model:Brands,
                attributes:["id","name_tm","name_ru","name_en","surat"]
            },
            {
                model:Razmerler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
            ,{
                model:Renkler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
        ],
        where:{
            [Op.and]:[
                {MarketId:market_id},Is_sale,Is_new,StartBaha,EndBaha,
                {is_active:true},{deleted:false}]
            
        },
        order: [
             Baha
        ],

        offset:Offset,
        limit : Limit,
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const getOneMarketKategoriyaPro = async(req,res)=>{
    const { marketKategoriya_id } = req.params;
    const {baha, startBaha,endBaha,is_new ,is_sale, limit,page} = req.query;

    let Page = page ?
    page : 0;
    let Limit = limit ? limit : 15;
    let Offset = Page * Limit;
    
    var Baha = baha==2
    ? 
        ['price', 'DESC']
    : (baha==1 ?
        ['price', 'ASC']
    : (baha ==3?
        ['name_tm','ASC']
    : (baha == 4?
        ['name_tm','DESC']
    :
        ['id', 'DESC'])));

    var StartBaha = startBaha
    ?
        {
            price :{[Op.gte]:startBaha}
        }
    :
    null;

    var EndBaha = endBaha 
    ?
        {
            price : {[Op.lte]:endBaha}
        }
    :
    null;

    var Is_sale = is_sale
    ? {
        is_sale:{[Op.eq]:is_sale}
    }
    : null;

    var Is_new = is_new
    ? {
        is_new:{[Op.eq]:is_new}
    }
    : null;

    Products.findAll({
        include:[{
            model:Units,
            attributes:["id","name_tm","name_ru","name_en"]
        },
        {
            model:MarketKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"],
            include:[{
                model:Markets,
                attributes:["id","name_tm","name_ru","name_en","surat"]
            }]
        },
        {
            model:MarketSubKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"]
        },
        {
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            },
        {
            model:Configs,
            attributes:["id","currency_exchange"]
        },
        {
            model:Welayatlar,
            attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
        },
        {
            model:Brands,
            attributes:["id","name_tm","name_ru","name_en","surat"]
        },
        {
            model:Razmerler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
        ,{
            model:Renkler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
    ],
        where:{
            [Op.and]:[
                {MarketKategoriyaId:marketKategoriya_id},
                    Is_sale,Is_new,StartBaha,EndBaha,
                
                {is_active:true},{deleted:false}]
            
        },
        order: [
             Baha
        ],
        offset:Offset,
        limit : Limit,
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const getOneMarketSubKategoriyaPro = async(req,res)=>{
    const { marketSubKategoriya_id } = req.params;
    const {baha, startBaha,endBaha,is_new ,is_sale,page,limit} = req.query;

    let Page = page ?
    page : 0;
    let Limit = limit ? limit : 15;
    let Offset = Page * Limit;

    var Baha = baha==2
    ? 
        ['price', 'DESC']
    : (baha==1 ?
        ['price', 'ASC']
    : (baha ==3?
        ['name_tm','ASC']
    : (baha == 4?
        ['name_tm','DESC']
    :
        ['id', 'DESC'])));

    var StartBaha = startBaha
    ?
        {
            price :{[Op.gte]:startBaha}
        }
    :
    null;

    var EndBaha = endBaha 
    ?
        {
            price : {[Op.lte]:endBaha}
        }
    :
    null;

    var Is_sale = is_sale
    ? {
        is_sale:{[Op.eq]:is_sale}
    }
    : null;

    var Is_new = is_new
    ? {
        is_new:{[Op.eq]:is_new}
    }
    : null;

    Products.findAll({
        include:[{
            model:Units,
            attributes:["id","name_tm","name_ru","name_en"]
        },
        {
            model:MarketKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"],
            include:[{
                model:Markets,
                attributes:["id","name_tm","name_ru","name_en","surat"]
            }]
        },
        {
            model:MarketSubKategoriya,
            attributes:["id","name_tm","name_ru","name_en","active"]
        },
        {
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            },
        {
            model:Configs,
            attributes:["id","currency_exchange"]
        },
        {
            model:Welayatlar,
            attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
        },
        {
            model:Brands,
            attributes:["id","name_tm","name_ru","name_en","surat"]
        },
        {
            model:Razmerler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
        ,{
            model:Renkler,
            attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
            // where:{deleted:false,active:true}
        }
    ],
        where:{
            [Op.and]:[
                {MarketSubKategoriyaId:marketSubKategoriya_id},
                    Is_sale,Is_new,StartBaha,EndBaha,
                
                {is_active:true},{deleted:false}]
            
        },
        order: [
             Baha
        ],
        
        offset:Offset,
        limit : Limit,
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const one_haryt = async(req,res)=>{
    const { product_id } = req.params;
    Products.findOne({
        include:[{
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            },
            {
                model:MarketKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:MarketSubKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:Units,
                attributes:["id","name_tm","name_ru","name_en"]
            },
            {
                model:Configs,
                attributes:["id","currency_exchange"]
            },
            {
                model:Welayatlar,
                attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
            },
            {
                model:Brands,
                attributes:["id","name_tm","name_ru","name_en","surat"]
            }
            ,{
                model:Razmerler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
            ,{
                model:Renkler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
        ],
        where:{
            id:product_id
        }

    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const sameProducts = async(req,res)=>{
    const { product_id } = req.params;
    var FoundProduct = await Products.findOne({
        include:[{
            model:Markets,
            attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            },
            {
                model:MarketKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:MarketSubKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            }
        ],
        where:{
            id:product_id
        }

    });

    if(FoundProduct?.MarketSubKategoriyaId!=null){
        Products.findAll({
            include:[{
                model:Markets,
                attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            },
            {
                model:MarketKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:MarketSubKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:Units,
                attributes:["id","name_tm","name_ru","name_en"]
            },
            {
                model:Configs,
                attributes:["id","currency_exchange"]
            },
            {
                model:Welayatlar,
                attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
            },
            {
                model:Brands,
                attributes:["id","name_tm","name_ru","name_en","surat"]
            }
            ,{
                model:Razmerler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
            ,{
                model:Renkler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }],
            where:{
                MarketSubKategoriyaId:FoundProduct?.MarketSubKategoriyaId
            },
            limit:16,
            
        }).then((data)=>{
            res.json(data)
        }).catch((err)=>{
            res.json(err)
        })
    }else if(FoundProduct?.MarketKategoriyaId!=null){
        Products.findAll({
            include:[{
                model:Markets,
                attributes:["id","name_tm","name_ru","name_en","surat","currency_exchange"],
            },
            {
                model:MarketKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:MarketSubKategoriya,
                attributes:["id","name_tm","name_ru","name_en","active"]
            },
            {
                model:Units,
                attributes:["id","name_tm","name_ru","name_en"]
            },
            {
                model:Configs,
                attributes:["id","currency_exchange"]
            },
            {
                model:Welayatlar,
                attributes:["name_tm","name_ru","name_en","short_name_tm","short_name_ru","short_name_en"]
            },
            {
                model:Brands,
                attributes:["id","name_tm","name_ru","name_en","surat"]
            }
            ,{
                model:Razmerler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }
            ,{
                model:Renkler,
                attributes:['id',"name_tm","name_ru","name_en","active","deleted"],
                // where:{deleted:false,active:true}
            }],
            where:{
                MarketKategoriyaId:FoundProduct?.MarketKategoriyaId
            },
            limit:16
        }).then((data)=>{
            res.json(data)
        }).catch((err)=>{
            res.json(err)
        })
    }else{
        res.json([])
    }
}

const create_haryt = async(req,res)=>{

    let data = req.body.data;
    console.log("coming file",data);
    // console.log("image name:",data.img_name);
    // console.log("image:",data.img);
  
   // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
      var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');
    
      return response;
    }
    function decodeBase64Image3(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
          response = {};
      
        if (matches.length !== 3) {
          return new Error('Invalid input string');
        }
      
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
      
        return response;
      }
      function decodeBase64Image1(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
          response = {};
      
        if (matches.length !== 3) {
          return new Error('Invalid input string');
        }
      
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
      
        return response;
      }
      function decodeBase64Image2(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
          response = {};
      
        if (matches.length !== 3) {
          return new Error('Invalid input string');
        }
      
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
      
        return response;
      }

    let img_direction="";
    let img_direction1 = "";
    let img_direction2 = "";
    let img_direction3 = "";
    if(req.body.data.img){
        var imageBuffer = await decodeBase64Image(req.body.data.img);
        // console.log(imageBuffer);
    // converting buffer to original image to /upload folder
    let randomNumber = Math.floor(Math.random() * 999999999999);
    console.log("random Number:",randomNumber);
    img_direction = `./uploads/`+randomNumber+`${req.body.data.img_name}`;
    fs.writeFile(img_direction, imageBuffer.data, function(err) { console.log(err) });
    ///////////////////////////////////////////////////////////////////////////////////////////
    }
    
    if(req.body.data.img1){
        var imageBuffer1 = await decodeBase64Image1(req.body.data.img1);
    // console.log(imageBuffer);
    // converting buffer to original image to /upload folder
    let randomNumber1 = Math.floor(Math.random() * 999999999999);
    console.log("random Number:",randomNumber1);
    img_direction1 = `./uploads/`+randomNumber1+`${req.body.data.img_name1}`;
    fs.writeFile(img_direction1, imageBuffer1.data, function(err) { console.log(err) });
    console.log(img_direction1)
    ///////////////////////////////////////////////////////////////////////////////////////////
    }
    if(req.body.data.img2){
        var imageBuffer2 = await decodeBase64Image2(req.body.data.img2);
    // console.log(imageBuffer);
    // converting buffer to original image to /upload folder
    let randomNumber2 = Math.floor(Math.random() * 999999999999);
    console.log("random Number:",randomNumber2);
    img_direction2 = `./uploads/`+randomNumber2+`${req.body.data.img_name2}`;
    fs.writeFile(img_direction2, imageBuffer2.data, function(err) { console.log(err) });
    console.log(img_direction2)
    ///////////////////////////////////////////////////////////////////////////////////////////
    }
    if(req.body.data.img3){
        var imageBuffer3 = await decodeBase64Image3(req.body.data.img3);
    // console.log(imageBuffer);
    // converting buffer to original image to /upload folder
    let randomNumber3 = Math.floor(Math.random() * 999999999999);
    console.log("random Number:",randomNumber3);
    img_direction3 = `./uploads/`+randomNumber3+`${req.body.data.img_name3}`;
    fs.writeFile(img_direction3, imageBuffer3.data, function(err) { console.log(err) });
    console.log(img_direction3)
    ///////////////////////////////////////////////////////////////////////////////////////////
    }

    const { market_id } = req.params;
    // const { name_tm,name_ru,name_en, price, sale_price, step, article_tm, article_ru, article_en, description_tm, description_ru, description_en, is_sale, sale_until, is_active, total_amount, view_count, is_valyuta_price, search, MarketKategoriyaId, UnitId } = req.body;
    
    const foundMarket = await Markets.findOne({
        where:{
            id:market_id
        }
    });
    
    if(foundMarket){    
        
        Products.create({
            name_tm:data.name_tm,
            name_ru:data.name_ru,
            name_en:data.name_en,
            gelenBaha:data.gelenBaha,
            // price_usd:data.price_usd,
            price:data.price,
            product_code:data.product_code,
            bahaPrasent:data.bahaPrasent,
            // sale_price:data.sale_price,
            step:data.step,
            article_tm:data.article_tm,
            article_ru:data.article_ru,
            article_en:data.article_en,
            description_tm:data.description_tm,
            description_ru:data.description_ru,
            description_en:data.description_en,
            is_sale:data.is_sale, 
            // sale_until:data.sale_until, 
            is_active:data.is_active, 
            total_amount:data.total_amount, 
            view_count:1, 
            is_valyuta_price:data.is_valyuta_price,
            search:data.search,
            MarketId:market_id,
            MarketKategoriyaId:data.MarketKategoriyaId,
            MarketSubKategoriyaId:data.MarketSubKategoriyaId,
            UnitId:data.UnitId,
            ConfigId:data.ConfigId,
            BrandId:data.BrandId,
            WelayatlarId:data.WelayatlarId,
            is_new:data.is_new,
            surat:img_direction,
            surat1:img_direction1,
            surat2:img_direction2,
            surat3:img_direction3,
            deleted:false,
        }).then((data)=>{
            res.json({
                msg:"successfully",data
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"Bu ID boyuncha Market tapylmady!"
        })
    }
}

const update_haryt = async(req,res)=>{
    const { product_id } = req.params;
    const foundHaryt = await Products.findOne({
        where:{
            id:product_id
        }
    });
    
    let data = req.body.data;
    // console.log("coming file",data);
    // console.log("image name:",data.img_name);
    // console.log("image:",data.img);
  
   // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
      var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
        response = {};
    
      if (matches.length !== 3) {
        return new Error('Invalid input string');
      }
    
      response.type = matches[1];
      response.data = new Buffer(matches[2], 'base64');
    
      return response;
    }
    if(req.body.data.img || req.body.data.img1 || req.body.data.img2 || req.body.data.img3){
        let img_direction;
        let img_direction1;
        let img_direction2;
        let img_direction3;

        if(req.body.data.img){
            var imageBuffer = decodeBase64Image(req.body.data.img);
            // console.log(imageBuffer);
            // converting buffer to original image to /upload folder
            let randomNumber = Math.floor(Math.random() * 999999999999);
            console.log("random Number:",randomNumber);
            img_direction = `./uploads/`+randomNumber+`${req.body.data.img_name}`;
            fs.writeFile(img_direction, imageBuffer.data, function(err) { console.log(err) });
            ///////////////////////////////////////////////////////////////////////////////////////////

        }else{
            img_direction = foundHaryt.surat;
        }
        if(req.body.data.img1){
            var imageBuffer1 = decodeBase64Image(req.body.data.img1);
            // console.log(imageBuffer);
            // converting buffer to original image to /upload folder
            let randomNumber1 = Math.floor(Math.random() * 999999999999);
            console.log("random Number:",randomNumber1);
            img_direction1 = `./uploads/`+randomNumber1+`${req.body.data.img_name1}`;
            fs.writeFile(img_direction1, imageBuffer1.data, function(err) { console.log(err) });
            ///////////////////////////////////////////////////////////////////////////////////////////

        }else{
            img_direction1 = foundHaryt.surat1;
        }
        if(req.body.data.img2){
            var imageBuffer2 = decodeBase64Image(req.body.data.img2);
            // console.log(imageBuffer);
            // converting buffer to original image to /upload folder
            let randomNumber2 = Math.floor(Math.random() * 999999999999);
            console.log("random Number:",randomNumber2);
            img_direction2 = `./uploads/`+randomNumber2+`${req.body.data.img_name2}`;
            fs.writeFile(img_direction2, imageBuffer2.data, function(err) { console.log(err) });
            ///////////////////////////////////////////////////////////////////////////////////////////

        }else{
            img_direction2 = foundHaryt.surat2;
        }
        if(req.body.data.img3){
            var imageBuffer3 = decodeBase64Image(req.body.data.img3);
            // console.log(imageBuffer);
            // converting buffer to original image to /upload folder
            let randomNumber3 = Math.floor(Math.random() * 999999999999);
            console.log("random Number:",randomNumber3);
            img_direction3 = `./uploads/`+randomNumber3+`${req.body.data.img_name3}`;
            fs.writeFile(img_direction3, imageBuffer3.data, function(err) { console.log(err) });
            ///////////////////////////////////////////////////////////////////////////////////////////

        }else{
            img_direction3 = foundHaryt.surat3;
        }

    
    if(foundHaryt){
           await fs.unlink(foundHaryt.surat,(err)=>{console.log("updated")})
            console.log(foundHaryt)
        
        Products.update({
            name_tm:data.name_tm,
            name_ru:data.name_ru,
            name_en:data.name_en,
            gelenBaha:data.gelenBaha,
            // price_usd:data.price_usd,
            price:data.price,
            product_code:data.product_code,
            bahaPrasent:data.bahaPrasent,
            sale_price:data.sale_price,
            step:data.step,
            article_tm:data.article_tm,
            article_ru:data.article_ru,
            article_en:data.article_en,
            description_tm:data.description_tm,
            description_ru:data.description_ru,
            description_en:data.description_en,
            is_sale:data.is_sale, 
            sale_until:data.sale_until, 
            is_active:data.is_active, 
            total_amount:data.total_amount, 
            view_count:data.view_count, 
            is_valyuta_price:data.is_valyuta_price,
            search:data.search,
            is_new:data.is_new,
            surat:img_direction,
            surat1:img_direction1,
            surat2:img_direction2,
            surat3:img_direction3,
            MarketId:data.MarketId,
            WelayatlarId:data.WelayatlarId,
            MarketKategoriyaId:data.MarketKategoriyaId,
            MarketSubKategoriyaId:data.MarketSubKategoriyaId,
            UnitId:data.UnitId,
            BrandId:data.BrandId,
            deleted:data.deleted,
        },
        {
            where:{
                id:product_id,
            }
        }).then(()=>{
            res.json({
                msg:"successfully",
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"Bu ID boyuncha Haryt tapylmady!"
        })
    }
}else{
    const { product_id } = req.params;
    const { name_tm,name_ru,name_en, price, sale_price, step, article_tm, article_ru, article_en, description_tm, description_ru, description_en, is_sale, sale_until, is_active, total_amount, view_count, is_valyuta_price, search, MarketKategoriyaid, UnitId } = req.body;
    const foundHaryt = await Products.findOne({
        where:{
            id:product_id
        }
    });
    if(foundHaryt){
        
        Products.update({
            name_tm:data.name_tm,
            name_ru:data.name_ru,
            name_en:data.name_en,
            gelenBaha:data.gelenBaha,
            // price_usd:data.price_usd,
            price:data.price,
            product_code:data.product_code,
            bahaPrasent:data.bahaPrasent,
            sale_price:data.sale_price,
            step:data.step,
            article_tm:data.article_tm,
            article_ru:data.article_ru,
            article_en:data.article_en,
            description_tm:data.description_tm,
            description_ru:data.description_ru,
            description_en:data.description_en,
            is_sale:data.is_sale, 
            sale_until:data.sale_until, 
            is_active:data.is_active, 
            total_amount:data.total_amount, 
            view_count:data.view_count,
            is_valyuta_price:data.is_valyuta_price,
            search:data.search,
            is_new:data.is_new,
            MarketId:data.MarketId,
            WelayatlarId:data.WelayatlarId,
            MarketKategoriyaId:data.MarketKategoriyaId,
            MarketSubKategoriyaId:data.MarketSubKategoriyaId,
            UnitId:data.UnitId,
            BrandId:data.BrandId,
            deleted:data.deleted,
        },
        {
            where:{
                id:product_id,
            }
        }).then(()=>{
            res.json({
                msg:"successfully",
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"Bu ID boyuncha Haryt tapylmady!"
        })
    }
}
}

 const Skidka = async(req,res)=>{
     const { product_id } = req.params;
     const { is_sale, sale_until, sale_price } = req.body;
     Products.update({
        is_sale,
        sale_price,
        sale_until
     },
     {
         where:{
             id:product_id
         }
     }).then((data)=>{
         res.json({
             msg:"successfully",
             data:data,
         })
     }).catch((err)=>{
         console.log(err);
     })
 }

 const IsActive = async(req,res)=>{
     const { product_id } = req.params;
     const { is_active } = req.body;
     Products.update({
        is_active
     },
     {
         where:{
             id:product_id
         }
     }).then((data)=>{
        res.json({
            msg:"successfully",
            data:data,
        })
    }).catch((err)=>{
        console.log(err);
    })
 }
const delete_haryt = async(req,res)=>{
    const { product_id } = req.params;
    const foundHaryt = await Products.findOne({
        where:{
            id:product_id
        }
    });
    if(foundHaryt){
        Products.update({
            is_active:false,
            deleted:true,
        },{
            where:{
                id:product_id,
            }
        }).then(()=>{
            res.json({
                msg:"successfully",
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"BU ID boyuncha Haryt tapylmady!"
        })
    }
}

const destroy_haryt = async(req,res)=>{
    const { product_id } = req.params;
    const foundHaryt = await Products.findOne({
        where:{
            id:product_id
        }
    });
    if(foundHaryt){
        Products.destroy({
            where:{
                id:product_id,
            }
        }).then(()=>{
            res.json({
                msg:"successfully",
            })
        }).catch((err)=>{
            console.log(err);
        });
    }else{
        res.json({
            msg:"BU ID boyuncha Haryt tapylmady!"
        })
    }
}

const UpdateNewAndSale = async(req,res)=>{
    const threeDaysAgo = new Date(new Date().setDate(new Date().getDate() - 3));
    Products.update(
        {
        is_new:false,
        },
        { where:{
            createdAt: {
                [Op.lt]: new Date(new Date() - 24 * 60 * 60 * 1000),
                // [Op.gt]: new Date(new Date() - 4*24 * 60 * 60 * 1000)
              }
                }
        }).then((data)=>{
            res.json("ok");
        }).catch(err=>console.log(err));

}

const UpdateSaleUntil = async (req,res)=>{
    Products.update(
        {
        is_sale:false,
        sale_price:null
        },
        { where:{
            sale_until: {
                [Op.lt]: new Date()
              }
                }
        }).then((data)=>{
            res.json("ok");
        }).catch(err=>console.log(err));
}

const Watched = async(req,res)=>{
    const { id } = req.params;
    const FoundPro = await Products.findOne({where:{id:id}});
    if(FoundPro){
        Products.update({
            view_count:FoundPro.view_count+1
        },{
            where:{
                id:id
            }
        }).then((data)=>{
            res.json("updated")
        }).catch((err)=>{
            console.log(err);
        })
    }else{
        res.json("Bu id boyuncha Produckt tapylmady!")
    }
    
}


const getDataFromExel = async(req,res)=>{
     GetData();
}

exports.getDataFromExel = getDataFromExel;

exports.products_tb=products_tb;
exports.sameProducts = sameProducts;
exports.all_harytlar = all_harytlar;
exports.allDisActive_harytlar = allDisActive_harytlar;
exports.getOneMarketPro = getOneMarketPro;
exports.getOneMarketKategoriyaPro = getOneMarketKategoriyaPro;
exports.getOneMarketSubKategoriyaPro = getOneMarketSubKategoriyaPro;
exports.one_haryt = one_haryt;
exports.create_haryt = create_haryt;
exports.update_haryt = update_haryt;
exports.Skidka = Skidka;
exports.IsActive = IsActive;
exports.delete_haryt = delete_haryt;
exports.destroy_haryt = destroy_haryt;

exports.SearchHarytlar = SearchHarytlar;
exports.UpdateSaleUntil = UpdateSaleUntil;
exports.UpdateNewAndSale = UpdateNewAndSale;
exports.Watched = Watched;