var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const BrandsKategory = require("../models/brandKategory");
const Brands = require("../models/brand");
const Op = Sequelize.Op;

const brandsKategory_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = BrandsKategory.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}

const getAll = async(req,res)=>{
    const {WelayatlarId,active,deleted} = req.query;
    const { welayatId } = req.params;
    var Welayat = welayatId
    ? {
        WelayatlarId :{[Op.eq]:`${welayatId}`}
    }
    : null;

    var Active = active ?
    {
      active:{[Op.eq]:active},
    }
    :{
      active:{[Op.eq]:true},
    };

    var Deleted = deleted 
    ? {
        deleted :{ [Op.eq]:deleted}
    }
    : {
        deleted :{ [Op.eq]:false}
    };
   
    BrandsKategory.findAll({
        include:[{
            model:Brands,
            attributes:["id","name_tm","name_ru","name_en","surat","active"],
            // where:{deleted:false,active:true}
        }],
        where:{
            [Op.and]:[Welayat,Active,Deleted]
        },
        order: [
            ['id', 'ASC'],
        ]
    }).then((data)=>{
        res.status(200).json(data);
    }).catch((err)=>{console.log(err);})
}

const create = async(req,res)=>{
    const {name_tm,name_ru,name_en,WelayatlarId,active} =  req.body;
    BrandsKategory.create({
        name_tm,
        name_ru,
        name_en,
        active:true,
        deleted:false,
        WelayatlarId
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{console.log(err);})
}

const update = async(req,res)=>{
    const {id} = req.params;
    const {name_tm,name_ru,name_en,WelayatlarId,active} =  req.body;
    BrandsKategory.update({
        name_tm,
        name_ru,
        name_en,
        active,
        WelayatlarId,
    },{
        where:{id:id}
    }).then(()=>{
        res.json("updated");
    }).catch((err)=>{console.log(err);})
}

const Delete = async(req,res)=>{
    const {id} = req.params;
    BrandsKategory.update({
        deleted:true,
        active:false,
    },{
        where:{id:id}
    }).then(()=>{
        res.json("deleted");
    }).catch((err)=>{console.log(err);})
}

const Destroy = async(req,res)=>{
    const {id} = req.params;
    BrandsKategory.destroy({
        where:{id:id}
    }).then(()=>{
        res.json("deleted");
    }).catch((err)=>{console.log(err);})
}

exports.brandsKategory_tb = brandsKategory_tb;

exports.getAll = getAll;
exports.create = create;
exports.update = update;
exports.Delete = Delete;
exports.Destroy = Destroy;