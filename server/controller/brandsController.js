var Sequelize = require("sequelize");
var sequelize = require("../../config/db");
const { json } = require("sequelize");
const Brands = require("../models/brand");
const Op = Sequelize.Op;
const fs = require("fs")

const brands_tb = async(req,res)=>{
    const response = await sequelize
    .sync()
    .then(()=>{
        const data = Brands.findAll();
        return data;
    }).catch((err)=>{
        return err;
    });
    res.json(response);
}


const getAll = async(req,res)=>{
    const {KategoryId,welayatId,SubKategoryId,active,deleted} = req.query;

    const WelayatlarId = welayatId
    ?{
        WelayatlarId:{[Op.eq]:welayatId}
    }
    :null;

    const Kategory = KategoryId
    ?{
        BrandsKategoryId:{[Op.eq]:KategoryId}
    }
    :null;
    var SubKategory = SubKategoryId 
    ? {
        BrandsSubKategoryId:{[Op.eq]:SubKategoryId}
    } 
    : null;
   
    var Active = active ?
    {
      active:{[Op.eq]:active},
    }
    :{
      active:{[Op.eq]:true},
    };

  var Deleted = deleted 
  ? {
    deleted :{ [Op.eq]:deleted}
  }
  : {
    deleted :{ [Op.eq]:false}
  };

    Brands.findAll({
        where:{
            [Op.and]:[Kategory,WelayatlarId,SubKategory,Active,Deleted]
        },
        order:[
            ["id","ASC"]
        ]
    }).then((data)=>{
        res.status(200).json(data);
    }).catch((err)=>{
        console.log(err);
    })
}

const create = async(req,res)=>{
    const data = req.body.data;
    // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
          response = {};
      
        if (matches.length !== 3) {
          return new Error('Invalid input string');
        }
      
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
      
        return response;
      }
      var imageBuffer = decodeBase64Image(req.body.data.img);
    // converting buffer to original image to /upload folder
    let randomNumber = Math.floor(Math.random() * 999999999999);
    console.log("random Number:",randomNumber);
    let img_direction = `./uploads/`+randomNumber+`${req.body.data.img_name}`;
      fs.writeFile(img_direction, imageBuffer.data, function(err) { console.log(err) });
      ///////////////////////////////////////////////////////////////////////////////////////////
      // console.log("data::::::::",data);
    Brands.create({
        name_tm:data.name_tm,
        name_ru:data.name_ru,
        name_en:data.name_en,
        surat:img_direction,
        active:true,
        deleted:false,
        BrandsKategoryId:data.BrandsKategoryId,
        BrandsSubKategoryId:data.BrandsSubKategoryId,
        WelayatlarId:data.welayatId,
    }).then((data)=>{
        res.json(data);
    }).catch((err)=>{
        console.log(err);
    });
}

const update = async(req,res)=>{
    const {id} = req.params;
    const data = req.body.data;
    const foundBrand = await Brands.findOne({where:{id:id}});
    // getting base64 image and converting to buffer
    function decodeBase64Image(dataString) {
        var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
          response = {};
      
        if (matches.length !== 3) {
          return new Error('Invalid input string');
        }
      
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
      
        return response;
      }
      let img_direction = "";
      if(data.img){
        var imageBuffer = decodeBase64Image(req.body.data.img);
        // converting buffer to original image to /upload folder
        let randomNumber = Math.floor(Math.random() * 999999999999);
        console.log("random Number:",randomNumber);
        img_direction = `./uploads/`+randomNumber+`${req.body.data.img_name}`;
          fs.writeFile(img_direction, imageBuffer.data, function(err) { console.log(err) });
          ///////////////////////////////////////////////////////////////////////////////////////////
          // console.log("data::::::::",data);
          
        await fs.unlink(foundBrand.surat,(err)=>{console.log("deleted")})
      }else{
          img_direction = foundBrand.surat;
      }
    Brands.update({
        name_tm:data.name_tm,
        name_ru:data.name_ru,
        name_en:data.name_en,
        surat:img_direction,
        BrandsKategoryId:data.BrandsKategoryId,
        BrandsSubKategoryId:data.BrandsSubKategoryId,
        WelayatlarId:data.WelayatlarId,
        active:data.active,
        deleted:data.deleted,
    },{
        where:{
            id:id
        }
    }).then(()=>{
        res.json("updated");
    }).catch((err)=>{
        console.log(err);
    });
}

const Delete = async(req,res)=>{
    const {id} = req.params;
    const foundBrand = await Brands.findOne({where:{id:id}});
    if(foundBrand){
        await fs.unlink(foundBrand.surat,(err)=>{console.log("deleted")})
    }
     
    Brands.update({
        deleted:true,
        active:false,
    },{
        where:{
            id:id
        }
    }).then(()=>{
        res.json("deleted");
    }).catch((err)=>{
        console.log(err);
    });
}

const Destroy = async(req,res)=>{
    const {id} = req.params;
    const foundBrand = await Brands.findOne({where:{id:id}});
    if(foundBrand){
        await fs.unlink(foundBrand.surat,(err)=>{console.log("deleted")})
    }
     
    Brands.destroy({
        where:{
            id:id
        }
    }).then(()=>{
        res.json("deleted");
    }).catch((err)=>{
        console.log(err);
    });
}

exports.brands_tb = brands_tb;
exports.getAll = getAll;
exports.create = create;
exports.update = update;
exports.Delete = Delete;
exports.Destroy = Destroy;